# JERM-Ema

Eye-movement analysis with hidden semi-Markov models to segment phases in reading tasks

This file contains instructions on how to work with the source code for performing scanpath segmentation with hidden semi-Markov models.

1. Run the code with Docker
==================================
docker run --rm -it -p 8888:8888 registry.gitlab.inria.fr/statify_public/jemr-ema/em:1.4
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root

As an alternative, you may build a singularity image with:
sudo singularity build Ema_1.4.simg PyENE-analysis/Docker-singularity/em-analysis.def
and run jupyter:
singularity run --app jupyter -e -B /my_scratch:/scratch:rw Ema_1.4.simg
where /my_scratch is the directory containing paper_oculo_hmm.ipynb (in principle PyENE-analysis/notebooks)

2. Structure of the project
==================================
- README.md: general information on this repository
- visu-scanpath: python package to visualize scanpaths
- em-analysis: core with data for eye-movement analysis.
- PyENE-analysis: scripts and other data for eye-movement and EEG analysis

