# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import os
from setuptools import setup

import subprocess

# Find files to copy
material = subprocess.Popen("find ./pyene_analysis/material ", shell=True, stdout=subprocess.PIPE).stdout.read()
material = str(material).split('\n')                                                                                              
material = material[1:-1]

models  = subprocess.Popen("find ./pyene_analysis/models ", shell=True, stdout=subprocess.PIPE).stdout.read()
models = str(models).split('\n')
models = models[1:-1]

data = subprocess.Popen("find ./pyene_analysis/data ", shell=True, stdout=subprocess.PIPE).stdout.read()
data = str(data).split('\n')
data = data[1:-1]

r_scripts = subprocess.Popen("find ./pyene_analysis/src/R-scripts", shell=True, stdout=subprocess.PIPE).stdout.read()
r_scripts = str(r_scripts).split('\n')
r_scripts = r_scripts[1:-1]


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='pyeneanalysis',
      version='0.1',
      description='A Python tool for analysis eye-movements.',
      install_requires=requirements,
      author=['Brice Olivier', 'Jean-Baptiste Durand'],
      author_email='Jean-Baptiste.Durand@univ-grenoble-alpes.fr',
      url='https://gitlab.inria.fr/mistis/oculonimbus',
      packages=['pyene_analysis'],
      license=read('LICENSE'),
      long_description=read('README.md'),
      data_files=[('material', material),
                  ('models', models),
                  ('data',data),
                  ('R-scripts',r_scripts)])
