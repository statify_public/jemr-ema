# on met un accent pour passer en UTF-8 : é
fontSize(16);
lineSpacing(48);
alignment("left");
paragraphWithOneLineVisible(2,1/5,4/5,1/5,4/5,"Les collectivités locales sont encouragées à coordonner leur action en faveur des réfugiés. Elles envisagent de monter une plate-forme pour assurer la traçabilité de l'acheminement des colis humanitaires.","cadre=no");
                                                                                                                                                ité 592.8 462 142 29
de 748.8 462 25 29
l'acheminement 231.8 526 180 29
des 425.8 526 38 29
colis 477.8 526 63 29
humanitaires 554.8 526 154 29
