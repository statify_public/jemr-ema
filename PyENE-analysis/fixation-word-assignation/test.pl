use Lingua::StopWords qw( getStopWords );
my $stopwords = getStopWords('en');
print join ' ', grep { !$stopwords->{$_} } @words;

