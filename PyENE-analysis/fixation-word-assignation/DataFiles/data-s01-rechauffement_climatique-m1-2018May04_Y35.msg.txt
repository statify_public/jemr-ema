# 145 Fixation 1 (321.5,270.1): Les~0 pluies~1
# 355 Fixation 2 (409.5,268.8): pluies~1 intenses~2
# 136 Fixation 3 (442.7,273.8): intenses~2
# 274 Fixation 4 (568.1,279.8): ont~3 duré~4
# 83 Fixation 5 (536.9,275.1): ont~3
# 118 Fixation 6 (641.1,284.7): duré~4 plusieurs~5
# 141 Fixation 7 (313.2,349.6): heures~6
# 135 Fixation 8 (282.7,352.4): heures~6
# 147 Fixation 9 (411.4,337.9): causant~7
# 291 Fixation 10 (391.1,344.4): causant~7
# 205 Fixation 11 (485.8,332.7): l'interruption~8
# 136 Fixation 12 (552,330.4): l'interruption~8
# 214 Fixation 13 (336.3,407.2): circulation~11
# 115 Fixation 14 (296.7,408.5): circulation~11
# 329 Fixation 15 (491.8,411.3): isolant~13
# 124 Fixation 16 (465.5,407.2): et~12 isolant~13
# 173 Fixation 17 (644.5,399.4): certain~15
# 88 Fixation 18 (678.1,403.6): certain~15 nombre~16
# 168 Fixation 19 (422.6,463): Le~19 bureau~20
# 155 Fixation 20 (470.1,463.6): bureau~20 prévoit~21
# 165 Fixation 21 (539.5,463.3): prévoit~21
# 247 Fixation 22 (651.4,461.9): une~22 augmentation~23
# 408 Fixation 23 (346.1,515.1): ces~25 dérèglements~26
# 115 Fixation 24 (441.2,519.8): dérèglements~26
# 340 Fixation 25 (567.1,532.7): climatiques~27
# 142 Fixation 26 (701.9,531.5): au~28 niveau~29
%%
# Les~0 Fixations= "1/"
# pluies~1 Fixations= "2/"
# intenses~2 Fixations= "3/"
# ont~3 Fixations= "5/"
# duré~4 Fixations= "6/"
# plusieurs~5 Fixations= "6/"
# heures~6 Fixations= "8/"
# causant~7 Fixations= "10/"
# l'interruption~8 Fixations= "12/"
# de~9 Fixations= ""
# la~10 Fixations= ""
# circulation~11 Fixations= "14/"
# et~12 Fixations= "16/"
# isolant~13 Fixations= "16/"
# un~14 Fixations= ""
# certain~15 Fixations= "18/"
# nombre~16 Fixations= "18/"
# de~17 Fixations= ""
# régions~18 Fixations= ""
# Le~19 Fixations= "19/"
# bureau~20 Fixations= "20/"
# prévoit~21 Fixations= "21/"
# une~22 Fixations= "22/"
# augmentation~23 Fixations= "22/"
# de~24 Fixations= ""
# ces~25 Fixations= "23/"
# dérèglements~26 Fixations= "24/"
# climatiques~27 Fixations= "25/"
# au~28 Fixations= "26/"
# niveau~29 Fixations= "26/"
# de~30 Fixations= ""
# la~31 Fixations= ""
# planète~32 Fixations= ""
%%
#  LES[1] PLUIES[2] INTENSES[3] ONT[5] DURé[6] PLUSIEURS[6] HEURES[8] CAUSANT[10] L'INTERRUPTION[12] de la CIRCULATION[14] ET[16] ISOLANT[16] un CERTAIN[18] NOMBRE[18] de régions LE[19] BUREAU[20] PRéVOIT[21] UNE[22] AUGMENTATION[22] de CES[23] DéRèGLEMENTS[24] CLIMATIQUES[25] AU[26] NIVEAU[26] de la planète
# s01 rechauffement_climatique-m1 PercentageOfWordsProcessed= 0.727272727272727 PercentageOfWordsReallyProcessed= 0.8
