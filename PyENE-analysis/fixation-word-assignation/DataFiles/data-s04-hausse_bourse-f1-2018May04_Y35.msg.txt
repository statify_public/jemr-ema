# 164 Fixation 1 (379.2,297.6): bourse~1 Tokyo~3
# 90 Fixation 2 (413.5,299.9): de~2 Tokyo~3
# 149 Fixation 3 (293.8,300.3): La~0 bourse~1
# 180 Fixation 4 (548.6,290.4): a~4 clôturé~5
# 199 Fixation 5 (646.4,290.4): la~6 séance~7
# 205 Fixation 6 (415.9,339.5): légère~11
# 194 Fixation 7 (357.9,348.6): en~10 légère~11
# 187 Fixation 8 (579,359.3): soutenue~13
# 191 Fixation 9 (581.5,407.6): étrangers~19
# 206 Fixation 10 (443.3,408.5): investisseurs~18
# 162 Fixation 11 (414.1,456): Nikkei~21
# 159 Fixation 12 (359.9,456.7): L'indice~20 Nikkei~21
# 149 Fixation 13 (278.9,457.3): L'indice~20
# 235 Fixation 14 (460.7,462.5): Nikkei~21 affichait~22
# 149 Fixation 15 (398.4,463.9): Nikkei~21
# 165 Fixation 16 (515.7,471.2): affichait~22
# 164 Fixation 17 (629.4,470.5): un~23 gain~24
# 94 Fixation 18 (692.9,471.5): gain~24 important~25
# 141 Fixation 19 (286.3,515.9): à~26
# 181 Fixation 20 (324.7,516.8): la~27 clôture~28
%%
# La~0 Fixations= "3/"
# bourse~1 Fixations= "3/"
# de~2 Fixations= "2/"
# Tokyo~3 Fixations= "2/"
# a~4 Fixations= "4/"
# clôturé~5 Fixations= "4/"
# la~6 Fixations= "5/"
# séance~7 Fixations= "5/"
# de~8 Fixations= ""
# mardi~9 Fixations= ""
# en~10 Fixations= "7/"
# légère~11 Fixations= "7/"
# hausse~12 Fixations= ""
# soutenue~13 Fixations= "8/"
# par~14 Fixations= ""
# les~15 Fixations= ""
# achats~16 Fixations= ""
# des~17 Fixations= ""
# investisseurs~18 Fixations= "10/"
# étrangers~19 Fixations= "9/"
# L'indice~20 Fixations= "13/"
# Nikkei~21 Fixations= "15/"
# affichait~22 Fixations= "16/"
# un~23 Fixations= "17/"
# gain~24 Fixations= "18/"
# important~25 Fixations= "18/"
# à~26 Fixations= "19/"
# la~27 Fixations= "20/"
# clôture~28 Fixations= "20/"
%%
#  LA[3] BOURSE[3] DE[2] TOKYO[2] A[4] CLôTURé[4] LA[5] SéANCE[5] de mardi EN[7] LéGèRE[7] hausse SOUTENUE[8] par les achats des INVESTISSEURS[10] éTRANGERS[9] L'INDICE[13] NIKKEI[15] AFFICHAIT[16] UN[17] GAIN[18] IMPORTANT[18] à[19] LA[20] CLôTURE[20]
# s04 hausse_bourse-f1 PercentageOfWordsProcessed= 0.758620689655172 PercentageOfWordsReallyProcessed= 0.758620689655172
