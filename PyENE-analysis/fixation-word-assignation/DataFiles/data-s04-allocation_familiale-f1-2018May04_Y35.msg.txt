# 149 Fixation 1 (323.4,289.7): Les~0 familles~1
# 175 Fixation 2 (461.9,277.5): ayant~2
# 179 Fixation 3 (580.1,279.5): des~3 jumeaux~4
# 305 Fixation 4 (677.7,291.6): bénéficient~5
# 153 Fixation 5 (591.8,283.4): jumeaux~4
# 112 Fixation 6 (685.1,292.5): bénéficient~5
# 201 Fixation 7 (334.1,346.1): des~6 mêmes~7
# 326 Fixation 8 (434.5,342.4): prestations~8
# 111 Fixation 9 (289.4,346.2): des~6 mêmes~7
# 213 Fixation 10 (540.1,333.4): familiales~9
# 136 Fixation 11 (607.2,342.2): familiales~9
# 132 Fixation 12 (705.7,348.9): que~10
# 278 Fixation 13 (309.5,404): autres~12
# 197 Fixation 14 (447.2,405): mais~13 congés~15
# 208 Fixation 15 (531.9,402.8): congés~15
# 227 Fixation 16 (380.8,408.2): mais~13 congés~15
# 140 Fixation 17 (585.9,399.8): maternité~16
# 278 Fixation 18 (482.6,277.1): ayant~2
# 197 Fixation 19 (446.8,280.9): ayant~2
# 272 Fixation 20 (582,392.9): congés~15 maternité~16
# 289 Fixation 21 (559.1,286): des~3 jumeaux~4
# 260 Fixation 22 (525.2,407.1): congés~15
# 173 Fixation 23 (422.9,410.8): mais~13 congés~15
# 174 Fixation 24 (580,403.6): congés~15 maternité~16
# 147 Fixation 25 (628.8,396.4): maternité~16
# 159 Fixation 26 (746,406.9): sont~17 plus~18
# 248 Fixation 27 (330,470.2): longs~19 18~21
# 103 Fixation 28 (283.4,468.4): longs~19
# 146 Fixation 29 (422.2,469.6): 18~21 semaines~22
# 181 Fixation 30 (352.5,448.9): longs~19 18~21
# 328 Fixation 31 (517.1,361.5): prestations~8 familiales~9
# 325 Fixation 32 (403.6,460.7): de~20 18~21 semaines~22
# 287 Fixation 33 (374.6,465.7): de~20 18~21
# 385 Fixation 34 (429.6,464.3): 18~21 semaines~22
# 234 Fixation 35 (557.7,403.1): congés~15 maternité~16
%%
# Les~0 Fixations= "1/"
# familles~1 Fixations= "1/"
# ayant~2 Fixations= "19/"
# des~3 Fixations= "21/"
# jumeaux~4 Fixations= "21/"
# bénéficient~5 Fixations= "6/"
# des~6 Fixations= "9/"
# mêmes~7 Fixations= "9/"
# prestations~8 Fixations= "31/"
# familiales~9 Fixations= "31/"
# que~10 Fixations= "12/"
# les~11 Fixations= ""
# autres~12 Fixations= "13/"
# mais~13 Fixations= "23/"
# les~14 Fixations= ""
# congés~15 Fixations= "35/"
# maternité~16 Fixations= "35/"
# sont~17 Fixations= "26/"
# plus~18 Fixations= "26/"
# longs~19 Fixations= "30/"
# de~20 Fixations= "33/"
# 18~21 Fixations= "34/"
# semaines~22 Fixations= "34/"
%%
#  LES[1] FAMILLES[1] AYANT[19] DES[21] JUMEAUX[21] BéNéFICIENT[6] DES[9] MêMES[9] PRESTATIONS[31] FAMILIALES[31] QUE[12] les AUTRES[13] MAIS[23] les CONGéS[35] MATERNITé[35] SONT[26] PLUS[26] LONGS[30] DE[33] 18[34] SEMAINES[34]
# s04 allocation_familiale-f1 PercentageOfWordsProcessed= 0.91304347826087 PercentageOfWordsReallyProcessed= 0.91304347826087
