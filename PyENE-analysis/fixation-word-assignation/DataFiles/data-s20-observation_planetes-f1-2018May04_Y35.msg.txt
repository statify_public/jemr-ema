# 234 Fixation 1 (315.2,334.2): entourent~7
# 251 Fixation 2 (426.9,328.6): toutes~8
# 163 Fixation 3 (605.3,233.8): des~4 anneaux~5
# 147 Fixation 4 (661.6,224): anneaux~5
# 192 Fixation 5 (261.5,403.1): orbite~13
# 150 Fixation 6 (299.5,393): orbite~13
# 148 Fixation 7 (449.5,385.5): autour~14 Soleil~16
%%
# L'étude~0 Fixations= ""
# de~1 Fixations= ""
# la~2 Fixations= ""
# dynamique~3 Fixations= ""
# des~4 Fixations= "3/"
# anneaux~5 Fixations= "4/"
# qui~6 Fixations= ""
# entourent~7 Fixations= "1/"
# toutes~8 Fixations= "2/"
# les~9 Fixations= ""
# planètes~10 Fixations= ""
# géantes~11 Fixations= ""
# en~12 Fixations= ""
# orbite~13 Fixations= "6/"
# autour~14 Fixations= "7/"
# du~15 Fixations= ""
# Soleil~16 Fixations= "7/"
# aide~17 Fixations= ""
# les~18 Fixations= ""
# astronomes~19 Fixations= ""
# à~20 Fixations= ""
# comprendre~21 Fixations= ""
# le~22 Fixations= ""
# processus~23 Fixations= ""
# de~24 Fixations= ""
# formation~25 Fixations= ""
# des~26 Fixations= ""
# lunes~27 Fixations= ""
# du~28 Fixations= ""
# système~29 Fixations= ""
# solaire~30 Fixations= ""
%%
#  L'étude de la dynamique DES[3] ANNEAUX[4] qui ENTOURENT[1] TOUTES[2] les planètes géantes en ORBITE[6] AUTOUR[7] du SOLEIL[7] aide les astronomes à comprendre le processus de formation des lunes du système solaire
# s20 observation_planetes-f1 PercentageOfWordsProcessed= 0.225806451612903 PercentageOfWordsReallyProcessed= 0.411764705882353
