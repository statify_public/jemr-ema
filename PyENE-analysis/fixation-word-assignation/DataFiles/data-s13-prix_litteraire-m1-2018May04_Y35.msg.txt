# 213 Fixation 1 (280.1,288.9): Le~0 jeu~1
# 225 Fixation 2 (334,281.2): jeu~1 favori~2
# 233 Fixation 3 (464.8,289.7): des~3 biographes~4
# 151 Fixation 4 (513.5,289.4): des~3 biographes~4
# 217 Fixation 5 (638.5,292.5): de~5 Conrad~6
# 192 Fixation 6 (662.5,295.5): de~5 Conrad~6
# 108 Fixation 7 (739.1,296.3): Conrad~6
# 291 Fixation 8 (291.5,370.5): célèbre~8
# 235 Fixation 9 (392.6,350): écrivain~9
# 227 Fixation 10 (492.2,353.1): anglais~10
# 121 Fixation 11 (481.4,358.6): écrivain~9 anglais~10
# 188 Fixation 12 (617.8,354.3): consiste~11
# 186 Fixation 13 (337,459.3): vie~20 réelle~21
# 146 Fixation 14 (364.8,495.2): réelle~21
# 236 Fixation 15 (465.2,491.5): et~22 romans~24
# 114 Fixation 16 (521,490.2): romans~24
# 205 Fixation 17 (644,481.1): qu'il~25 a~27
# 295 Fixation 18 (711.3,480.1): en~26 a~27 tirés~28
%%
# Le~0 Fixations= "1/"
# jeu~1 Fixations= "2/"
# favori~2 Fixations= "2/"
# des~3 Fixations= "4/"
# biographes~4 Fixations= "4/"
# de~5 Fixations= "6/"
# Conrad~6 Fixations= "7/"
# le~7 Fixations= ""
# célèbre~8 Fixations= "8/"
# écrivain~9 Fixations= "11/"
# anglais~10 Fixations= "11/"
# consiste~11 Fixations= "12/"
# à~12 Fixations= ""
# mesurer~13 Fixations= ""
# l'écart~14 Fixations= ""
# entre~15 Fixations= ""
# les~16 Fixations= ""
# événements~17 Fixations= ""
# de~18 Fixations= ""
# sa~19 Fixations= ""
# vie~20 Fixations= "13/"
# réelle~21 Fixations= "14/"
# et~22 Fixations= "15/"
# les~23 Fixations= ""
# romans~24 Fixations= "16/"
# qu'il~25 Fixations= "17/"
# en~26 Fixations= "18/"
# a~27 Fixations= "18/"
# tirés~28 Fixations= "18/"
%%
#  LE[1] JEU[2] FAVORI[2] DES[4] BIOGRAPHES[4] DE[6] CONRAD[7] le CéLèBRE[8] éCRIVAIN[11] ANGLAIS[11] CONSISTE[12] à mesurer l'écart entre les événements de sa VIE[13] RéELLE[14] ET[15] les ROMANS[16] QU'IL[17] EN[18] A[18] TIRéS[18]
# s13 prix_litteraire-m1 PercentageOfWordsProcessed= 0.655172413793103 PercentageOfWordsReallyProcessed= 0.655172413793103
