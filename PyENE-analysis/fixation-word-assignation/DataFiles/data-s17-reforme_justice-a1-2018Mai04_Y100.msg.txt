# 191 Fixation 1 (307.8,278.4): La~0 science~1
# 289 Fixation 2 (437.4,268.4): n'apporte~2
# 197 Fixation 3 (561.9,273.2): pas~3
# 125 Fixation 4 (531.9,278.2): pas~3
# 173 Fixation 5 (687.6,279.9): seulement~4
# 246 Fixation 6 (293.5,349.9): bienfaits~6
# 152 Fixation 7 (438,351.3): mais~7 a~8
# 180 Fixation 8 (502.2,346.7): n'apporte~2 a~8 apporté~9
# 183 Fixation 9 (397.2,344.2): science~1 n'apporte~2 a~8
# 154 Fixation 10 (506.6,342.7): n'apporte~2 a~8 apporté~9
# 145 Fixation 11 (612.1,349.5): des~10 armes~11
# 152 Fixation 12 (503.8,340.3): n'apporte~2 a~8 apporté~9
# 205 Fixation 13 (437.9,344): n'apporte~2 a~8
# 98 Fixation 14 (527.5,345.7): n'apporte~2 apporté~9
# 153 Fixation 15 (616.8,357.2): des~10 armes~11
# 137 Fixation 16 (597.6,352.7): apporté~9 armes~11
# 178 Fixation 17 (705.1,354.3): armes~11
# 253 Fixation 18 (296.7,412.5): bienfaits~6 destruction~13
# 248 Fixation 19 (429.6,422.4): massive~14
# 158 Fixation 20 (573.9,414.4): et~15 possibilités~17
# 282 Fixation 21 (646.4,418.7): possibilités~17
# 174 Fixation 22 (304.4,462.1): destruction~13 manipulation~19
# 202 Fixation 23 (485.2,475.5): massive~14 biologique~20
# 228 Fixation 24 (643.7,472.9): possibilités~17 technique~22
# 137 Fixation 25 (309.9,534.3): manipulation~19 l'économie~24
# 121 Fixation 26 (581.1,542.4): à~26
%%
# La~0 Fixations= "1/"
# science~1 Fixations= "9/"
# n'apporte~2 Fixations= "14/"
# pas~3 Fixations= "4/"
# seulement~4 Fixations= "5/"
# des~5 Fixations= ""
# bienfaits~6 Fixations= "18/"
# mais~7 Fixations= "7/"
# a~8 Fixations= "13/"
# apporté~9 Fixations= "16/"
# des~10 Fixations= "15/"
# armes~11 Fixations= "17/"
# de~12 Fixations= ""
# destruction~13 Fixations= "22/"
# massive~14 Fixations= "23/"
# et~15 Fixations= "20/"
# des~16 Fixations= ""
# possibilités~17 Fixations= "24/"
# de~18 Fixations= ""
# manipulation~19 Fixations= "25/"
# biologique~20 Fixations= "23/"
# La~21 Fixations= ""
# technique~22 Fixations= "24/"
# et~23 Fixations= ""
# l'économie~24 Fixations= "25/"
# concourent~25 Fixations= ""
# à~26 Fixations= "26/"
# la~27 Fixations= ""
# dégradation~28 Fixations= ""
# de~29 Fixations= ""
# la~30 Fixations= ""
# biosphère~31 Fixations= ""
%%
#  LA[1] SCIENCE[9] N'APPORTE[14] PAS[4] SEULEMENT[5] des BIENFAITS[18] MAIS[7] A[13] APPORTé[16] DES[15] ARMES[17] de DESTRUCTION[22] MASSIVE[23] ET[20] des POSSIBILITéS[24] de MANIPULATION[25] BIOLOGIQUE[23] La TECHNIQUE[24] et L'éCONOMIE[25] concourent à[26] la dégradation de la biosphère
# s17 reforme_justice-a1 PercentageOfWordsProcessed= 0.625 PercentageOfWordsReallyProcessed= 0.740740740740741
