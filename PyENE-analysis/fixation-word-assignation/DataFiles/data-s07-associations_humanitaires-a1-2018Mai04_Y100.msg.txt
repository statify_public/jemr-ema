# 190 Fixation 1 (233.8,277.6): La~0 déflagration~1
# 190 Fixation 2 (427.5,300.2): déflagration~1 militaire~8
# 89 Fixation 3 (352,293.9): déflagration~1
# 234 Fixation 4 (538.5,299.1): s'est~2 produite~3
# 148 Fixation 5 (705.4,294.6): au~4 passage~5
# 205 Fixation 6 (320.9,342.3): La~0 déflagration~1 véhicule~7
# 192 Fixation 7 (453.9,340.4): s'est~2 produite~3 militaire~8
# 152 Fixation 8 (626.4,324.5): produite~3 route~11
# 151 Fixation 9 (434,426.7): à~14
# 147 Fixation 10 (468.2,464.9): à~14 l'armée~22
# 148 Fixation 11 (564.2,463): centaine~16 turque~23
# 82 Fixation 12 (673.4,470.2): de~17 mètres~18 selon~24
%%
# La~0 Fixations= "6/"
# déflagration~1 Fixations= "6/"
# s'est~2 Fixations= "7/"
# produite~3 Fixations= "8/"
# au~4 Fixations= "5/"
# passage~5 Fixations= "5/"
# d'un~6 Fixations= ""
# véhicule~7 Fixations= "6/"
# militaire~8 Fixations= "7/"
# sur~9 Fixations= ""
# une~10 Fixations= ""
# route~11 Fixations= "8/"
# du~12 Fixations= ""
# centre-ville~13 Fixations= ""
# à~14 Fixations= "10/"
# une~15 Fixations= ""
# centaine~16 Fixations= "11/"
# de~17 Fixations= "12/"
# mètres~18 Fixations= "12/"
# d'une~19 Fixations= ""
# base~20 Fixations= ""
# de~21 Fixations= ""
# l'armée~22 Fixations= "10/"
# turque~23 Fixations= "11/"
# selon~24 Fixations= "12/"
# la~25 Fixations= ""
# police~26 Fixations= ""
%%
#  LA[6] DéFLAGRATION[6] S'EST[7] PRODUITE[8] AU[5] PASSAGE[5] d'un VéHICULE[6] MILITAIRE[7] sur une ROUTE[8] du centre-ville à[10] une CENTAINE[11] DE[12] MèTRES[12] d'une base de L'ARMéE[10] TURQUE[11] SELON[12] la police
# s07 associations_humanitaires-a1 PercentageOfWordsProcessed= 0.592592592592593 PercentageOfWordsReallyProcessed= 0.64
