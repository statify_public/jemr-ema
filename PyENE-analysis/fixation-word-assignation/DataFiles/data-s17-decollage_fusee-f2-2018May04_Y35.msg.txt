# 156 Fixation 1 (271.6,273.7): La~0 sonde~1
# 191 Fixation 2 (394.2,272.7): américaine~2
# 210 Fixation 3 (539.9,282.1): Stardust~3
# 159 Fixation 4 (645.8,275.6): qui~4 doit~5
# 158 Fixation 5 (325.6,331.8): ramener~6
# 148 Fixation 6 (451,344.7): terre~8
%%
# La~0 Fixations= "1/"
# sonde~1 Fixations= "1/"
# américaine~2 Fixations= "2/"
# Stardust~3 Fixations= "3/"
# qui~4 Fixations= "4/"
# doit~5 Fixations= "4/"
# ramener~6 Fixations= "5/"
# sur~7 Fixations= ""
# terre~8 Fixations= "6/"
# des~9 Fixations= ""
# échantillons~10 Fixations= ""
# de~11 Fixations= ""
# la~12 Fixations= ""
# comète~13 Fixations= ""
# Wild-2~14 Fixations= ""
# a~15 Fixations= ""
# été~16 Fixations= ""
# lancée~17 Fixations= ""
# de~18 Fixations= ""
# Cap~19 Fixations= ""
# Canaveral~20 Fixations= ""
# en~21 Fixations= ""
# Floride~22 Fixations= ""
# par~23 Fixations= ""
# une~24 Fixations= ""
# fusée~25 Fixations= ""
# Delta-2~26 Fixations= ""
%%
#  LA[1] SONDE[1] AMéRICAINE[2] STARDUST[3] QUI[4] DOIT[4] RAMENER[5] sur TERRE[6] des échantillons de la comète Wild-2 a été lancée de Cap Canaveral en Floride par une fusée Delta-2
# s17 decollage_fusee-f2 PercentageOfWordsProcessed= 0.296296296296296 PercentageOfWordsReallyProcessed= 0.888888888888889
