# 145 Fixation 1 (304.4,279.3): L'ortolan~0
# 236 Fixation 2 (399.8,288.5): petit~1
# 152 Fixation 3 (482.3,277.9): petit~1 oiseau~2
# 165 Fixation 4 (589.3,261.8): très~3
# 479 Fixation 5 (603.9,273.8): très~3 prisé~4
# 300 Fixation 6 (671.4,266.9): prisé~4
# 140 Fixation 7 (693.3,273.4): prisé~4
# 397 Fixation 8 (290.6,343.5): gourmets~7
# 294 Fixation 9 (396.2,340.7): du~8 Sud-ouest~9
# 562 Fixation 10 (563.4,356.3): appartiendra~10
# 219 Fixation 11 (718.7,321.7): bientôt~11
# 227 Fixation 12 (298.7,396.6): aux~12 espèces~13
# 247 Fixation 13 (254.7,400.4): aux~12 espèces~13
# 348 Fixation 14 (421.2,395.5): espèces~13 protégées~14
# 303 Fixation 15 (554.4,399): Un~15 décret~16
# 250 Fixation 16 (497.6,461.3): représente~21
# 164 Fixation 17 (566.6,488): le~22 premier~23
%%
# L'ortolan~0 Fixations= "1/"
# petit~1 Fixations= "3/"
# oiseau~2 Fixations= "3/"
# très~3 Fixations= "5/"
# prisé~4 Fixations= "7/"
# par~5 Fixations= ""
# les~6 Fixations= ""
# gourmets~7 Fixations= "8/"
# du~8 Fixations= "9/"
# Sud-ouest~9 Fixations= "9/"
# appartiendra~10 Fixations= "10/"
# bientôt~11 Fixations= "11/"
# aux~12 Fixations= "13/"
# espèces~13 Fixations= "14/"
# protégées~14 Fixations= "14/"
# Un~15 Fixations= "15/"
# décret~16 Fixations= "15/"
# a~17 Fixations= ""
# été~18 Fixations= ""
# signé~19 Fixations= ""
# qui~20 Fixations= ""
# représente~21 Fixations= "16/"
# le~22 Fixations= "17/"
# premier~23 Fixations= "17/"
# acte~24 Fixations= ""
# d'un~25 Fixations= ""
# compromis~26 Fixations= ""
# global~27 Fixations= ""
# entre~28 Fixations= ""
# écologistes~29 Fixations= ""
# et~30 Fixations= ""
# chasseurs~31 Fixations= ""
%%
#  L'ORTOLAN[1] PETIT[3] OISEAU[3] TRèS[5] PRISé[7] par les GOURMETS[8] DU[9] SUD-OUEST[9] APPARTIENDRA[10] BIENTôT[11] AUX[13] ESPèCES[14] PROTéGéES[14] UN[15] DéCRET[15] a été signé qui REPRéSENTE[16] LE[17] PREMIER[17] acte d'un compromis global entre écologistes et chasseurs
# s14 chasse_oiseaux-m1 PercentageOfWordsProcessed= 0.5625 PercentageOfWordsReallyProcessed= 0.75
