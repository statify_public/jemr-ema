# 243 Fixation 1 (324.5,306.9): Quand~0 brûlons~2
# 209 Fixation 2 (392.2,299.1): nous~1 brûlons~2
# 108 Fixation 3 (495.9,288.8): brûlons~2
# 191 Fixation 4 (563.3,290): des~3 combustibles~4
# 155 Fixation 5 (607.7,282.2): combustibles~4
# 482 Fixation 6 (445,397.2): l'atmosphère~15
# 455 Fixation 7 (509.4,490.7): la~22 réchauffer~23
# 463 Fixation 8 (593.3,513.7): des~30 glaciers~31
%%
# Quand~0 Fixations= "1/"
# nous~1 Fixations= "2/"
# brûlons~2 Fixations= "3/"
# des~3 Fixations= "4/"
# combustibles~4 Fixations= "5/"
# fossiles~5 Fixations= ""
# nous~6 Fixations= ""
# émettons~7 Fixations= ""
# des~8 Fixations= ""
# gaz~9 Fixations= ""
# à~10 Fixations= ""
# effet~11 Fixations= ""
# de~12 Fixations= ""
# serre~13 Fixations= ""
# dans~14 Fixations= ""
# l'atmosphère~15 Fixations= "6/"
# ce~16 Fixations= ""
# qui~17 Fixations= ""
# a~18 Fixations= ""
# pour~19 Fixations= ""
# conséquence~20 Fixations= ""
# de~21 Fixations= ""
# la~22 Fixations= "7/"
# réchauffer~23 Fixations= "7/"
# et~24 Fixations= ""
# de~25 Fixations= ""
# contribuer~26 Fixations= ""
# à~27 Fixations= ""
# la~28 Fixations= ""
# fonte~29 Fixations= ""
# des~30 Fixations= "8/"
# glaciers~31 Fixations= "8/"
%%
#  QUAND[1] NOUS[2] BRûLONS[3] DES[4] COMBUSTIBLES[5] fossiles nous émettons des gaz à effet de serre dans L'ATMOSPHèRE[6] ce qui a pour conséquence de LA[7] RéCHAUFFER[7] et de contribuer à la fonte DES[8] GLACIERS[8]
# s14 rechauffement_climatique-f2 PercentageOfWordsProcessed= 0.3125 PercentageOfWordsReallyProcessed= 0.3125
