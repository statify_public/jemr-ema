# 143 Fixation 1 (278.9,273.2): Comme~0 écho~2
# 138 Fixation 2 (378.1,267.6): en~1 écho~2
# 158 Fixation 3 (478.8,281.3): à~3
# 141 Fixation 4 (600.7,290.3): assurance~5
# 121 Fixation 5 (676.3,299.6): retrouvée~6
# 157 Fixation 6 (275.4,338.7): depuis~7 fin~9
# 145 Fixation 7 (404.6,340.4): la~8 fin~9
# 202 Fixation 8 (653.7,375): la~13 commission~14
# 225 Fixation 9 (298.5,402.1): des~15 affaires~16
# 170 Fixation 10 (433.4,407.1): étrangères~17
# 188 Fixation 11 (606.4,408.3): du~18 Sénat~19
# 152 Fixation 12 (707.6,402.1): a~20 fixé~21
# 184 Fixation 13 (305.5,453.8): date~23
# 180 Fixation 14 (408.7,471.2): pour~24 début~26
# 178 Fixation 15 (547,469.6): des~27 auditions~28
# 114 Fixation 16 (648.3,461.5): auditions~28
# 241 Fixation 17 (286.8,539.2): nomination~31
# 183 Fixation 18 (436.2,522.4): qui~32 était~33
# 144 Fixation 19 (532.4,526.5): était~33 bloquée~34
# 138 Fixation 20 (645.9,525): depuis~35 dix~36
# 101 Fixation 21 (285.5,603.6): mois~37
# 162 Fixation 22 (326.2,295.4): Comme~0 écho~2
# 210 Fixation 23 (509.5,284): cette~4 assurance~5
# 143 Fixation 24 (662,304.3): assurance~5 retrouvée~6
# 177 Fixation 25 (283.5,340.3): depuis~7 fin~9
# 252 Fixation 26 (433.9,325.2): fin~9
# 182 Fixation 27 (414.1,525.9): qui~32 était~33
# 171 Fixation 28 (559.3,521.9): bloquée~34
# 99 Fixation 29 (635.7,521): bloquée~34 dix~36
%%
# Comme~0 Fixations= "22/"
# en~1 Fixations= "2/"
# écho~2 Fixations= "22/"
# à~3 Fixations= "3/"
# cette~4 Fixations= "23/"
# assurance~5 Fixations= "24/"
# retrouvée~6 Fixations= "24/"
# depuis~7 Fixations= "25/"
# la~8 Fixations= "7/"
# fin~9 Fixations= "26/"
# de~10 Fixations= ""
# la~11 Fixations= ""
# guerre~12 Fixations= ""
# la~13 Fixations= "8/"
# commission~14 Fixations= "8/"
# des~15 Fixations= "9/"
# affaires~16 Fixations= "9/"
# étrangères~17 Fixations= "10/"
# du~18 Fixations= "11/"
# Sénat~19 Fixations= "11/"
# a~20 Fixations= "12/"
# fixé~21 Fixations= "12/"
# une~22 Fixations= ""
# date~23 Fixations= "13/"
# pour~24 Fixations= "14/"
# le~25 Fixations= ""
# début~26 Fixations= "14/"
# des~27 Fixations= "15/"
# auditions~28 Fixations= "16/"
# et~29 Fixations= ""
# la~30 Fixations= ""
# nomination~31 Fixations= "17/"
# qui~32 Fixations= "27/"
# était~33 Fixations= "27/"
# bloquée~34 Fixations= "29/"
# depuis~35 Fixations= "20/"
# dix~36 Fixations= "29/"
# mois~37 Fixations= "21/"
%%
#  COMME[22] EN[2] éCHO[22] à[3] CETTE[23] ASSURANCE[24] RETROUVéE[24] DEPUIS[25] LA[7] FIN[26] de la guerre LA[8] COMMISSION[8] DES[9] AFFAIRES[9] éTRANGèRES[10] DU[11] SéNAT[11] A[12] FIXé[12] une DATE[13] POUR[14] le DéBUT[14] DES[15] AUDITIONS[16] et la NOMINATION[17] QUI[27] éTAIT[27] BLOQUéE[29] DEPUIS[20] DIX[29] MOIS[21]
# s07 conflit_irak-m1 PercentageOfWordsProcessed= 0.815789473684211 PercentageOfWordsReallyProcessed= 0.815789473684211
