# 414 Fixation 1 (286.1,285.5): La~0 nageuse~1
# 205 Fixation 2 (414.3,279.7): a~2
# 268 Fixation 3 (425.7,273.1): a~2 décroché~3
# 107 Fixation 4 (459.6,271.3): a~2 décroché~3
# 350 Fixation 5 (591.8,277.5): sa~4 deuxième~5
# 279 Fixation 6 (692.3,284.9): deuxième~5 médaille~6
# 203 Fixation 7 (293.3,331.6): La~0 nageuse~1 d'or~7
# 413 Fixation 8 (318,339.2): La~0 nageuse~1 d'or~7
# 225 Fixation 9 (400.7,337.5): nageuse~1 a~2 championnats~9
# 264 Fixation 10 (571.5,346.7): sa~4 deuxième~5 d'Europe~10
# 177 Fixation 11 (710.2,346.5): médaille~6 natation~12
# 193 Fixation 12 (328.2,417.1): en~13 petit~14
# 137 Fixation 13 (313.2,416.4): en~13 petit~14
%%
# La~0 Fixations= "8/"
# nageuse~1 Fixations= "9/"
# a~2 Fixations= "9/"
# décroché~3 Fixations= "4/"
# sa~4 Fixations= "10/"
# deuxième~5 Fixations= "10/"
# médaille~6 Fixations= "11/"
# d'or~7 Fixations= "8/"
# aux~8 Fixations= ""
# championnats~9 Fixations= "9/"
# d'Europe~10 Fixations= "10/"
# de~11 Fixations= ""
# natation~12 Fixations= "11/"
# en~13 Fixations= "13/"
# petit~14 Fixations= "13/"
# bassin~15 Fixations= ""
# en~16 Fixations= ""
# s'imposant~17 Fixations= ""
# dans~18 Fixations= ""
# son~19 Fixations= ""
# épreuve~20 Fixations= ""
# de~21 Fixations= ""
# prédilection~22 Fixations= ""
# La~23 Fixations= ""
# Française~24 Fixations= ""
# l'a~25 Fixations= ""
# emporté~26 Fixations= ""
# facilement~27 Fixations= ""
# reléguant~28 Fixations= ""
# sa~29 Fixations= ""
# grande~30 Fixations= ""
# rivale~31 Fixations= ""
%%
#  LA[8] NAGEUSE[9] A[9] DéCROCHé[4] SA[10] DEUXIèME[10] MéDAILLE[11] D'OR[8] aux CHAMPIONNATS[9] D'EUROPE[10] de NATATION[11] EN[13] PETIT[13] bassin en s'imposant dans son épreuve de prédilection La Française l'a emporté facilement reléguant sa grande rivale
# s13 associations_humanitaires-a2 PercentageOfWordsProcessed= 0.40625 PercentageOfWordsReallyProcessed= 0.866666666666667
