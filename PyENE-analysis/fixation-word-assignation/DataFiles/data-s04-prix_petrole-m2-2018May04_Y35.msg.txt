# 169 Fixation 1 (377.6,303.8): la~1 pollution~2
# 151 Fixation 2 (318.8,297.1): Outre~0
# 109 Fixation 3 (466.2,280.9): pollution~2
# 238 Fixation 4 (575.5,292.9): surface~4
# 129 Fixation 5 (681.2,296.1): se~5 pose~6
# 206 Fixation 6 (298.5,334.3): aussi~7 problème~9
# 170 Fixation 7 (399.4,337.7): le~8 problème~9
# 185 Fixation 8 (565.5,339.9): des~10 tonnes~11
# 253 Fixation 9 (668.4,350.6): de~12 pétrole~13
# 140 Fixation 10 (539.6,344.6): des~10 tonnes~11
# 184 Fixation 11 (324.5,408.8): encore~14 contenues~15
# 190 Fixation 12 (385.3,411.7): contenues~15
# 178 Fixation 13 (281.7,416.3): encore~14 contenues~15
# 138 Fixation 14 (405.5,412.7): contenues~15
# 173 Fixation 15 (551.1,413.5): dans~16 soutes~18
# 125 Fixation 16 (622.6,416.8): soutes~18
# 323 Fixation 17 (703.4,417.1): du~19 navire~20
# 352 Fixation 18 (595.4,421.9): les~17 soutes~18
# 152 Fixation 19 (520.8,418.1): dans~16 soutes~18
# 126 Fixation 20 (710.1,420.1): du~19 navire~20
# 180 Fixation 21 (372.3,468.7): sondages~22
# 158 Fixation 22 (326.8,476.7): Les~21 sondages~22
# 225 Fixation 23 (468.5,474.6): effectués~23
# 274 Fixation 24 (563.7,478.8): indiquent~24
# 125 Fixation 25 (695.7,474.5): que~25
# 266 Fixation 26 (323.8,529.9): morceaux~27
# 210 Fixation 27 (422,533.6): du~28 bateau~29
# 349 Fixation 28 (577.2,538.7): sont~30 stabilisés~31
# 334 Fixation 29 (523.2,532.7): sont~30
# 173 Fixation 30 (396,513.5): du~28 bateau~29
%%
# Outre~0 Fixations= "2/"
# la~1 Fixations= "1/"
# pollution~2 Fixations= "3/"
# de~3 Fixations= ""
# surface~4 Fixations= "4/"
# se~5 Fixations= "5/"
# pose~6 Fixations= "5/"
# aussi~7 Fixations= "6/"
# le~8 Fixations= "7/"
# problème~9 Fixations= "7/"
# des~10 Fixations= "10/"
# tonnes~11 Fixations= "10/"
# de~12 Fixations= "9/"
# pétrole~13 Fixations= "9/"
# encore~14 Fixations= "13/"
# contenues~15 Fixations= "14/"
# dans~16 Fixations= "19/"
# les~17 Fixations= "18/"
# soutes~18 Fixations= "19/"
# du~19 Fixations= "20/"
# navire~20 Fixations= "20/"
# Les~21 Fixations= "22/"
# sondages~22 Fixations= "22/"
# effectués~23 Fixations= "23/"
# indiquent~24 Fixations= "24/"
# que~25 Fixations= "25/"
# les~26 Fixations= ""
# morceaux~27 Fixations= "26/"
# du~28 Fixations= "30/"
# bateau~29 Fixations= "30/"
# sont~30 Fixations= "29/"
# stabilisés~31 Fixations= "28/"
%%
#  OUTRE[2] LA[1] POLLUTION[3] de SURFACE[4] SE[5] POSE[5] AUSSI[6] LE[7] PROBLèME[7] DES[10] TONNES[10] DE[9] PéTROLE[9] ENCORE[13] CONTENUES[14] DANS[19] LES[18] SOUTES[19] DU[20] NAVIRE[20] LES[22] SONDAGES[22] EFFECTUéS[23] INDIQUENT[24] QUE[25] les MORCEAUX[26] DU[30] BATEAU[30] SONT[29] STABILISéS[28]
# s04 prix_petrole-m2 PercentageOfWordsProcessed= 0.9375 PercentageOfWordsReallyProcessed= 0.9375
