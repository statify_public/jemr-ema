# 187 Fixation 1 (318.4,290.9): L'Irak~0 a~1
# 140 Fixation 2 (292.9,284.4): L'Irak~0
# 653 Fixation 3 (374.5,282.9): a~1 demandé~2
# 154 Fixation 4 (344.5,281.6): L'Irak~0 a~1
# 257 Fixation 5 (515.5,274.5): à~3 l'ONU~4
%%
# L'Irak~0 Fixations= "4/"
# a~1 Fixations= "4/"
# demandé~2 Fixations= "3/"
# à~3 Fixations= "5/"
# l'ONU~4 Fixations= "5/"
# d'empêcher~5 Fixations= ""
# les~6 Fixations= ""
# survols~7 Fixations= ""
# américains~8 Fixations= ""
# et~9 Fixations= ""
# britanniques~10 Fixations= ""
# et~11 Fixations= ""
# de~12 Fixations= ""
# permettre~13 Fixations= ""
# à~14 Fixations= ""
# ses~15 Fixations= ""
# avions~16 Fixations= ""
# civils~17 Fixations= ""
# de~18 Fixations= ""
# transporter~19 Fixations= ""
# des~20 Fixations= ""
# scientifiques~21 Fixations= ""
# étrangers~22 Fixations= ""
# L'Irak~23 Fixations= ""
# est~24 Fixations= ""
# soumis~25 Fixations= ""
# à~26 Fixations= ""
# un~27 Fixations= ""
# embargo~28 Fixations= ""
# aérien~29 Fixations= ""
# depuis~30 Fixations= ""
# 1990~31 Fixations= ""
%%
#  L'IRAK[4] A[4] DEMANDé[3] à[5] L'ONU[5] d'empêcher les survols américains et britanniques et de permettre à ses avions civils de transporter des scientifiques étrangers L'Irak est soumis à un embargo aérien depuis 1990
# s14 conflit_irak-f2 PercentageOfWordsProcessed= 0.15625 PercentageOfWordsReallyProcessed= 1
