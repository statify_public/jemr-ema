# 136 Fixation 1 (290.8,280.2): L'artillerie~0
# 118 Fixation 2 (355.8,278.9): L'artillerie~0
# 277 Fixation 3 (432.9,282.7): israélienne~1
# 93 Fixation 4 (458.6,284.4): israélienne~1
# 266 Fixation 5 (609.6,299.7): a~2 massivement~3
# 161 Fixation 6 (310.6,356.2): bombardé~4
# 187 Fixation 7 (279,361.8): bombardé~4
# 192 Fixation 8 (462.7,347.7): régions~6
# 196 Fixation 9 (572.8,361.7): supposées~7
# 116 Fixation 10 (669.5,363.2): être~8
# 175 Fixation 11 (365,430.9): bastions~10 Hezbollah~12
# 298 Fixation 12 (462.2,424.8): Hezbollah~12
# 306 Fixation 13 (580.7,416.8): au~13 Liban~14
# 199 Fixation 14 (671,420.6): Liban~14 tirs~16
# 216 Fixation 15 (430,480.3): riposte~19 à~20
# 260 Fixation 16 (396,482.7): une~18 riposte~19
# 153 Fixation 17 (510.3,561.4): la~26 milice~27
# 136 Fixation 18 (687.4,551.6): intégriste~28 Chiite~29
# 267 Fixation 19 (362.8,584): Israël~31
# 298 Fixation 20 (700.5,545): intégriste~28 Chiite~29
# 140 Fixation 21 (540.2,547.1): milice~27
%%
# L'artillerie~0 Fixations= "2/"
# israélienne~1 Fixations= "4/"
# a~2 Fixations= "5/"
# massivement~3 Fixations= "5/"
# bombardé~4 Fixations= "7/"
# des~5 Fixations= ""
# régions~6 Fixations= "8/"
# supposées~7 Fixations= "9/"
# être~8 Fixations= "10/"
# des~9 Fixations= ""
# bastions~10 Fixations= "11/"
# du~11 Fixations= ""
# Hezbollah~12 Fixations= "12/"
# au~13 Fixations= "13/"
# Liban~14 Fixations= "14/"
# Ces~15 Fixations= ""
# tirs~16 Fixations= "14/"
# sont~17 Fixations= ""
# une~18 Fixations= "16/"
# riposte~19 Fixations= "16/"
# à~20 Fixations= "15/"
# une~21 Fixations= ""
# série~22 Fixations= ""
# d'attaques~23 Fixations= ""
# meurtrières~24 Fixations= ""
# de~25 Fixations= ""
# la~26 Fixations= "17/"
# milice~27 Fixations= "21/"
# intégriste~28 Fixations= "20/"
# Chiite~29 Fixations= "20/"
# contre~30 Fixations= ""
# Israël~31 Fixations= "19/"
%%
#  L'ARTILLERIE[2] ISRAéLIENNE[4] A[5] MASSIVEMENT[5] BOMBARDé[7] des RéGIONS[8] SUPPOSéES[9] êTRE[10] des BASTIONS[11] du HEZBOLLAH[12] AU[13] LIBAN[14] Ces TIRS[14] sont UNE[16] RIPOSTE[16] à[15] une série d'attaques meurtrières de LA[17] MILICE[21] INTéGRISTE[20] CHIITE[20] contre ISRAëL[19]
# s01 conflit_israelo_palestinien-f1 PercentageOfWordsProcessed= 0.65625 PercentageOfWordsReallyProcessed= 0.65625
