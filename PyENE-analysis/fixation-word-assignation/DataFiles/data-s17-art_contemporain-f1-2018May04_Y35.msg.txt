# 142 Fixation 1 (292.4,280.3): On~0 souvient~2
# 141 Fixation 2 (347.1,277.7): se~1 souvient~2
# 232 Fixation 3 (472.2,276.3): du~3 fameux~4
# 202 Fixation 4 (593.8,284): fameux~4 symbole~5
# 172 Fixation 5 (695.1,277): du~6 dollar~7
# 232 Fixation 6 (673.7,280.2): symbole~5 dollar~7
# 179 Fixation 7 (301.2,341.5): revu~8
# 114 Fixation 8 (389.2,340.3): par~9 Andy~10
# 210 Fixation 9 (270.7,341.2): revu~8
# 140 Fixation 10 (383.2,337.2): par~9 Andy~10
# 87 Fixation 11 (448.7,342.5): Andy~10 Warhol~11
# 258 Fixation 12 (594.2,349.9): Le~12 pop-artiste~13
# 203 Fixation 13 (338.8,414.9): américain~14
# 149 Fixation 14 (595.7,416.7): signe~17 simple~18
# 176 Fixation 15 (710.1,412.3): simple~18 amalgamé~19
# 172 Fixation 16 (358.9,466.9): et~21 l'argent~22
# 116 Fixation 17 (434.2,472.8): l'argent~22
# 134 Fixation 18 (732.8,480.2): grande~26 joie~27
# 189 Fixation 19 (322.1,534.4): de~28 collectionneurs~30
# 213 Fixation 20 (386.4,537.1): collectionneurs~30
%%
# On~0 Fixations= "1/"
# se~1 Fixations= "2/"
# souvient~2 Fixations= "2/"
# du~3 Fixations= "3/"
# fameux~4 Fixations= "4/"
# symbole~5 Fixations= "6/"
# du~6 Fixations= "5/"
# dollar~7 Fixations= "6/"
# revu~8 Fixations= "9/"
# par~9 Fixations= "10/"
# Andy~10 Fixations= "11/"
# Warhol~11 Fixations= "11/"
# Le~12 Fixations= "12/"
# pop-artiste~13 Fixations= "12/"
# américain~14 Fixations= "13/"
# avait~15 Fixations= ""
# d'un~16 Fixations= ""
# signe~17 Fixations= "14/"
# simple~18 Fixations= "15/"
# amalgamé~19 Fixations= "15/"
# l'art~20 Fixations= ""
# et~21 Fixations= "16/"
# l'argent~22 Fixations= "17/"
# pour~23 Fixations= ""
# la~24 Fixations= ""
# plus~25 Fixations= ""
# grande~26 Fixations= "18/"
# joie~27 Fixations= "18/"
# de~28 Fixations= "19/"
# ses~29 Fixations= ""
# collectionneurs~30 Fixations= "20/"
%%
#  ON[1] SE[2] SOUVIENT[2] DU[3] FAMEUX[4] SYMBOLE[6] DU[5] DOLLAR[6] REVU[9] PAR[10] ANDY[11] WARHOL[11] LE[12] POP-ARTISTE[12] AMéRICAIN[13] avait d'un SIGNE[14] SIMPLE[15] AMALGAMé[15] l'art ET[16] L'ARGENT[17] pour la plus GRANDE[18] JOIE[18] DE[19] ses COLLECTIONNEURS[20]
# s17 art_contemporain-f1 PercentageOfWordsProcessed= 0.774193548387097 PercentageOfWordsReallyProcessed= 0.774193548387097
