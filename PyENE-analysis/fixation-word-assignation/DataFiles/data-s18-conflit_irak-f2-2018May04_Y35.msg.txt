# 209 Fixation 1 (305.2,294.1): L'Irak~0 a~1
# 169 Fixation 2 (388.4,288.4): a~1 demandé~2
# 249 Fixation 3 (495.2,282.1): à~3 l'ONU~4
# 204 Fixation 4 (376.8,288.9): a~1 demandé~2
# 217 Fixation 5 (587.3,287.1): l'ONU~4 d'empêcher~5
# 220 Fixation 6 (305,349.8): survols~7
# 120 Fixation 7 (354.4,348.8): survols~7 américains~8
# 162 Fixation 8 (264.7,354.8): survols~7
# 320 Fixation 9 (540.7,354): et~9 britanniques~10
# 281 Fixation 10 (464.4,420.4): ses~15 avions~16
# 270 Fixation 11 (360,464.9): transporter~19
# 125 Fixation 12 (454.4,472.6): des~20 scientifiques~21
# 199 Fixation 13 (302,526.3): L'Irak~23
# 148 Fixation 14 (380.6,530.9): est~24 soumis~25
%%
# L'Irak~0 Fixations= "1/"
# a~1 Fixations= "4/"
# demandé~2 Fixations= "4/"
# à~3 Fixations= "3/"
# l'ONU~4 Fixations= "5/"
# d'empêcher~5 Fixations= "5/"
# les~6 Fixations= ""
# survols~7 Fixations= "8/"
# américains~8 Fixations= "7/"
# et~9 Fixations= "9/"
# britanniques~10 Fixations= "9/"
# et~11 Fixations= ""
# de~12 Fixations= ""
# permettre~13 Fixations= ""
# à~14 Fixations= ""
# ses~15 Fixations= "10/"
# avions~16 Fixations= "10/"
# civils~17 Fixations= ""
# de~18 Fixations= ""
# transporter~19 Fixations= "11/"
# des~20 Fixations= "12/"
# scientifiques~21 Fixations= "12/"
# étrangers~22 Fixations= ""
# L'Irak~23 Fixations= "13/"
# est~24 Fixations= "14/"
# soumis~25 Fixations= "14/"
# à~26 Fixations= ""
# un~27 Fixations= ""
# embargo~28 Fixations= ""
# aérien~29 Fixations= ""
# depuis~30 Fixations= ""
# 1990~31 Fixations= ""
%%
#  L'IRAK[1] A[4] DEMANDé[4] à[3] L'ONU[5] D'EMPêCHER[5] les SURVOLS[8] AMéRICAINS[7] ET[9] BRITANNIQUES[9] et de permettre à SES[10] AVIONS[10] civils de TRANSPORTER[11] DES[12] SCIENTIFIQUES[12] étrangers L'IRAK[13] EST[14] SOUMIS[14] à un embargo aérien depuis 1990
# s18 conflit_irak-f2 PercentageOfWordsProcessed= 0.5625 PercentageOfWordsReallyProcessed= 0.692307692307692
