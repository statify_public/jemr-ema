# 110 Fixation 1 (324.4,310.5): La~0 sonde~1
# 125 Fixation 2 (355,282.8): sonde~1
# 149 Fixation 3 (409.4,282.6): américaine~2
# 187 Fixation 4 (543.5,282.3): Stardust~3
# 241 Fixation 5 (297.9,277.1): La~0 sonde~1
# 146 Fixation 6 (425.1,277): américaine~2
# 148 Fixation 7 (570.5,295.7): Stardust~3
# 132 Fixation 8 (655.3,286.2): qui~4 doit~5
# 177 Fixation 9 (319.5,325.8): ramener~6
# 175 Fixation 10 (426.9,343.2): sur~7 terre~8
# 172 Fixation 11 (546.9,344.9): des~9 échantillons~10
# 117 Fixation 12 (585.1,348.5): échantillons~10
# 128 Fixation 13 (720.4,346.7): de~11
# 315 Fixation 14 (330.6,371): ramener~6
# 126 Fixation 15 (421.7,394.8): Wild-2~14 a~15
# 193 Fixation 16 (537.5,406.5): été~16 lancée~17
# 146 Fixation 17 (673.3,416.1): de~18 Cap~19
# 108 Fixation 18 (716.4,417.2): Cap~19 Canaveral~20
# 173 Fixation 19 (388.8,441.9): Wild-2~14
# 170 Fixation 20 (341.4,439.8): comète~13 Wild-2~14
# 144 Fixation 21 (545.9,452.9): fusée~25
# 146 Fixation 22 (602.4,461.8): Delta-2~26
%%
# La~0 Fixations= "5/"
# sonde~1 Fixations= "5/"
# américaine~2 Fixations= "6/"
# Stardust~3 Fixations= "7/"
# qui~4 Fixations= "8/"
# doit~5 Fixations= "8/"
# ramener~6 Fixations= "14/"
# sur~7 Fixations= "10/"
# terre~8 Fixations= "10/"
# des~9 Fixations= "11/"
# échantillons~10 Fixations= "12/"
# de~11 Fixations= "13/"
# la~12 Fixations= ""
# comète~13 Fixations= "20/"
# Wild-2~14 Fixations= "20/"
# a~15 Fixations= "15/"
# été~16 Fixations= "16/"
# lancée~17 Fixations= "16/"
# de~18 Fixations= "17/"
# Cap~19 Fixations= "18/"
# Canaveral~20 Fixations= "18/"
# en~21 Fixations= ""
# Floride~22 Fixations= ""
# par~23 Fixations= ""
# une~24 Fixations= ""
# fusée~25 Fixations= "21/"
# Delta-2~26 Fixations= "22/"
%%
#  LA[5] SONDE[5] AMéRICAINE[6] STARDUST[7] QUI[8] DOIT[8] RAMENER[14] SUR[10] TERRE[10] DES[11] éCHANTILLONS[12] DE[13] la COMèTE[20] WILD-2[20] A[15] éTé[16] LANCéE[16] DE[17] CAP[18] CANAVERAL[18] en Floride par une FUSéE[21] DELTA-2[22]
# s08 decollage_fusee-f2 PercentageOfWordsProcessed= 0.814814814814815 PercentageOfWordsReallyProcessed= 0.814814814814815
