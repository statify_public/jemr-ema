# 140 Fixation 1 (302.2,302.3): La~0 direction~1
# 120 Fixation 2 (464,294.9): de~2 l'entreprise~3
# 152 Fixation 3 (541,291.7): l'entreprise~3
# 151 Fixation 4 (286.3,339): l'existence~5
# 141 Fixation 5 (488.1,360.2): document~7
# 215 Fixation 6 (572,368.4): document~7 incriminé~8
# 171 Fixation 7 (577.2,411.6): collaborateur~13
# 158 Fixation 8 (725.3,438.8): externe~14
# 124 Fixation 9 (425.8,474.7): processus~16 recrutement~18
# 146 Fixation 10 (505.4,480.1): recrutement~18
# 158 Fixation 11 (379.5,521.6): inadmissible~20
# 114 Fixation 12 (441.2,530.7): Elle~21 cautionne~23
# 153 Fixation 13 (566.3,534.7): ne~22 cautionne~23
# 121 Fixation 14 (703.1,542): absolument~24
# 162 Fixation 15 (348.6,592.4): cette~26 action~27
# 172 Fixation 16 (279.2,591): pas~25 action~27
%%
# La~0 Fixations= "1/"
# direction~1 Fixations= "1/"
# de~2 Fixations= "2/"
# l'entreprise~3 Fixations= "3/"
# confirme~4 Fixations= ""
# l'existence~5 Fixations= "4/"
# du~6 Fixations= ""
# document~7 Fixations= "6/"
# incriminé~8 Fixations= "6/"
# jugeant~9 Fixations= ""
# que~10 Fixations= ""
# l'initiative~11 Fixations= ""
# d'un~12 Fixations= ""
# collaborateur~13 Fixations= "7/"
# externe~14 Fixations= "8/"
# au~15 Fixations= ""
# processus~16 Fixations= "9/"
# de~17 Fixations= ""
# recrutement~18 Fixations= "10/"
# est~19 Fixations= ""
# inadmissible~20 Fixations= "11/"
# Elle~21 Fixations= "12/"
# ne~22 Fixations= "13/"
# cautionne~23 Fixations= "13/"
# absolument~24 Fixations= "14/"
# pas~25 Fixations= "16/"
# cette~26 Fixations= "15/"
# action~27 Fixations= "16/"
%%
#  LA[1] DIRECTION[1] DE[2] L'ENTREPRISE[3] confirme L'EXISTENCE[4] du DOCUMENT[6] INCRIMINé[6] jugeant que l'initiative d'un COLLABORATEUR[7] EXTERNE[8] au PROCESSUS[9] de RECRUTEMENT[10] est INADMISSIBLE[11] ELLE[12] NE[13] CAUTIONNE[13] ABSOLUMENT[14] PAS[16] CETTE[15] ACTION[16]
# s10 greve_cheminot-m1 PercentageOfWordsProcessed= 0.678571428571429 PercentageOfWordsReallyProcessed= 0.678571428571429
