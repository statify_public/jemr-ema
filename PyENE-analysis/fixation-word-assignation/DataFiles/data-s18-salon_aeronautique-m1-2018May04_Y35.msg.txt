# 164 Fixation 1 (275,257): L'exploit~0
# 187 Fixation 2 (402.3,269.2): de~1 Louis~2
# 143 Fixation 3 (478.8,267.5): Louis~2 Blériot~3
# 127 Fixation 4 (595.6,276.4): Blériot~3
# 196 Fixation 5 (287.6,347.6): aviateur~6
# 195 Fixation 6 (374.7,335.8): aviateur~6 à~7
# 207 Fixation 7 (468.6,337.2): avoir~8 traversé~9
# 460 Fixation 8 (586.4,329.8): traversé~9 manche~11
# 276 Fixation 9 (637.5,330.4): la~10 manche~11
# 126 Fixation 10 (526.7,387.4): été~17 reproduit~18
# 262 Fixation 11 (291,403.1): avion~13
# 154 Fixation 12 (330.4,455.3): juillet~21 90~22
# 154 Fixation 13 (407.3,456): 90~22 ans~23
# 208 Fixation 14 (500,457.3): jour~24
# 192 Fixation 15 (585,462.8): pour~25 jour~26
# 161 Fixation 16 (646.6,469): jour~26 aprés~27
# 193 Fixation 17 (515.9,520.2): ligne~32 suédois~33
# 170 Fixation 18 (716.8,406.3): dimanche~19
# 92 Fixation 19 (725.7,404.9): dimanche~19 25~20
# 99 Fixation 20 (247.2,483.8): juillet~21
# 182 Fixation 21 (270.4,475.9): juillet~21
# 143 Fixation 22 (316.3,543.3): un~29 pilote~30
# 148 Fixation 23 (437.8,534.3): de~31 ligne~32
# 118 Fixation 24 (498.4,530.2): ligne~32 suédois~33
%%
# L'exploit~0 Fixations= "1/"
# de~1 Fixations= "2/"
# Louis~2 Fixations= "3/"
# Blériot~3 Fixations= "4/"
# le~4 Fixations= ""
# premier~5 Fixations= ""
# aviateur~6 Fixations= "6/"
# à~7 Fixations= "6/"
# avoir~8 Fixations= "7/"
# traversé~9 Fixations= "8/"
# la~10 Fixations= "9/"
# manche~11 Fixations= "9/"
# en~12 Fixations= ""
# avion~13 Fixations= "11/"
# en~14 Fixations= ""
# 1909~15 Fixations= ""
# a~16 Fixations= ""
# été~17 Fixations= "10/"
# reproduit~18 Fixations= "10/"
# dimanche~19 Fixations= "19/"
# 25~20 Fixations= "19/"
# juillet~21 Fixations= "21/"
# 90~22 Fixations= "13/"
# ans~23 Fixations= "13/"
# jour~24 Fixations= "14/"
# pour~25 Fixations= "15/"
# jour~26 Fixations= "16/"
# aprés~27 Fixations= "16/"
# par~28 Fixations= ""
# un~29 Fixations= "22/"
# pilote~30 Fixations= "22/"
# de~31 Fixations= "23/"
# ligne~32 Fixations= "24/"
# suédois~33 Fixations= "24/"
%%
#  L'EXPLOIT[1] DE[2] LOUIS[3] BLéRIOT[4] le premier AVIATEUR[6] à[6] AVOIR[7] TRAVERSé[8] LA[9] MANCHE[9] en AVION[11] en 1909 a éTé[10] REPRODUIT[10] DIMANCHE[19] 25[19] JUILLET[21] 90[13] ANS[13] JOUR[14] POUR[15] JOUR[16] APRéS[16] par UN[22] PILOTE[22] DE[23] LIGNE[24] SUéDOIS[24]
# s18 salon_aeronautique-m1 PercentageOfWordsProcessed= 0.794117647058823 PercentageOfWordsReallyProcessed= 0.794117647058823
