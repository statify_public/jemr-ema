# 157 Fixation 1 (275.8,292.8): A~0 Moscou~1
# 146 Fixation 2 (433.2,289.4): les~2 communiqués~3
# 130 Fixation 3 (497.8,284.8): communiqués~3
# 206 Fixation 4 (609.7,284.9): triomphants~4
# 128 Fixation 5 (689.4,284.3): triomphants~4
# 191 Fixation 6 (252.1,338.5): succèdent~6
# 245 Fixation 7 (321.6,338.5): succèdent~6
# 140 Fixation 8 (281.2,337.4): succèdent~6
# 136 Fixation 9 (403.9,339.3): Dans~7 Daghestan~9
# 256 Fixation 10 (522.1,346.6): le~8 Daghestan~9
# 150 Fixation 11 (501.5,348.2): le~8 Daghestan~9
# 162 Fixation 12 (647.1,345.3): les~10 rebelles~11
# 138 Fixation 13 (709.4,355.2): les~10 rebelles~11
# 101 Fixation 14 (210.8,388.5): sont~12
# 160 Fixation 15 (321.6,387.5): sont~12 complètement~13
# 102 Fixation 16 (410.3,398.2): complètement~13
# 134 Fixation 17 (513.9,405.5): encerclés~14
# 162 Fixation 18 (495.5,407.3): encerclés~14
# 161 Fixation 19 (566.2,408.3): encerclés~14
# 147 Fixation 20 (574,406.4): encerclés~14
# 145 Fixation 21 (645,416.4): leur~15 chef~16
# 84 Fixation 22 (699.2,414.6): leur~15 chef~16
# 243 Fixation 23 (247.3,454.6): militaire~17
# 103 Fixation 24 (311.2,456.9): militaire~17
# 331 Fixation 25 (446.2,462.1): est~18 blessé~19
# 286 Fixation 26 (562.2,471.8): les~20 islamistes~21
# 326 Fixation 27 (610,476.1): les~20 islamistes~21
# 151 Fixation 28 (289.6,513): subi~23
%%
# A~0 Fixations= "1/"
# Moscou~1 Fixations= "1/"
# les~2 Fixations= "2/"
# communiqués~3 Fixations= "3/"
# triomphants~4 Fixations= "5/"
# se~5 Fixations= ""
# succèdent~6 Fixations= "8/"
# Dans~7 Fixations= "9/"
# le~8 Fixations= "11/"
# Daghestan~9 Fixations= "11/"
# les~10 Fixations= "13/"
# rebelles~11 Fixations= "13/"
# sont~12 Fixations= "15/"
# complètement~13 Fixations= "16/"
# encerclés~14 Fixations= "20/"
# leur~15 Fixations= "22/"
# chef~16 Fixations= "22/"
# militaire~17 Fixations= "24/"
# est~18 Fixations= "25/"
# blessé~19 Fixations= "25/"
# les~20 Fixations= "27/"
# islamistes~21 Fixations= "27/"
# ont~22 Fixations= ""
# subi~23 Fixations= "28/"
# de~24 Fixations= ""
# lourdes~25 Fixations= ""
# pertes~26 Fixations= ""
%%
#  A[1] MOSCOU[1] LES[2] COMMUNIQUéS[3] TRIOMPHANTS[5] se SUCCèDENT[8] DANS[9] LE[11] DAGHESTAN[11] LES[13] REBELLES[13] SONT[15] COMPLèTEMENT[16] ENCERCLéS[20] LEUR[22] CHEF[22] MILITAIRE[24] EST[25] BLESSé[25] LES[27] ISLAMISTES[27] ont SUBI[28] de lourdes pertes
# s06 chef_russie-f1 PercentageOfWordsProcessed= 0.814814814814815 PercentageOfWordsReallyProcessed= 0.916666666666667
