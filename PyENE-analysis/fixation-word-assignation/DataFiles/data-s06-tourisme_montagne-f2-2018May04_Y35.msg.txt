# 133 Fixation 1 (269.3,289.7): Toute~0
# 188 Fixation 2 (352.8,286.5): Toute~0 magie~2
# 104 Fixation 3 (416.3,284.3): magie~2
# 262 Fixation 4 (494.1,289.8): des~3 hauts~4
# 164 Fixation 5 (599.1,289.6): hauts~4 plateaux~5
# 137 Fixation 6 (588.1,295.4): hauts~4 plateaux~5
# 415 Fixation 7 (271.6,334.7): basaltiques~6
# 174 Fixation 8 (337.7,334.7): basaltiques~6
# 222 Fixation 9 (426.8,339.5): avec~7 neige~9
# 159 Fixation 10 (482.9,343.6): avec~7 neige~9
# 152 Fixation 11 (527.6,348.9): la~8 neige~9
# 326 Fixation 12 (625,360.9): en~10 plus~11
# 137 Fixation 13 (673.2,360.6): plus~11
# 85 Fixation 14 (704.4,361): plus~11
# 217 Fixation 15 (267,383.9): programme~13
# 191 Fixation 16 (403.8,398.2): randonnée~14
# 147 Fixation 17 (457.6,403.5): randonnée~14
# 294 Fixation 18 (561.2,412.5): en~15 raquettes~16
# 466 Fixation 19 (597.9,413): en~15 raquettes~16
%%
# Toute~0 Fixations= "2/"
# la~1 Fixations= ""
# magie~2 Fixations= "3/"
# des~3 Fixations= "4/"
# hauts~4 Fixations= "6/"
# plateaux~5 Fixations= "6/"
# basaltiques~6 Fixations= "8/"
# avec~7 Fixations= "10/"
# la~8 Fixations= "11/"
# neige~9 Fixations= "11/"
# en~10 Fixations= "12/"
# plus~11 Fixations= "14/"
# Au~12 Fixations= ""
# programme~13 Fixations= "15/"
# randonnée~14 Fixations= "17/"
# en~15 Fixations= "19/"
# raquettes~16 Fixations= "19/"
# avec~17 Fixations= ""
# guide~18 Fixations= ""
# ski~19 Fixations= ""
# de~20 Fixations= ""
# fond~21 Fixations= ""
# et~22 Fixations= ""
# relaxation~23 Fixations= ""
# dans~24 Fixations= ""
# les~25 Fixations= ""
# eaux~26 Fixations= ""
# chaudes~27 Fixations= ""
# thermales~28 Fixations= ""
# de~29 Fixations= ""
# la~30 Fixations= ""
# montagne~31 Fixations= ""
%%
#  TOUTE[2] la MAGIE[3] DES[4] HAUTS[6] PLATEAUX[6] BASALTIQUES[8] AVEC[10] LA[11] NEIGE[11] EN[12] PLUS[14] Au PROGRAMME[15] RANDONNéE[17] EN[19] RAQUETTES[19] avec guide ski de fond et relaxation dans les eaux chaudes thermales de la montagne
# s06 tourisme_montagne-f2 PercentageOfWordsProcessed= 0.46875 PercentageOfWordsReallyProcessed= 0.882352941176471
