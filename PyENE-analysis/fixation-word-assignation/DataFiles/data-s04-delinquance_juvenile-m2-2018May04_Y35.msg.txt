# 158 Fixation 1 (329.3,299.7): société~1
# 328 Fixation 2 (397.7,294.4): société~1 HLM~2
# 185 Fixation 3 (450.9,286.8): HLM~2 3F~3
# 234 Fixation 4 (419.1,289.9): HLM~2 3F~3
# 337 Fixation 5 (386.1,296.3): société~1 HLM~2
# 211 Fixation 6 (427.1,309.9): HLM~2 3F~3
# 278 Fixation 7 (514.5,330.5): réservée~11
# 124 Fixation 8 (496.6,327.3): réservée~11
# 254 Fixation 9 (653.8,318.5): gardiens~13
# 130 Fixation 10 (615.1,307.7): organisé~5
# 137 Fixation 11 (540,295.7): a~4 organisé~5
# 161 Fixation 12 (655.8,300.4): une~6 formation~7
# 211 Fixation 13 (315.5,351.1): de~8 trois~9
# 192 Fixation 14 (393.5,351.8): trois~9 jours~10
# 182 Fixation 15 (456.1,349.2): jours~10 réservée~11
# 320 Fixation 16 (589.3,353.5): aux~12 gardiens~13
# 236 Fixation 17 (289.1,418.4): d'immeubles~14
# 151 Fixation 18 (400.6,411.6): d'immeubles~14 L'objectif~15
# 153 Fixation 19 (474.4,418.9): L'objectif~15
# 248 Fixation 20 (602.2,417.5): est~16 permettre~18
# 240 Fixation 21 (305.2,468.8): aux~19 personnels~20
# 206 Fixation 22 (491.6,475): de~21 terrain~22
# 217 Fixation 23 (610.3,477.1): de~23 garder~24
# 221 Fixation 24 (301.7,516.6): distance~26
# 133 Fixation 25 (396.7,522.1): nécessaire~27
# 92 Fixation 26 (283.1,522.5): distance~26
# 82 Fixation 27 (514.4,523.3): nécessaire~27
# 131 Fixation 28 (629.2,529.3): les~29 situations~30
# 102 Fixation 29 (281,580.6): violence~32
# 262 Fixation 30 (365.6,515.6): distance~26 nécessaire~27
# 150 Fixation 31 (542.8,515.5): dans~28 situations~30
# 178 Fixation 32 (507.6,454.4): de~21 terrain~22
# 293 Fixation 33 (377.1,455.6): personnels~20
# 172 Fixation 34 (445.2,405): L'objectif~15
# 322 Fixation 35 (616.6,411.3): est~16 permettre~18
# 182 Fixation 36 (341.4,483.4): personnels~20
# 226 Fixation 37 (498,464.3): de~21 terrain~22
# 112 Fixation 38 (606.3,469.3): de~23 garder~24
# 140 Fixation 39 (420.7,465.3): personnels~20
# 140 Fixation 40 (609.6,480.2): de~23 garder~24
# 230 Fixation 41 (590.1,532.2): dans~28 situations~30
# 126 Fixation 42 (612.3,486.7): de~23 garder~24
# 167 Fixation 43 (533.6,525.5): dans~28 situations~30
# 148 Fixation 44 (410.9,520.8): nécessaire~27
# 141 Fixation 45 (290.4,518.1): distance~26
# 163 Fixation 46 (540.9,530.6): dans~28 situations~30
# 143 Fixation 47 (648.9,532.6): les~29 situations~30
# 200 Fixation 48 (296.5,576.2): violence~32
# 164 Fixation 49 (617.2,533.5): les~29 situations~30
%%
# La~0 Fixations= ""
# société~1 Fixations= "5/"
# HLM~2 Fixations= "6/"
# 3F~3 Fixations= "6/"
# a~4 Fixations= "11/"
# organisé~5 Fixations= "11/"
# une~6 Fixations= "12/"
# formation~7 Fixations= "12/"
# de~8 Fixations= "13/"
# trois~9 Fixations= "14/"
# jours~10 Fixations= "15/"
# réservée~11 Fixations= "15/"
# aux~12 Fixations= "16/"
# gardiens~13 Fixations= "16/"
# d'immeubles~14 Fixations= "18/"
# L'objectif~15 Fixations= "34/"
# est~16 Fixations= "35/"
# de~17 Fixations= ""
# permettre~18 Fixations= "35/"
# aux~19 Fixations= "21/"
# personnels~20 Fixations= "39/"
# de~21 Fixations= "37/"
# terrain~22 Fixations= "37/"
# de~23 Fixations= "42/"
# garder~24 Fixations= "42/"
# la~25 Fixations= ""
# distance~26 Fixations= "45/"
# nécessaire~27 Fixations= "44/"
# dans~28 Fixations= "46/"
# les~29 Fixations= "49/"
# situations~30 Fixations= "49/"
# de~31 Fixations= ""
# violence~32 Fixations= "48/"
%%
#  La SOCIéTé[5] HLM[6] 3F[6] A[11] ORGANISé[11] UNE[12] FORMATION[12] DE[13] TROIS[14] JOURS[15] RéSERVéE[15] AUX[16] GARDIENS[16] D'IMMEUBLES[18] L'OBJECTIF[34] EST[35] de PERMETTRE[35] AUX[21] PERSONNELS[39] DE[37] TERRAIN[37] DE[42] GARDER[42] la DISTANCE[45] NéCESSAIRE[44] DANS[46] LES[49] SITUATIONS[49] de VIOLENCE[48]
# s04 delinquance_juvenile-m2 PercentageOfWordsProcessed= 0.878787878787879 PercentageOfWordsReallyProcessed= 0.878787878787879
