# 131 Fixation 1 (325.8,275.7): Le~0 chef~1
# 146 Fixation 2 (449.5,279.3): gouvernement~3
# 193 Fixation 3 (637.1,296.6): est~4 accusé~5
# 205 Fixation 4 (317.1,331.5): relations~7
# 148 Fixation 5 (495.3,336.5): tendues~8
# 214 Fixation 6 (618.1,345.2): les~10 États-Unis~11
# 132 Fixation 7 (363.3,384.8): l'isolement~13
# 163 Fixation 8 (303.2,390.6): l'isolement~13
# 106 Fixation 9 (454.6,390.6): d'Israël~14
# 295 Fixation 10 (613.6,407.5): dans~15 monde~17
# 209 Fixation 11 (413,448.2): en~20 montée~21
# 184 Fixation 12 (562.2,468.5): vertigineuse~22
# 175 Fixation 13 (672.1,466.9): et~23
# 160 Fixation 14 (483.3,526.8): de~27 toutes~28
# 137 Fixation 15 (605.9,534.4): les~29 normes~30
# 131 Fixation 16 (350.4,629.7): gouvernement~32
# 177 Fixation 17 (280.7,626): gouvernement~32
# 211 Fixation 18 (441.2,615.1): démocratique~33
# 214 Fixation 19 (409,341.3): tendues~8
%%
# Le~0 Fixations= "1/"
# chef~1 Fixations= "1/"
# du~2 Fixations= ""
# gouvernement~3 Fixations= "2/"
# est~4 Fixations= "3/"
# accusé~5 Fixations= "3/"
# des~6 Fixations= ""
# relations~7 Fixations= "4/"
# tendues~8 Fixations= "19/"
# avec~9 Fixations= ""
# les~10 Fixations= "6/"
# États-Unis~11 Fixations= "6/"
# de~12 Fixations= ""
# l'isolement~13 Fixations= "8/"
# d'Israël~14 Fixations= "9/"
# dans~15 Fixations= "10/"
# le~16 Fixations= ""
# monde~17 Fixations= "10/"
# du~18 Fixations= ""
# chômage~19 Fixations= ""
# en~20 Fixations= "11/"
# montée~21 Fixations= "11/"
# vertigineuse~22 Fixations= "12/"
# et~23 Fixations= "13/"
# de~24 Fixations= ""
# la~25 Fixations= ""
# détérioration~26 Fixations= ""
# de~27 Fixations= "14/"
# toutes~28 Fixations= "14/"
# les~29 Fixations= "15/"
# normes~30 Fixations= "15/"
# de~31 Fixations= ""
# gouvernement~32 Fixations= "17/"
# démocratique~33 Fixations= "18/"
%%
#  LE[1] CHEF[1] du GOUVERNEMENT[2] EST[3] ACCUSé[3] des RELATIONS[4] TENDUES[19] avec LES[6] ÉTATS-UNIS[6] de L'ISOLEMENT[8] D'ISRAëL[9] DANS[10] le MONDE[10] du chômage EN[11] MONTéE[11] VERTIGINEUSE[12] ET[13] de la détérioration DE[14] TOUTES[14] LES[15] NORMES[15] de GOUVERNEMENT[17] DéMOCRATIQUE[18]
# s21 conflit_israelo_palestinien-m1 PercentageOfWordsProcessed= 0.676470588235294 PercentageOfWordsReallyProcessed= 0.676470588235294
