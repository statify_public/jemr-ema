# 106 Fixation 1 (308.6,294.9): Comme~0 écho~2
# 149 Fixation 2 (414.7,287.7): écho~2 à~3
# 176 Fixation 3 (520.4,273.6): cette~4 assurance~5
# 190 Fixation 4 (628.9,275.2): assurance~5
# 83 Fixation 5 (680.5,280.6): retrouvée~6
# 170 Fixation 6 (390.9,289.3): en~1 écho~2
# 92 Fixation 7 (337.6,288.9): Comme~0 écho~2
# 253 Fixation 8 (470.3,284.2): à~3
# 161 Fixation 9 (549.3,277.7): cette~4 assurance~5
# 232 Fixation 10 (673.8,275.4): assurance~5 retrouvée~6
# 157 Fixation 11 (351.8,338.7): depuis~7 fin~9
# 158 Fixation 12 (513.1,349.1): la~11 guerre~12
# 203 Fixation 13 (646.1,329.9): la~13 commission~14
# 174 Fixation 14 (320.2,395.2): des~15 affaires~16
# 185 Fixation 15 (459.6,399.4): étrangères~17
# 143 Fixation 16 (585.3,401.3): du~18 Sénat~19
# 305 Fixation 17 (707.4,392.1): a~20 fixé~21
# 148 Fixation 18 (631.6,398.4): du~18 Sénat~19
# 139 Fixation 19 (728.8,398.1): a~20 fixé~21
# 96 Fixation 20 (338.7,446.3): date~23
# 139 Fixation 21 (279.1,470.6): date~23
# 136 Fixation 22 (445.3,466.2): le~25 début~26
# 171 Fixation 23 (550.7,462.8): des~27 auditions~28
# 115 Fixation 24 (667.2,458.1): auditions~28
# 199 Fixation 25 (301.1,527.1): nomination~31
# 173 Fixation 26 (446.2,527): qui~32 était~33
# 168 Fixation 27 (527.2,527.7): était~33 bloquée~34
# 160 Fixation 28 (636.2,536.7): bloquée~34 dix~36
# 125 Fixation 29 (258.4,588.9): mois~37
# 85 Fixation 30 (232.5,595.8): mois~37
%%
# Comme~0 Fixations= "7/"
# en~1 Fixations= "6/"
# écho~2 Fixations= "7/"
# à~3 Fixations= "8/"
# cette~4 Fixations= "9/"
# assurance~5 Fixations= "10/"
# retrouvée~6 Fixations= "10/"
# depuis~7 Fixations= "11/"
# la~8 Fixations= ""
# fin~9 Fixations= "11/"
# de~10 Fixations= ""
# la~11 Fixations= "12/"
# guerre~12 Fixations= "12/"
# la~13 Fixations= "13/"
# commission~14 Fixations= "13/"
# des~15 Fixations= "14/"
# affaires~16 Fixations= "14/"
# étrangères~17 Fixations= "15/"
# du~18 Fixations= "18/"
# Sénat~19 Fixations= "18/"
# a~20 Fixations= "19/"
# fixé~21 Fixations= "19/"
# une~22 Fixations= ""
# date~23 Fixations= "21/"
# pour~24 Fixations= ""
# le~25 Fixations= "22/"
# début~26 Fixations= "22/"
# des~27 Fixations= "23/"
# auditions~28 Fixations= "24/"
# et~29 Fixations= ""
# la~30 Fixations= ""
# nomination~31 Fixations= "25/"
# qui~32 Fixations= "26/"
# était~33 Fixations= "27/"
# bloquée~34 Fixations= "28/"
# depuis~35 Fixations= ""
# dix~36 Fixations= "28/"
# mois~37 Fixations= "30/"
%%
#  COMME[7] EN[6] éCHO[7] à[8] CETTE[9] ASSURANCE[10] RETROUVéE[10] DEPUIS[11] la FIN[11] de LA[12] GUERRE[12] LA[13] COMMISSION[13] DES[14] AFFAIRES[14] éTRANGèRES[15] DU[18] SéNAT[18] A[19] FIXé[19] une DATE[21] pour LE[22] DéBUT[22] DES[23] AUDITIONS[24] et la NOMINATION[25] QUI[26] éTAIT[27] BLOQUéE[28] depuis DIX[28] MOIS[30]
# s19 conflit_irak-m1 PercentageOfWordsProcessed= 0.815789473684211 PercentageOfWordsReallyProcessed= 0.815789473684211
