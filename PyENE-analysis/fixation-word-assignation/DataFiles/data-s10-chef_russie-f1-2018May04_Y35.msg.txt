# 132 Fixation 1 (301.8,282.2): A~0 Moscou~1
# 149 Fixation 2 (432.6,280.6): les~2 communiqués~3
# 124 Fixation 3 (522.6,280.6): communiqués~3
# 256 Fixation 4 (618.3,275.7): triomphants~4
# 142 Fixation 5 (471.6,437.6): complètement~13 encerclés~14
# 154 Fixation 6 (541,426.8): encerclés~14
# 144 Fixation 7 (640,416): leur~15 chef~16
# 217 Fixation 8 (300.5,467.7): militaire~17
# 126 Fixation 9 (414.7,462.8): est~18 blessé~19
# 89 Fixation 10 (454.2,466.8): est~18 blessé~19
# 133 Fixation 11 (567.9,466.9): les~20 islamistes~21
# 204 Fixation 12 (625.5,474.2): islamistes~21
# 88 Fixation 13 (690.5,485): islamistes~21 ont~22
# 131 Fixation 14 (334.6,516.6): subi~23 lourdes~25
# 172 Fixation 15 (428,524): lourdes~25 pertes~26
# 141 Fixation 16 (425.4,391.6): complètement~13
# 109 Fixation 17 (479.2,367.9): Dans~7 Daghestan~9
# 216 Fixation 18 (496.5,341.5): le~8 Daghestan~9
# 130 Fixation 19 (393.1,342.1): Dans~7
# 137 Fixation 20 (665.5,343.6): les~10 rebelles~11
# 85 Fixation 21 (704.3,348.1): les~10 rebelles~11
%%
# A~0 Fixations= "1/"
# Moscou~1 Fixations= "1/"
# les~2 Fixations= "2/"
# communiqués~3 Fixations= "3/"
# triomphants~4 Fixations= "4/"
# se~5 Fixations= ""
# succèdent~6 Fixations= ""
# Dans~7 Fixations= "19/"
# le~8 Fixations= "18/"
# Daghestan~9 Fixations= "18/"
# les~10 Fixations= "21/"
# rebelles~11 Fixations= "21/"
# sont~12 Fixations= ""
# complètement~13 Fixations= "16/"
# encerclés~14 Fixations= "6/"
# leur~15 Fixations= "7/"
# chef~16 Fixations= "7/"
# militaire~17 Fixations= "8/"
# est~18 Fixations= "10/"
# blessé~19 Fixations= "10/"
# les~20 Fixations= "11/"
# islamistes~21 Fixations= "13/"
# ont~22 Fixations= "13/"
# subi~23 Fixations= "14/"
# de~24 Fixations= ""
# lourdes~25 Fixations= "15/"
# pertes~26 Fixations= "15/"
%%
#  A[1] MOSCOU[1] LES[2] COMMUNIQUéS[3] TRIOMPHANTS[4] se succèdent DANS[19] LE[18] DAGHESTAN[18] LES[21] REBELLES[21] sont COMPLèTEMENT[16] ENCERCLéS[6] LEUR[7] CHEF[7] MILITAIRE[8] EST[10] BLESSé[10] LES[11] ISLAMISTES[13] ONT[13] SUBI[14] de LOURDES[15] PERTES[15]
# s10 chef_russie-f1 PercentageOfWordsProcessed= 0.851851851851852 PercentageOfWordsReallyProcessed= 0.851851851851852
