# 132 Fixation 1 (260.9,263.1): On~0 souvient~2
# 122 Fixation 2 (310.8,278.2): On~0 souvient~2
# 98 Fixation 3 (372.6,278.7): souvient~2
# 385 Fixation 4 (486.1,282.5): du~3 fameux~4
# 129 Fixation 5 (516.9,283.2): du~3 fameux~4
# 167 Fixation 6 (577.3,287.2): fameux~4 symbole~5
# 204 Fixation 7 (682.3,290.5): symbole~5 dollar~7
# 162 Fixation 8 (318.7,344.5): revu~8 Andy~10
# 133 Fixation 9 (286.1,359.2): revu~8
# 95 Fixation 10 (269.3,357.4): revu~8
# 171 Fixation 11 (397.2,346.5): par~9 Andy~10
# 113 Fixation 12 (448.9,353.2): Andy~10 Warhol~11
# 205 Fixation 13 (610.7,353): Le~12 pop-artiste~13
# 177 Fixation 14 (391.4,425.5): avait~15
# 149 Fixation 15 (275.5,417.5): américain~14
# 169 Fixation 16 (562.2,363): Le~12 pop-artiste~13
# 85 Fixation 17 (548.3,354.7): Le~12 pop-artiste~13
# 255 Fixation 18 (315.6,425.8): américain~14
# 268 Fixation 19 (576.6,406.8): signe~17
# 124 Fixation 20 (610.6,358.7): Le~12 pop-artiste~13
# 398 Fixation 21 (696.5,414.7): simple~18 amalgamé~19
# 215 Fixation 22 (290.8,473.1): l'art~20
# 180 Fixation 23 (420.9,477.6): l'argent~22
# 141 Fixation 24 (387.9,475.4): et~21 l'argent~22
%%
# On~0 Fixations= "2/"
# se~1 Fixations= ""
# souvient~2 Fixations= "3/"
# du~3 Fixations= "5/"
# fameux~4 Fixations= "6/"
# symbole~5 Fixations= "7/"
# du~6 Fixations= ""
# dollar~7 Fixations= "7/"
# revu~8 Fixations= "10/"
# par~9 Fixations= "11/"
# Andy~10 Fixations= "12/"
# Warhol~11 Fixations= "12/"
# Le~12 Fixations= "20/"
# pop-artiste~13 Fixations= "20/"
# américain~14 Fixations= "18/"
# avait~15 Fixations= "14/"
# d'un~16 Fixations= ""
# signe~17 Fixations= "19/"
# simple~18 Fixations= "21/"
# amalgamé~19 Fixations= "21/"
# l'art~20 Fixations= "22/"
# et~21 Fixations= "24/"
# l'argent~22 Fixations= "24/"
# pour~23 Fixations= ""
# la~24 Fixations= ""
# plus~25 Fixations= ""
# grande~26 Fixations= ""
# joie~27 Fixations= ""
# de~28 Fixations= ""
# ses~29 Fixations= ""
# collectionneurs~30 Fixations= ""
%%
#  ON[2] se SOUVIENT[3] DU[5] FAMEUX[6] SYMBOLE[7] du DOLLAR[7] REVU[10] PAR[11] ANDY[12] WARHOL[12] LE[20] POP-ARTISTE[20] AMéRICAIN[18] AVAIT[14] d'un SIGNE[19] SIMPLE[21] AMALGAMé[21] L'ART[22] ET[24] L'ARGENT[24] pour la plus grande joie de ses collectionneurs
# s01 art_contemporain-f1 PercentageOfWordsProcessed= 0.645161290322581 PercentageOfWordsReallyProcessed= 0.869565217391304
