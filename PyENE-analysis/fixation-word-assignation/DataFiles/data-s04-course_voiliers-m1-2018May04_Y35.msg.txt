# 117 Fixation 1 (381.2,338.3): soient~9
# 146 Fixation 2 (313.9,300.8): La~0 formule~1
# 204 Fixation 3 (447.5,290.4): 1~2 a~3 réclamé~4
# 156 Fixation 4 (575.9,281.8): de~5 pilotes~7
# 358 Fixation 5 (393.3,286.4): formule~1 1~2 a~3
# 363 Fixation 6 (369.1,290.2): formule~1 1~2
# 140 Fixation 7 (360.4,295.1): formule~1 1~2
# 151 Fixation 8 (469.3,288.1): a~3 réclamé~4
# 272 Fixation 9 (610.1,289.3): de~5 pilotes~7
# 147 Fixation 10 (578.7,288): de~5 pilotes~7
# 178 Fixation 11 (300.2,340.5): qu'ils~8
# 182 Fixation 12 (349.7,349.3): qu'ils~8 soient~9
# 187 Fixation 13 (467.9,351.2): les~10 plus~11
# 261 Fixation 14 (542.6,354.3): plus~11 rapides~12
# 176 Fixation 15 (305.7,397.1): moment~15
# 255 Fixation 16 (287.2,405.1): moment~15
# 204 Fixation 17 (398.7,400.2): la~16 discipline~17
# 275 Fixation 18 (369.4,353.3): soient~9
# 117 Fixation 19 (471.1,353): les~10 plus~11
# 297 Fixation 20 (421.1,298.7): 1~2 a~3
# 120 Fixation 21 (604,288.5): de~5 pilotes~7
# 237 Fixation 22 (306,299.1): La~0 formule~1
# 92 Fixation 23 (416.5,294): 1~2 a~3
# 287 Fixation 24 (578.1,288.3): de~5 pilotes~7
# 292 Fixation 25 (405.3,402.6): la~16 discipline~17
# 211 Fixation 26 (525.1,412.5): discipline~17 reine~18
# 158 Fixation 27 (625.9,410.1): reine~18 sport~20
# 248 Fixation 28 (321.5,473.4): automobile~21
# 160 Fixation 29 (276.1,469.4): automobile~21
# 245 Fixation 30 (399.6,468.5): exige~22
# 236 Fixation 31 (483.8,467.8): exige~22
# 154 Fixation 32 (613.3,467.8): de~24 conducteurs~26
# 196 Fixation 33 (683.2,471.1): ses~25 conducteurs~26
# 171 Fixation 34 (313.7,524): qu'ils~27
# 275 Fixation 35 (419.6,530.8): rongent~28
# 150 Fixation 36 (503.7,531.2): leur~29 frein~30
# 327 Fixation 37 (363,529.7): qu'ils~27 rongent~28
# 305 Fixation 38 (473.8,460.6): exige~22
# 325 Fixation 39 (417.9,466.8): exige~22
# 146 Fixation 40 (500,475.8): parfois~23
# 355 Fixation 41 (644.5,488.1): de~24 conducteurs~26
# 208 Fixation 42 (587.5,484.4): parfois~23 conducteurs~26
# 264 Fixation 43 (456.1,477.1): exige~22
# 109 Fixation 44 (648.3,487.7): de~24 conducteurs~26
# 357 Fixation 45 (428.1,416.4): la~16 discipline~17
# 254 Fixation 46 (578.8,426.3): reine~18
# 269 Fixation 47 (353.1,488.5): automobile~21 exige~22
%%
# La~0 Fixations= "22/"
# formule~1 Fixations= "22/"
# 1~2 Fixations= "23/"
# a~3 Fixations= "23/"
# réclamé~4 Fixations= "8/"
# de~5 Fixations= "24/"
# ses~6 Fixations= ""
# pilotes~7 Fixations= "24/"
# qu'ils~8 Fixations= "12/"
# soient~9 Fixations= "18/"
# les~10 Fixations= "19/"
# plus~11 Fixations= "19/"
# rapides~12 Fixations= "14/"
# à~13 Fixations= ""
# tout~14 Fixations= ""
# moment~15 Fixations= "16/"
# la~16 Fixations= "45/"
# discipline~17 Fixations= "45/"
# reine~18 Fixations= "46/"
# du~19 Fixations= ""
# sport~20 Fixations= "27/"
# automobile~21 Fixations= "47/"
# exige~22 Fixations= "47/"
# parfois~23 Fixations= "42/"
# de~24 Fixations= "44/"
# ses~25 Fixations= "33/"
# conducteurs~26 Fixations= "44/"
# qu'ils~27 Fixations= "37/"
# rongent~28 Fixations= "37/"
# leur~29 Fixations= "36/"
# frein~30 Fixations= "36/"
%%
#  LA[22] FORMULE[22] 1[23] A[23] RéCLAMé[8] DE[24] ses PILOTES[24] QU'ILS[12] SOIENT[18] LES[19] PLUS[19] RAPIDES[14] à tout MOMENT[16] LA[45] DISCIPLINE[45] REINE[46] du SPORT[27] AUTOMOBILE[47] EXIGE[47] PARFOIS[42] DE[44] SES[33] CONDUCTEURS[44] QU'ILS[37] RONGENT[37] LEUR[36] FREIN[36]
# s04 course_voiliers-m1 PercentageOfWordsProcessed= 0.870967741935484 PercentageOfWordsReallyProcessed= 0.870967741935484
