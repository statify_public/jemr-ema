# 106 Fixation 1 (276.7,290.7): Comme~0 écho~2
# 166 Fixation 2 (353.4,287.3): Comme~0 écho~2
# 96 Fixation 3 (384.1,283.5): en~1 écho~2
# 156 Fixation 4 (461.7,278.9): écho~2 à~3
# 159 Fixation 5 (575.3,277.6): assurance~5
# 280 Fixation 6 (691.3,288.4): retrouvée~6
# 313 Fixation 7 (682.7,298.5): retrouvée~6
# 108 Fixation 8 (708.9,303.1): retrouvée~6
# 113 Fixation 9 (295.4,353.3): depuis~7 fin~9
# 281 Fixation 10 (258.7,345.6): depuis~7 fin~9
# 141 Fixation 11 (307.9,343.4): depuis~7 fin~9
# 157 Fixation 12 (371.7,343.7): la~8 fin~9
# 223 Fixation 13 (493.2,349.1): de~10 guerre~12
# 188 Fixation 14 (635,358.1): la~13 commission~14
# 105 Fixation 15 (691.1,364.3): commission~14
# 218 Fixation 16 (280.2,391.3): des~15 affaires~16
# 251 Fixation 17 (430.7,412): affaires~16 étrangères~17
# 202 Fixation 18 (590.2,429.6): du~18 Sénat~19
# 231 Fixation 19 (682.8,432.8): Sénat~19 a~20
# 223 Fixation 20 (719,432.9): a~20 fixé~21
# 112 Fixation 21 (278.1,465.9): date~23
# 206 Fixation 22 (250.4,461.6): date~23
# 184 Fixation 23 (396.2,457.8): pour~24 début~26
# 118 Fixation 24 (456.7,468.1): le~25 début~26
# 291 Fixation 25 (580.5,481.6): des~27 auditions~28
# 165 Fixation 26 (571.2,481): des~27 auditions~28
# 239 Fixation 27 (687.8,492.5): et~29
# 189 Fixation 28 (239.3,519.4): nomination~31
# 137 Fixation 29 (324.3,518): nomination~31
# 153 Fixation 30 (421,515.4): qui~32 était~33
# 156 Fixation 31 (476.4,522): qui~32 était~33
# 200 Fixation 32 (544.1,529): était~33 bloquée~34
# 345 Fixation 33 (643.8,534.6): depuis~35 dix~36
# 186 Fixation 34 (653,540.4): depuis~35 dix~36
%%
# Comme~0 Fixations= "2/"
# en~1 Fixations= "3/"
# écho~2 Fixations= "4/"
# à~3 Fixations= "4/"
# cette~4 Fixations= ""
# assurance~5 Fixations= "5/"
# retrouvée~6 Fixations= "8/"
# depuis~7 Fixations= "11/"
# la~8 Fixations= "12/"
# fin~9 Fixations= "12/"
# de~10 Fixations= "13/"
# la~11 Fixations= ""
# guerre~12 Fixations= "13/"
# la~13 Fixations= "14/"
# commission~14 Fixations= "15/"
# des~15 Fixations= "16/"
# affaires~16 Fixations= "17/"
# étrangères~17 Fixations= "17/"
# du~18 Fixations= "18/"
# Sénat~19 Fixations= "19/"
# a~20 Fixations= "20/"
# fixé~21 Fixations= "20/"
# une~22 Fixations= ""
# date~23 Fixations= "22/"
# pour~24 Fixations= "23/"
# le~25 Fixations= "24/"
# début~26 Fixations= "24/"
# des~27 Fixations= "26/"
# auditions~28 Fixations= "26/"
# et~29 Fixations= "27/"
# la~30 Fixations= ""
# nomination~31 Fixations= "29/"
# qui~32 Fixations= "31/"
# était~33 Fixations= "32/"
# bloquée~34 Fixations= "32/"
# depuis~35 Fixations= "34/"
# dix~36 Fixations= "34/"
# mois~37 Fixations= ""
%%
#  COMME[2] EN[3] éCHO[4] à[4] cette ASSURANCE[5] RETROUVéE[8] DEPUIS[11] LA[12] FIN[12] DE[13] la GUERRE[13] LA[14] COMMISSION[15] DES[16] AFFAIRES[17] éTRANGèRES[17] DU[18] SéNAT[19] A[20] FIXé[20] une DATE[22] POUR[23] LE[24] DéBUT[24] DES[26] AUDITIONS[26] ET[27] la NOMINATION[29] QUI[31] éTAIT[32] BLOQUéE[32] DEPUIS[34] DIX[34] mois
# s06 conflit_irak-m1 PercentageOfWordsProcessed= 0.868421052631579 PercentageOfWordsReallyProcessed= 0.891891891891892
