# 94 Fixation 1 (278.8,283.2): Bien~0
# 114 Fixation 2 (456,292.3): que~2 connaissance~4
# 107 Fixation 3 (526.1,292.1): connaissance~4
# 139 Fixation 4 (671.3,291.5): d'une~5 langue~6
# 84 Fixation 5 (733.6,297.6): d'une~5 langue~6
# 154 Fixation 6 (272,351.8): morte~7
# 200 Fixation 7 (369.4,349.9): l'apprentissage~8
# 140 Fixation 8 (550.1,354.1): du~9 latin~10
# 161 Fixation 9 (621.8,349.1): du~9 latin~10
# 144 Fixation 10 (674.5,354): latin~10 dès~11
# 176 Fixation 11 (307.4,408.2): collège~13
# 94 Fixation 12 (337.9,405.6): collège~13
# 193 Fixation 13 (436.9,400.8): est~14 exercice~16
# 147 Fixation 14 (321.9,474): d'affirmation~20
# 133 Fixation 15 (330.2,472.3): d'affirmation~20
%%
# Bien~0 Fixations= "1/"
# plus~1 Fixations= ""
# que~2 Fixations= "2/"
# la~3 Fixations= ""
# connaissance~4 Fixations= "3/"
# d'une~5 Fixations= "5/"
# langue~6 Fixations= "5/"
# morte~7 Fixations= "6/"
# l'apprentissage~8 Fixations= "7/"
# du~9 Fixations= "9/"
# latin~10 Fixations= "10/"
# dès~11 Fixations= "10/"
# le~12 Fixations= ""
# collège~13 Fixations= "12/"
# est~14 Fixations= "13/"
# un~15 Fixations= ""
# exercice~16 Fixations= "13/"
# du~17 Fixations= ""
# raisonnement~18 Fixations= ""
# et~19 Fixations= ""
# d'affirmation~20 Fixations= "15/"
# du~21 Fixations= ""
# caractère~22 Fixations= ""
# Il~23 Fixations= ""
# est~24 Fixations= ""
# un~25 Fixations= ""
# puissant~26 Fixations= ""
# signe~27 Fixations= ""
# de~28 Fixations= ""
# différence~29 Fixations= ""
# et~30 Fixations= ""
# de~31 Fixations= ""
# condition~32 Fixations= ""
# supérieure~33 Fixations= ""
%%
#  BIEN[1] plus QUE[2] la CONNAISSANCE[3] D'UNE[5] LANGUE[5] MORTE[6] L'APPRENTISSAGE[7] DU[9] LATIN[10] DèS[10] le COLLèGE[12] EST[13] un EXERCICE[13] du raisonnement et D'AFFIRMATION[15] du caractère Il est un puissant signe de différence et de condition supérieure
# s02 formation_informatique-m1 PercentageOfWordsProcessed= 0.411764705882353 PercentageOfWordsReallyProcessed= 0.666666666666667
