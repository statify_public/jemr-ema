# 153 Fixation 1 (340.4,286.8): abords~1
# 178 Fixation 2 (466.6,293.2): de~2 gare~4
# 187 Fixation 3 (567.4,289): nulle~5
# 127 Fixation 4 (640.2,290.2): nulle~5 barre~6
# 215 Fixation 5 (320.8,338.3): logements~8
# 140 Fixation 6 (437.1,346.5): ni~9 cabine~10
# 138 Fixation 7 (521.2,346.8): cabine~10 téléphonique~11
# 134 Fixation 8 (582.3,350.5): téléphonique~11
# 141 Fixation 9 (702.9,347.4): dévastée~12
# 192 Fixation 10 (353.5,401.6): un~14 habitat~15
# 149 Fixation 11 (459.3,395.3): habitat~15 constitué~16
# 136 Fixation 12 (534.7,405.4): constitué~16
# 163 Fixation 13 (603.3,411.5): d'immeubles~17
# 176 Fixation 14 (406.2,444.6): et~19 maisons~21
# 161 Fixation 15 (511.3,448.2): de~20 maisons~21
# 135 Fixation 16 (621.9,461.3): en~22 pierre~23
# 198 Fixation 17 (329.4,497.5): réhabilités~18
# 167 Fixation 18 (420,500.9): et~19 maisons~21
# 187 Fixation 19 (555,512.1): et~26 ornées~27
# 152 Fixation 20 (688.5,522.8): de~28 charmants~29
%%
# Aux~0 Fixations= ""
# abords~1 Fixations= "1/"
# de~2 Fixations= "2/"
# la~3 Fixations= ""
# gare~4 Fixations= "2/"
# nulle~5 Fixations= "4/"
# barre~6 Fixations= "4/"
# de~7 Fixations= ""
# logements~8 Fixations= "5/"
# ni~9 Fixations= "6/"
# cabine~10 Fixations= "7/"
# téléphonique~11 Fixations= "8/"
# dévastée~12 Fixations= "9/"
# mais~13 Fixations= ""
# un~14 Fixations= "10/"
# habitat~15 Fixations= "11/"
# constitué~16 Fixations= "12/"
# d'immeubles~17 Fixations= "13/"
# réhabilités~18 Fixations= "17/"
# et~19 Fixations= "18/"
# de~20 Fixations= "15/"
# maisons~21 Fixations= "18/"
# en~22 Fixations= "16/"
# pierre~23 Fixations= "16/"
# meulière~24 Fixations= ""
# restaurées~25 Fixations= ""
# et~26 Fixations= "19/"
# ornées~27 Fixations= "19/"
# de~28 Fixations= "20/"
# charmants~29 Fixations= "20/"
# balcons~30 Fixations= ""
%%
#  Aux ABORDS[1] DE[2] la GARE[2] NULLE[4] BARRE[4] de LOGEMENTS[5] NI[6] CABINE[7] TéLéPHONIQUE[8] DéVASTéE[9] mais UN[10] HABITAT[11] CONSTITUé[12] D'IMMEUBLES[13] RéHABILITéS[17] ET[18] DE[15] MAISONS[18] EN[16] PIERRE[16] meulière restaurées ET[19] ORNéES[19] DE[20] CHARMANTS[20] balcons
# s08 rehabilitation_logement-f1 PercentageOfWordsProcessed= 0.774193548387097 PercentageOfWordsReallyProcessed= 0.8
