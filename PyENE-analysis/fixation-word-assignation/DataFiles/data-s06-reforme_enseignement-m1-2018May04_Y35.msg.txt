# 120 Fixation 1 (263.9,300.1): Afin~0
# 156 Fixation 2 (348.3,294.4): d'apprendre~1
# 101 Fixation 3 (412.5,295.1): d'apprendre~1
# 208 Fixation 4 (519.4,290.8): une~2 langue~3
# 159 Fixation 5 (621.7,296.1): langue~3 étrangère~4
# 177 Fixation 6 (673.2,303.3): étrangère~4
# 105 Fixation 7 (735,305.2): étrangère~4
# 238 Fixation 8 (272.1,349.1): ministère~6
# 217 Fixation 9 (414.8,353.8): entend~7
# 127 Fixation 10 (400.8,352.2): entend~7
# 193 Fixation 11 (296.7,346): ministère~6
# 122 Fixation 12 (318.2,346.2): ministère~6
# 179 Fixation 13 (421.8,345.5): entend~7
# 372 Fixation 14 (507.4,350.6): embaucher~8
# 174 Fixation 15 (655.6,361.2): des~9 locuteurs~10
# 144 Fixation 16 (695.1,358.7): locuteurs~10
# 98 Fixation 17 (292.3,386.9): natifs~11
# 379 Fixation 18 (242,389.9): natifs~11
# 121 Fixation 19 (320.6,396.9): natifs~11
# 228 Fixation 20 (410.6,400.3): pour~12 faire~13
# 351 Fixation 21 (504.9,407.5): faire~13 pratiquer~14
# 452 Fixation 22 (639.8,410.5): l'oral~15
# 158 Fixation 23 (688.3,409): l'oral~15
# 324 Fixation 24 (265.3,448.1): élèves~17
# 144 Fixation 25 (396.1,461.7): Cette~18 politique~19
# 265 Fixation 26 (463.3,458): politique~19
# 188 Fixation 27 (588.2,474): est~20 généralisée~21
# 237 Fixation 28 (637.1,472.4): est~20 généralisée~21
# 155 Fixation 29 (707.2,466.3): généralisée~21
# 176 Fixation 30 (285,523.2): pour~22
# 113 Fixation 31 (438.6,511.1): enseignements~24
# 135 Fixation 32 (414.4,523.6): enseignements~24
# 216 Fixation 33 (564.5,519.7): communs~25
# 254 Fixation 34 (609.2,530.8): communs~25
# 245 Fixation 35 (449.7,515.3): enseignements~24
# 84 Fixation 36 (503.9,521.6): enseignements~24 communs~25
# 234 Fixation 37 (610,530.4): communs~25
# 159 Fixation 38 (712.8,532.4): les~26 options~27
# 86 Fixation 39 (762.6,538.3): options~27
# 186 Fixation 40 (276.7,585.2): obligatoires~28
# 116 Fixation 41 (345.4,592.4): obligatoires~28
%%
# Afin~0 Fixations= "1/"
# d'apprendre~1 Fixations= "3/"
# une~2 Fixations= "4/"
# langue~3 Fixations= "5/"
# étrangère~4 Fixations= "7/"
# le~5 Fixations= ""
# ministère~6 Fixations= "12/"
# entend~7 Fixations= "13/"
# embaucher~8 Fixations= "14/"
# des~9 Fixations= "15/"
# locuteurs~10 Fixations= "16/"
# natifs~11 Fixations= "19/"
# pour~12 Fixations= "20/"
# faire~13 Fixations= "21/"
# pratiquer~14 Fixations= "21/"
# l'oral~15 Fixations= "23/"
# aux~16 Fixations= ""
# élèves~17 Fixations= "24/"
# Cette~18 Fixations= "25/"
# politique~19 Fixations= "26/"
# est~20 Fixations= "28/"
# généralisée~21 Fixations= "29/"
# pour~22 Fixations= "30/"
# les~23 Fixations= ""
# enseignements~24 Fixations= "36/"
# communs~25 Fixations= "37/"
# les~26 Fixations= "38/"
# options~27 Fixations= "39/"
# obligatoires~28 Fixations= "41/"
# et~29 Fixations= ""
# facultatives~30 Fixations= ""
%%
#  AFIN[1] D'APPRENDRE[3] UNE[4] LANGUE[5] éTRANGèRE[7] le MINISTèRE[12] ENTEND[13] EMBAUCHER[14] DES[15] LOCUTEURS[16] NATIFS[19] POUR[20] FAIRE[21] PRATIQUER[21] L'ORAL[23] aux éLèVES[24] CETTE[25] POLITIQUE[26] EST[28] GéNéRALISéE[29] POUR[30] les ENSEIGNEMENTS[36] COMMUNS[37] LES[38] OPTIONS[39] OBLIGATOIRES[41] et facultatives
# s06 reforme_enseignement-m1 PercentageOfWordsProcessed= 0.838709677419355 PercentageOfWordsReallyProcessed= 0.896551724137931
