# 152 Fixation 1 (302.3,284.6): Un~0 groupe~1
# 216 Fixation 2 (418.3,293.1): d'ingénieurs~2
# 332 Fixation 3 (568.6,298.1): météorologistes~3
# 136 Fixation 4 (605.4,299): météorologistes~3
# 137 Fixation 5 (652.3,301.9): météorologistes~3
# 87 Fixation 6 (709.2,298.8): météorologistes~3 a~4
# 145 Fixation 7 (317.1,348.7): obtenu~5
# 99 Fixation 8 (307.3,348.2): obtenu~5
# 164 Fixation 9 (393.8,343.9): le~6 prix~7
# 141 Fixation 10 (488.3,346.1): Nobel~8
# 106 Fixation 11 (556.8,347.8): pour~9 travaux~11
# 86 Fixation 12 (627.7,353.2): ses~10 travaux~11
# 149 Fixation 13 (353.9,397.5): relatifs~12 à~13
# 99 Fixation 14 (310.2,398.3): relatifs~12
# 189 Fixation 15 (445.8,400.5): la~14 prévision~15
# 186 Fixation 16 (638.3,408.2): des~16 évènements~17
# 94 Fixation 17 (610.6,409.3): des~16 évènements~17
# 161 Fixation 18 (339,455.8): climatiques~18
# 90 Fixation 19 (310.2,458.7): climatiques~18
# 205 Fixation 20 (467.1,455.3): à~19 long~20
# 132 Fixation 21 (537.5,457.7): terme~21
# 169 Fixation 22 (634.6,463.7): et~22 effets~24
# 215 Fixation 23 (686.2,470.6): aux~23 effets~24
# 113 Fixation 24 (407.6,532.7): pollution~27
# 166 Fixation 25 (336.6,534.7): pollution~27
%%
# Un~0 Fixations= "1/"
# groupe~1 Fixations= "1/"
# d'ingénieurs~2 Fixations= "2/"
# météorologistes~3 Fixations= "6/"
# a~4 Fixations= "6/"
# obtenu~5 Fixations= "8/"
# le~6 Fixations= "9/"
# prix~7 Fixations= "9/"
# Nobel~8 Fixations= "10/"
# pour~9 Fixations= "11/"
# ses~10 Fixations= "12/"
# travaux~11 Fixations= "12/"
# relatifs~12 Fixations= "14/"
# à~13 Fixations= "13/"
# la~14 Fixations= "15/"
# prévision~15 Fixations= "15/"
# des~16 Fixations= "17/"
# évènements~17 Fixations= "17/"
# climatiques~18 Fixations= "19/"
# à~19 Fixations= "20/"
# long~20 Fixations= "20/"
# terme~21 Fixations= "21/"
# et~22 Fixations= "22/"
# aux~23 Fixations= "23/"
# effets~24 Fixations= "23/"
# de~25 Fixations= ""
# la~26 Fixations= ""
# pollution~27 Fixations= "25/"
# de~28 Fixations= ""
# l'atmosphère~29 Fixations= ""
%%
#  UN[1] GROUPE[1] D'INGéNIEURS[2] MéTéOROLOGISTES[6] A[6] OBTENU[8] LE[9] PRIX[9] NOBEL[10] POUR[11] SES[12] TRAVAUX[12] RELATIFS[14] à[13] LA[15] PRéVISION[15] DES[17] éVèNEMENTS[17] CLIMATIQUES[19] à[20] LONG[20] TERME[21] ET[22] AUX[23] EFFETS[23] de la POLLUTION[25] de l'atmosphère
# s05 rechauffement_climatique-m2 PercentageOfWordsProcessed= 0.866666666666667 PercentageOfWordsReallyProcessed= 0.928571428571429
