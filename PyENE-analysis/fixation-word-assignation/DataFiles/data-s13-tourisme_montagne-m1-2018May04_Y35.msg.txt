# 164 Fixation 1 (281.6,283.7): Pour~0 vacanciers~2
# 186 Fixation 2 (358,282.6): les~1 vacanciers~2
# 396 Fixation 3 (401.2,291.9): vacanciers~2
# 231 Fixation 4 (545.2,288.1): qui~3 souhaitent~4
# 159 Fixation 5 (586.9,289.3): qui~3 souhaitent~4
# 126 Fixation 6 (711.6,285.9): avant~5
# 230 Fixation 7 (261.2,357.4): tout~6 reposer~8
# 286 Fixation 8 (361.6,352.8): se~7 reposer~8
# 182 Fixation 9 (485.1,355.8): dans~9 cadre~11
# 291 Fixation 10 (577.8,354.4): un~10 cadre~11
# 181 Fixation 11 (661.8,353.2): bucolique~12
# 267 Fixation 12 (645.1,354): cadre~11 bucolique~12
# 86 Fixation 13 (662.3,355.6): bucolique~12
# 218 Fixation 14 (426.5,364): reposer~8
# 167 Fixation 15 (293.3,382.9): les~13 résidences~14
# 479 Fixation 16 (322.8,404.4): les~13 résidences~14
# 125 Fixation 17 (351.1,394.7): résidences~14
# 262 Fixation 18 (475.2,393.6): proposent~15
# 288 Fixation 19 (639.9,388.7): un~16 hébergement~17
# 118 Fixation 20 (608.4,390.8): un~16 hébergement~17
# 232 Fixation 21 (327.3,456.1): adapté~18
# 200 Fixation 22 (391.1,447.9): leur~19 permettant~20
# 215 Fixation 23 (663.6,447.8): partir~22
# 169 Fixation 24 (413.1,530.4): sportifs~26
# 86 Fixation 25 (327.1,535.4): amis~25 sportifs~26
# 148 Fixation 26 (312,590.3): inactifs~31
%%
# Pour~0 Fixations= "1/"
# les~1 Fixations= "2/"
# vacanciers~2 Fixations= "3/"
# qui~3 Fixations= "5/"
# souhaitent~4 Fixations= "5/"
# avant~5 Fixations= "6/"
# tout~6 Fixations= "7/"
# se~7 Fixations= "8/"
# reposer~8 Fixations= "14/"
# dans~9 Fixations= "9/"
# un~10 Fixations= "10/"
# cadre~11 Fixations= "12/"
# bucolique~12 Fixations= "13/"
# les~13 Fixations= "16/"
# résidences~14 Fixations= "17/"
# proposent~15 Fixations= "18/"
# un~16 Fixations= "20/"
# hébergement~17 Fixations= "20/"
# adapté~18 Fixations= "21/"
# leur~19 Fixations= "22/"
# permettant~20 Fixations= "22/"
# de~21 Fixations= ""
# partir~22 Fixations= "23/"
# avec~23 Fixations= ""
# des~24 Fixations= ""
# amis~25 Fixations= "25/"
# sportifs~26 Fixations= "25/"
# sans~27 Fixations= ""
# pour~28 Fixations= ""
# autant~29 Fixations= ""
# rester~30 Fixations= ""
# inactifs~31 Fixations= "26/"
%%
#  POUR[1] LES[2] VACANCIERS[3] QUI[5] SOUHAITENT[5] AVANT[6] TOUT[7] SE[8] REPOSER[14] DANS[9] UN[10] CADRE[12] BUCOLIQUE[13] LES[16] RéSIDENCES[17] PROPOSENT[18] UN[20] HéBERGEMENT[20] ADAPTé[21] LEUR[22] PERMETTANT[22] de PARTIR[23] avec des AMIS[25] SPORTIFS[25] sans pour autant rester INACTIFS[26]
# s13 tourisme_montagne-m1 PercentageOfWordsProcessed= 0.78125 PercentageOfWordsReallyProcessed= 0.78125
