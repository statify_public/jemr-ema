# 86 Fixation 1 (315.3,294.3): Il~0 a~2
# 93 Fixation 2 (396.7,294.9): une~3 multitude~4
# 132 Fixation 3 (444.2,295.6): multitude~4
# 181 Fixation 4 (554.1,297.8): de~5 variantes~6
# 148 Fixation 5 (608.3,297.7): variantes~6
# 137 Fixation 6 (698.9,300.9): variantes~6 vont~8
# 138 Fixation 7 (268.1,349.1): de~9 l'impossibilité~10
# 158 Fixation 8 (330.1,351.2): l'impossibilité~10
# 164 Fixation 9 (402.1,350.8): l'impossibilité~10
# 144 Fixation 10 (528.2,356.5): pour~11 sonde~13
# 146 Fixation 11 (596.1,358.1): la~12 sonde~13
# 241 Fixation 12 (732.8,356.9): de~14 pointer~15
# 136 Fixation 13 (288,407.2): son~16 antenne~17
# 163 Fixation 14 (338.5,405.2): antenne~17
# 224 Fixation 15 (459.1,406.3): vers~18 à~20
# 85 Fixation 16 (539.4,413.4): nous~19 à~20
# 204 Fixation 17 (605.6,419.4): à~20 basculement~22
# 109 Fixation 18 (675.1,425.1): basculement~22
# 122 Fixation 19 (740.2,423.7): basculement~22
# 164 Fixation 20 (327.3,471.6): l'engin~24
# 122 Fixation 21 (300.2,472.9): l'engin~24
# 258 Fixation 22 (437.7,466.9): qui~25 aurait~26
# 155 Fixation 23 (539.4,469.1): été~27
# 170 Fixation 24 (595.2,475.2): déséquilibré~28
# 83 Fixation 25 (696.4,482.5): déséquilibré~28
# 112 Fixation 26 (746.3,485.8): par~29
# 139 Fixation 27 (329,522.1): accident~31
# 129 Fixation 28 (289.5,534.3): accident~31
# 197 Fixation 29 (427.9,534.4): de~32 terrain~33
# 144 Fixation 30 (415.8,530): de~32 terrain~33
# 269 Fixation 31 (365.6,477.9): l'engin~24 aurait~26
# 145 Fixation 32 (360.1,337.3): l'impossibilité~10
# 144 Fixation 33 (369.9,350.8): l'impossibilité~10
%%
# Il~0 Fixations= "1/"
# y~1 Fixations= ""
# a~2 Fixations= "1/"
# une~3 Fixations= "2/"
# multitude~4 Fixations= "3/"
# de~5 Fixations= "4/"
# variantes~6 Fixations= "6/"
# qui~7 Fixations= ""
# vont~8 Fixations= "6/"
# de~9 Fixations= "7/"
# l'impossibilité~10 Fixations= "33/"
# pour~11 Fixations= "10/"
# la~12 Fixations= "11/"
# sonde~13 Fixations= "11/"
# de~14 Fixations= "12/"
# pointer~15 Fixations= "12/"
# son~16 Fixations= "13/"
# antenne~17 Fixations= "14/"
# vers~18 Fixations= "15/"
# nous~19 Fixations= "16/"
# à~20 Fixations= "17/"
# un~21 Fixations= ""
# basculement~22 Fixations= "19/"
# de~23 Fixations= ""
# l'engin~24 Fixations= "31/"
# qui~25 Fixations= "22/"
# aurait~26 Fixations= "31/"
# été~27 Fixations= "23/"
# déséquilibré~28 Fixations= "25/"
# par~29 Fixations= "26/"
# un~30 Fixations= ""
# accident~31 Fixations= "28/"
# de~32 Fixations= "30/"
# terrain~33 Fixations= "30/"
%%
#  IL[1] y A[1] UNE[2] MULTITUDE[3] DE[4] VARIANTES[6] qui VONT[6] DE[7] L'IMPOSSIBILITé[33] POUR[10] LA[11] SONDE[11] DE[12] POINTER[12] SON[13] ANTENNE[14] VERS[15] NOUS[16] à[17] un BASCULEMENT[19] de L'ENGIN[31] QUI[22] AURAIT[31] éTé[23] DéSéQUILIBRé[25] PAR[26] un ACCIDENT[28] DE[30] TERRAIN[30]
# s02 observation_planetes-m1 PercentageOfWordsProcessed= 0.852941176470588 PercentageOfWordsReallyProcessed= 0.852941176470588
