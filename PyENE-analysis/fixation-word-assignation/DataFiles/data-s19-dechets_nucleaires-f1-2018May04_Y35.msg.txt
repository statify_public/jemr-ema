# 228 Fixation 1 (313.1,281.4): La~0 CRIIRAD~1
# 185 Fixation 2 (423.3,273): a~2 dénoncé~3
# 165 Fixation 3 (570.3,269.1): la~4 sous-évaluation~5
# 151 Fixation 4 (645.9,273.2): sous-évaluation~5
# 200 Fixation 5 (287.7,336.4): la~7 Cogema~8
# 217 Fixation 6 (453.2,331.9): de~9 radioactivité~11
# 334 Fixation 7 (644.2,331.4): produite~12
# 163 Fixation 8 (304.8,379.9): la~7 Cogema~8
%%
# La~0 Fixations= "1/"
# CRIIRAD~1 Fixations= "1/"
# a~2 Fixations= "2/"
# dénoncé~3 Fixations= "2/"
# la~4 Fixations= "3/"
# sous-évaluation~5 Fixations= "4/"
# par~6 Fixations= ""
# la~7 Fixations= "8/"
# Cogema~8 Fixations= "8/"
# de~9 Fixations= "6/"
# la~10 Fixations= ""
# radioactivité~11 Fixations= "6/"
# produite~12 Fixations= "7/"
# par~13 Fixations= ""
# des~14 Fixations= ""
# eaux~15 Fixations= ""
# de~16 Fixations= ""
# ruissellement~17 Fixations= ""
# d'anciennes~18 Fixations= ""
# mines~19 Fixations= ""
# d'uranium~20 Fixations= ""
# en~21 Fixations= ""
# Loire-atlantique~22 Fixations= ""
%%
#  LA[1] CRIIRAD[1] A[2] DéNONCé[2] LA[3] SOUS-éVALUATION[4] par LA[8] COGEMA[8] DE[6] la RADIOACTIVITé[6] PRODUITE[7] par des eaux de ruissellement d'anciennes mines d'uranium en Loire-atlantique
# s19 dechets_nucleaires-f1 PercentageOfWordsProcessed= 0.478260869565217 PercentageOfWordsReallyProcessed= 0.846153846153846
