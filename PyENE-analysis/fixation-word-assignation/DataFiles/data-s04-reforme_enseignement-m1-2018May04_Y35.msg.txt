# 142 Fixation 1 (325.1,300.5): Afin~0 d'apprendre~1
# 151 Fixation 2 (406,298.8): d'apprendre~1
# 188 Fixation 3 (530.3,296): une~2 langue~3
# 250 Fixation 4 (650.9,293.9): étrangère~4
# 263 Fixation 5 (315.8,337.4): ministère~6
# 225 Fixation 6 (444.1,339.2): entend~7
# 193 Fixation 7 (689.5,297.6): étrangère~4
# 250 Fixation 8 (416.2,358): entend~7
# 196 Fixation 9 (499.3,370.5): embaucher~8
# 407 Fixation 10 (632.4,372.1): des~9 locuteurs~10
# 224 Fixation 11 (335.4,420.7): natifs~11
# 163 Fixation 12 (291.8,416.6): natifs~11
# 117 Fixation 13 (453.5,419.5): faire~13
# 148 Fixation 14 (626.9,418.3): l'oral~15
# 237 Fixation 15 (288.4,460): élèves~17
# 94 Fixation 16 (430.1,459.1): Cette~18 politique~19
# 235 Fixation 17 (609.8,470.6): est~20 généralisée~21
# 111 Fixation 18 (588.7,473.5): est~20 généralisée~21
# 85 Fixation 19 (654.3,473.3): généralisée~21
# 176 Fixation 20 (352.2,518.7): les~23 enseignements~24
# 127 Fixation 21 (467.6,532): enseignements~24
# 217 Fixation 22 (577.4,538.5): communs~25
# 212 Fixation 23 (713.3,549.4): les~26 options~27
# 185 Fixation 24 (364.2,604): obligatoires~28
# 97 Fixation 25 (312.5,607.9): obligatoires~28
# 148 Fixation 26 (511.4,613.4): facultatives~30
# 239 Fixation 27 (692.9,534.4): les~26 options~27
# 265 Fixation 28 (395.1,603.8): obligatoires~28
# 219 Fixation 29 (499.2,603.1): facultatives~30
# 193 Fixation 30 (495.8,604.2): et~29 facultatives~30
%%
# Afin~0 Fixations= "1/"
# d'apprendre~1 Fixations= "2/"
# une~2 Fixations= "3/"
# langue~3 Fixations= "3/"
# étrangère~4 Fixations= "7/"
# le~5 Fixations= ""
# ministère~6 Fixations= "5/"
# entend~7 Fixations= "8/"
# embaucher~8 Fixations= "9/"
# des~9 Fixations= "10/"
# locuteurs~10 Fixations= "10/"
# natifs~11 Fixations= "12/"
# pour~12 Fixations= ""
# faire~13 Fixations= "13/"
# pratiquer~14 Fixations= ""
# l'oral~15 Fixations= "14/"
# aux~16 Fixations= ""
# élèves~17 Fixations= "15/"
# Cette~18 Fixations= "16/"
# politique~19 Fixations= "16/"
# est~20 Fixations= "18/"
# généralisée~21 Fixations= "19/"
# pour~22 Fixations= ""
# les~23 Fixations= "20/"
# enseignements~24 Fixations= "21/"
# communs~25 Fixations= "22/"
# les~26 Fixations= "27/"
# options~27 Fixations= "27/"
# obligatoires~28 Fixations= "28/"
# et~29 Fixations= "30/"
# facultatives~30 Fixations= "30/"
%%
#  AFIN[1] D'APPRENDRE[2] UNE[3] LANGUE[3] éTRANGèRE[7] le MINISTèRE[5] ENTEND[8] EMBAUCHER[9] DES[10] LOCUTEURS[10] NATIFS[12] pour FAIRE[13] pratiquer L'ORAL[14] aux éLèVES[15] CETTE[16] POLITIQUE[16] EST[18] GéNéRALISéE[19] pour LES[20] ENSEIGNEMENTS[21] COMMUNS[22] LES[27] OPTIONS[27] OBLIGATOIRES[28] ET[30] FACULTATIVES[30]
# s04 reforme_enseignement-m1 PercentageOfWordsProcessed= 0.838709677419355 PercentageOfWordsReallyProcessed= 0.838709677419355
