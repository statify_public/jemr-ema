# 166 Fixation 1 (465.7,270.7): Louis~2
# 216 Fixation 2 (408.1,276.3): de~1 Louis~2
# 206 Fixation 3 (638.1,283.5): le~4 premier~5
# 133 Fixation 4 (264,352.1): aviateur~6
# 81 Fixation 5 (320.4,348.7): aviateur~6 à~7
# 199 Fixation 6 (403.6,344.5): à~7 avoir~8
# 159 Fixation 7 (511.8,353.6): traversé~9
# 194 Fixation 8 (630.6,361.6): la~10 manche~11
# 99 Fixation 9 (326.7,378.3): aviateur~6 à~7
# 164 Fixation 10 (262.1,389): avion~13
# 119 Fixation 11 (455.6,416.3): 1909~15 a~16 été~17
# 132 Fixation 12 (524.2,417.5): été~17 reproduit~18
# 132 Fixation 13 (585.1,422.7): reproduit~18
# 286 Fixation 14 (671.5,426.5): dimanche~19
# 194 Fixation 15 (260.9,446.5): juillet~21
# 202 Fixation 16 (476.2,458.4): ans~23 jour~24
# 166 Fixation 17 (623.7,463.8): jour~26
# 164 Fixation 18 (665.9,470.9): jour~26 aprés~27
# 105 Fixation 19 (734.1,471): aprés~27
# 252 Fixation 20 (297.8,522.6): un~29 pilote~30
# 234 Fixation 21 (408.3,524.6): de~31 ligne~32
# 306 Fixation 22 (531.3,525.8): suédois~33
%%
# L'exploit~0 Fixations= ""
# de~1 Fixations= "2/"
# Louis~2 Fixations= "2/"
# Blériot~3 Fixations= ""
# le~4 Fixations= "3/"
# premier~5 Fixations= "3/"
# aviateur~6 Fixations= "9/"
# à~7 Fixations= "9/"
# avoir~8 Fixations= "6/"
# traversé~9 Fixations= "7/"
# la~10 Fixations= "8/"
# manche~11 Fixations= "8/"
# en~12 Fixations= ""
# avion~13 Fixations= "10/"
# en~14 Fixations= ""
# 1909~15 Fixations= "11/"
# a~16 Fixations= "11/"
# été~17 Fixations= "12/"
# reproduit~18 Fixations= "13/"
# dimanche~19 Fixations= "14/"
# 25~20 Fixations= ""
# juillet~21 Fixations= "15/"
# 90~22 Fixations= ""
# ans~23 Fixations= "16/"
# jour~24 Fixations= "16/"
# pour~25 Fixations= ""
# jour~26 Fixations= "18/"
# aprés~27 Fixations= "19/"
# par~28 Fixations= ""
# un~29 Fixations= "20/"
# pilote~30 Fixations= "20/"
# de~31 Fixations= "21/"
# ligne~32 Fixations= "21/"
# suédois~33 Fixations= "22/"
%%
#  L'exploit DE[2] LOUIS[2] Blériot LE[3] PREMIER[3] AVIATEUR[9] à[9] AVOIR[6] TRAVERSé[7] LA[8] MANCHE[8] en AVION[10] en 1909[11] A[11] éTé[12] REPRODUIT[13] DIMANCHE[14] 25 JUILLET[15] 90 ANS[16] JOUR[16] pour JOUR[18] APRéS[19] par UN[20] PILOTE[20] DE[21] LIGNE[21] SUéDOIS[22]
# s06 salon_aeronautique-m1 PercentageOfWordsProcessed= 0.764705882352941 PercentageOfWordsReallyProcessed= 0.764705882352941
