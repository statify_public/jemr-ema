# 220 Fixation 1 (290.2,285.7): La~0 pianiste~1
# 168 Fixation 2 (431,275.3): américaine~2
# 234 Fixation 3 (546.4,277.7): américaine~2 Rosalyn~3
# 195 Fixation 4 (591.2,401): quatre-vingt-quatre~9
%%
# La~0 Fixations= "1/"
# pianiste~1 Fixations= "1/"
# américaine~2 Fixations= "3/"
# Rosalyn~3 Fixations= "3/"
# Tureck~4 Fixations= ""
# fait~5 Fixations= ""
# son~6 Fixations= ""
# entrée~7 Fixations= ""
# à~8 Fixations= ""
# quatre-vingt-quatre~9 Fixations= "4/"
# ans~10 Fixations= ""
# dans~11 Fixations= ""
# le~12 Fixations= ""
# répertoire~13 Fixations= ""
# discographique~14 Fixations= ""
# de~15 Fixations= ""
# Deutsche~16 Fixations= ""
# Grammophon~17 Fixations= ""
# Son~18 Fixations= ""
# interprétation~19 Fixations= ""
# des~20 Fixations= ""
# variations~21 Fixations= ""
# Goldberg~22 Fixations= ""
# de~23 Fixations= ""
# Bach~24 Fixations= ""
# va~25 Fixations= ""
# la~26 Fixations= ""
# faire~27 Fixations= ""
# connaître~28 Fixations= ""
# au~29 Fixations= ""
# grand~30 Fixations= ""
# public~31 Fixations= ""
%%
#  LA[1] PIANISTE[1] AMéRICAINE[3] ROSALYN[3] Tureck fait son entrée à QUATRE-VINGT-QUATRE[4] ans dans le répertoire discographique de Deutsche Grammophon Son interprétation des variations Goldberg de Bach va la faire connaître au grand public
# s18 allocation_familiale-a2 PercentageOfWordsProcessed= 0.15625 PercentageOfWordsReallyProcessed= 0.5
