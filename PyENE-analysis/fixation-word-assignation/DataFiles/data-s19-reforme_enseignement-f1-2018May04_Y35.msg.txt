# 101 Fixation 1 (302.9,281.2): La~0 critique~1
# 156 Fixation 2 (478.2,262.7): s'est~2 exprimée~3
# 90 Fixation 3 (557.7,263.9): exprimée~3
# 159 Fixation 4 (279,345.8): radicalement~5
# 144 Fixation 5 (462.8,344): sur~6 nature~8
# 85 Fixation 6 (571.2,326.4): nature~8
# 150 Fixation 7 (678.4,334.7): l'idéologie~10
# 151 Fixation 8 (313.4,407.1): du~11 projet~12
# 150 Fixation 9 (419.9,403.4): éducatif~13
# 278 Fixation 10 (563.3,400): Dans~14 débat~16
# 127 Fixation 11 (610.8,399.4): ce~15 débat~16
# 172 Fixation 12 (294.1,463): déclenché~17
# 161 Fixation 13 (432.9,466.8): en~18 dehors~19
# 178 Fixation 14 (583.7,470): des~20 organisations~21
# 172 Fixation 15 (291.3,535.1): syndicales~22
# 200 Fixation 16 (489.8,538): les~23 enseignants~24
# 150 Fixation 17 (665.6,543): s'en~25 prennent~26
# 155 Fixation 18 (301.6,596.2): aux~27 orientations~28
%%
# La~0 Fixations= "1/"
# critique~1 Fixations= "1/"
# s'est~2 Fixations= "2/"
# exprimée~3 Fixations= "3/"
# plus~4 Fixations= ""
# radicalement~5 Fixations= "4/"
# sur~6 Fixations= "5/"
# la~7 Fixations= ""
# nature~8 Fixations= "6/"
# et~9 Fixations= ""
# l'idéologie~10 Fixations= "7/"
# du~11 Fixations= "8/"
# projet~12 Fixations= "8/"
# éducatif~13 Fixations= "9/"
# Dans~14 Fixations= "10/"
# ce~15 Fixations= "11/"
# débat~16 Fixations= "11/"
# déclenché~17 Fixations= "12/"
# en~18 Fixations= "13/"
# dehors~19 Fixations= "13/"
# des~20 Fixations= "14/"
# organisations~21 Fixations= "14/"
# syndicales~22 Fixations= "15/"
# les~23 Fixations= "16/"
# enseignants~24 Fixations= "16/"
# s'en~25 Fixations= "17/"
# prennent~26 Fixations= "17/"
# aux~27 Fixations= "18/"
# orientations~28 Fixations= "18/"
# fondamentales~29 Fixations= ""
%%
#  LA[1] CRITIQUE[1] S'EST[2] EXPRIMéE[3] plus RADICALEMENT[4] SUR[5] la NATURE[6] et L'IDéOLOGIE[7] DU[8] PROJET[8] éDUCATIF[9] DANS[10] CE[11] DéBAT[11] DéCLENCHé[12] EN[13] DEHORS[13] DES[14] ORGANISATIONS[14] SYNDICALES[15] LES[16] ENSEIGNANTS[16] S'EN[17] PRENNENT[17] AUX[18] ORIENTATIONS[18] fondamentales
# s19 reforme_enseignement-f1 PercentageOfWordsProcessed= 0.866666666666667 PercentageOfWordsReallyProcessed= 0.896551724137931
