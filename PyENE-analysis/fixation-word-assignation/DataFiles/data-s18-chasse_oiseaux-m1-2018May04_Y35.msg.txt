# 102 Fixation 1 (267.7,270.8): L'ortolan~0
# 130 Fixation 2 (324.7,282.5): L'ortolan~0
# 149 Fixation 3 (288.3,282.6): L'ortolan~0
# 177 Fixation 4 (400,281.2): petit~1
# 129 Fixation 5 (470.9,280.3): petit~1 oiseau~2
# 325 Fixation 6 (586.2,279.4): très~3
# 169 Fixation 7 (676.4,283.2): prisé~4
# 172 Fixation 8 (379.5,328.1): gourmets~7 Sud-ouest~9
# 230 Fixation 9 (560,346.1): appartiendra~10
# 106 Fixation 10 (691.4,343.4): appartiendra~10 bientôt~11
# 184 Fixation 11 (340.9,399.9): espèces~13
# 187 Fixation 12 (382.7,406.1): espèces~13
# 167 Fixation 13 (574,414.8): Un~15 décret~16
# 107 Fixation 14 (642.4,415.2): décret~16 a~17
# 181 Fixation 15 (323.9,463.9): signé~19
# 161 Fixation 16 (392.2,461.4): qui~20 représente~21
# 217 Fixation 17 (553.5,464): le~22 premier~23
# 114 Fixation 18 (641.1,464.6): premier~23 acte~24
# 198 Fixation 19 (317.5,515.1): compromis~26
# 162 Fixation 20 (347.7,523): compromis~26 global~27
# 116 Fixation 21 (265.6,616.4): chasseurs~31
# 199 Fixation 22 (521.4,530.8): entre~28 écologistes~29
%%
# L'ortolan~0 Fixations= "3/"
# petit~1 Fixations= "5/"
# oiseau~2 Fixations= "5/"
# très~3 Fixations= "6/"
# prisé~4 Fixations= "7/"
# par~5 Fixations= ""
# les~6 Fixations= ""
# gourmets~7 Fixations= "8/"
# du~8 Fixations= ""
# Sud-ouest~9 Fixations= "8/"
# appartiendra~10 Fixations= "10/"
# bientôt~11 Fixations= "10/"
# aux~12 Fixations= ""
# espèces~13 Fixations= "12/"
# protégées~14 Fixations= ""
# Un~15 Fixations= "13/"
# décret~16 Fixations= "14/"
# a~17 Fixations= "14/"
# été~18 Fixations= ""
# signé~19 Fixations= "15/"
# qui~20 Fixations= "16/"
# représente~21 Fixations= "16/"
# le~22 Fixations= "17/"
# premier~23 Fixations= "18/"
# acte~24 Fixations= "18/"
# d'un~25 Fixations= ""
# compromis~26 Fixations= "20/"
# global~27 Fixations= "20/"
# entre~28 Fixations= "22/"
# écologistes~29 Fixations= "22/"
# et~30 Fixations= ""
# chasseurs~31 Fixations= "21/"
%%
#  L'ORTOLAN[3] PETIT[5] OISEAU[5] TRèS[6] PRISé[7] par les GOURMETS[8] du SUD-OUEST[8] APPARTIENDRA[10] BIENTôT[10] aux ESPèCES[12] protégées UN[13] DéCRET[14] A[14] été SIGNé[15] QUI[16] REPRéSENTE[16] LE[17] PREMIER[18] ACTE[18] d'un COMPROMIS[20] GLOBAL[20] ENTRE[22] éCOLOGISTES[22] et CHASSEURS[21]
# s18 chasse_oiseaux-m1 PercentageOfWordsProcessed= 0.75 PercentageOfWordsReallyProcessed= 0.75
