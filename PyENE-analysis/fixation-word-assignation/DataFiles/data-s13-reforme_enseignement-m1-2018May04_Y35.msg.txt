# 193 Fixation 1 (283.3,287.1): Afin~0
# 359 Fixation 2 (362.4,290.3): d'apprendre~1
# 179 Fixation 3 (513.1,282.5): une~2 langue~3
# 166 Fixation 4 (554.7,281.3): une~2 langue~3
# 358 Fixation 5 (648.3,283.5): étrangère~4
# 118 Fixation 6 (735.3,289.6): étrangère~4
# 260 Fixation 7 (285.7,344.6): ministère~6
# 126 Fixation 8 (312.7,334.3): ministère~6
# 299 Fixation 9 (426.5,335.8): entend~7
# 333 Fixation 10 (512.2,342.4): embaucher~8
# 264 Fixation 11 (668.2,348.1): des~9 locuteurs~10
# 260 Fixation 12 (313.5,388.6): natifs~11
# 187 Fixation 13 (301,400.7): natifs~11
# 113 Fixation 14 (598.1,350.8): embaucher~8
# 182 Fixation 15 (657.6,345.8): des~9 locuteurs~10
# 126 Fixation 16 (683.7,347): des~9 locuteurs~10
# 360 Fixation 17 (496.1,400.5): faire~13 pratiquer~14
# 302 Fixation 18 (661,413): l'oral~15
# 82 Fixation 19 (631.8,412.7): l'oral~15
# 157 Fixation 20 (701.8,410.5): l'oral~15
# 178 Fixation 21 (296.2,480.6): élèves~17
# 156 Fixation 22 (276.4,471.8): élèves~17
# 211 Fixation 23 (399,484.4): Cette~18 politique~19
# 244 Fixation 24 (469.9,475.4): politique~19
# 87 Fixation 25 (645.6,625.8):
# 219 Fixation 26 (312.4,581.2): obligatoires~28
# 189 Fixation 27 (346.3,605.7): obligatoires~28
# 221 Fixation 28 (495.3,598.8): et~29 facultatives~30
# 232 Fixation 29 (531.5,595.4): facultatives~30
# 208 Fixation 30 (504.2,594.8): facultatives~30
%%
# Afin~0 Fixations= "1/"
# d'apprendre~1 Fixations= "2/"
# une~2 Fixations= "4/"
# langue~3 Fixations= "4/"
# étrangère~4 Fixations= "6/"
# le~5 Fixations= ""
# ministère~6 Fixations= "8/"
# entend~7 Fixations= "9/"
# embaucher~8 Fixations= "14/"
# des~9 Fixations= "16/"
# locuteurs~10 Fixations= "16/"
# natifs~11 Fixations= "13/"
# pour~12 Fixations= ""
# faire~13 Fixations= "17/"
# pratiquer~14 Fixations= "17/"
# l'oral~15 Fixations= "20/"
# aux~16 Fixations= ""
# élèves~17 Fixations= "22/"
# Cette~18 Fixations= "23/"
# politique~19 Fixations= "24/"
# est~20 Fixations= ""
# généralisée~21 Fixations= ""
# pour~22 Fixations= ""
# les~23 Fixations= ""
# enseignements~24 Fixations= ""
# communs~25 Fixations= ""
# les~26 Fixations= ""
# options~27 Fixations= ""
# obligatoires~28 Fixations= "27/"
# et~29 Fixations= "28/"
# facultatives~30 Fixations= "30/"
%%
#  AFIN[1] D'APPRENDRE[2] UNE[4] LANGUE[4] éTRANGèRE[6] le MINISTèRE[8] ENTEND[9] EMBAUCHER[14] DES[16] LOCUTEURS[16] NATIFS[13] pour FAIRE[17] PRATIQUER[17] L'ORAL[20] aux éLèVES[22] CETTE[23] POLITIQUE[24] est généralisée pour les enseignements communs les options OBLIGATOIRES[27] ET[28] FACULTATIVES[30]
# s13 reforme_enseignement-m1 PercentageOfWordsProcessed= 0.645161290322581 PercentageOfWordsReallyProcessed= 0.645161290322581
