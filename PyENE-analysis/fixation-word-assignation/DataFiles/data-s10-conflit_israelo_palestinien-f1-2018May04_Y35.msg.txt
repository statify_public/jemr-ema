# 138 Fixation 1 (289.9,284.7): L'artillerie~0
# 119 Fixation 2 (396.5,274.8): L'artillerie~0 israélienne~1
# 188 Fixation 3 (471.2,268): israélienne~1
# 104 Fixation 4 (555.3,263.4): israélienne~1 a~2
# 204 Fixation 5 (635,262.6): a~2 massivement~3
# 136 Fixation 6 (477.5,474.9): riposte~19 à~20
# 153 Fixation 7 (570.6,473.1): une~21 série~22
# 130 Fixation 8 (649.9,481.7): d'attaques~23
# 160 Fixation 9 (313.2,538): meurtrières~24
# 213 Fixation 10 (485.2,528.9): la~26 milice~27
# 222 Fixation 11 (594.9,535.5): milice~27 intégriste~28
# 215 Fixation 12 (348.5,592.3): contre~30 Israël~31
# 149 Fixation 13 (714,548.6): intégriste~28 Chiite~29
# 133 Fixation 14 (731.3,546.2): Chiite~29
# 102 Fixation 15 (338.9,604.3): contre~30 Israël~31
# 167 Fixation 16 (274.2,591.6): contre~30 Israël~31
# 87 Fixation 17 (367.7,558.8): meurtrières~24
# 126 Fixation 18 (533,418.8): Hezbollah~12 Liban~14
# 177 Fixation 19 (608.4,419.9): au~13 Liban~14
# 279 Fixation 20 (677.2,412.3): Liban~14 tirs~16
# 125 Fixation 21 (342.6,402.8): bastions~10
# 195 Fixation 22 (291.7,399.6): bastions~10
# 199 Fixation 23 (411.2,401.1): du~11 Hezbollah~12
%%
# L'artillerie~0 Fixations= "2/"
# israélienne~1 Fixations= "4/"
# a~2 Fixations= "5/"
# massivement~3 Fixations= "5/"
# bombardé~4 Fixations= ""
# des~5 Fixations= ""
# régions~6 Fixations= ""
# supposées~7 Fixations= ""
# être~8 Fixations= ""
# des~9 Fixations= ""
# bastions~10 Fixations= "22/"
# du~11 Fixations= "23/"
# Hezbollah~12 Fixations= "23/"
# au~13 Fixations= "19/"
# Liban~14 Fixations= "20/"
# Ces~15 Fixations= ""
# tirs~16 Fixations= "20/"
# sont~17 Fixations= ""
# une~18 Fixations= ""
# riposte~19 Fixations= "6/"
# à~20 Fixations= "6/"
# une~21 Fixations= "7/"
# série~22 Fixations= "7/"
# d'attaques~23 Fixations= "8/"
# meurtrières~24 Fixations= "17/"
# de~25 Fixations= ""
# la~26 Fixations= "10/"
# milice~27 Fixations= "11/"
# intégriste~28 Fixations= "13/"
# Chiite~29 Fixations= "14/"
# contre~30 Fixations= "16/"
# Israël~31 Fixations= "16/"
%%
#  L'ARTILLERIE[2] ISRAéLIENNE[4] A[5] MASSIVEMENT[5] bombardé des régions supposées être des BASTIONS[22] DU[23] HEZBOLLAH[23] AU[19] LIBAN[20] Ces TIRS[20] sont une RIPOSTE[6] à[6] UNE[7] SéRIE[7] D'ATTAQUES[8] MEURTRIèRES[17] de LA[10] MILICE[11] INTéGRISTE[13] CHIITE[14] CONTRE[16] ISRAëL[16]
# s10 conflit_israelo_palestinien-f1 PercentageOfWordsProcessed= 0.6875 PercentageOfWordsReallyProcessed= 0.6875
