# 182 Fixation 1 (285.8,285.5): Le~0 chef~1
# 167 Fixation 2 (375.3,278.2): chef~1 gouvernement~3
# 200 Fixation 3 (546.3,272.3): gouvernement~3 accusé~5
# 156 Fixation 4 (599.4,267.1): est~4 accusé~5
# 197 Fixation 5 (289.7,324.5): relations~7
# 166 Fixation 6 (272.8,347): relations~7
# 375 Fixation 7 (372.9,345.3): relations~7 tendues~8
# 269 Fixation 8 (619.3,360.4): les~10 États-Unis~11
# 331 Fixation 9 (680.1,354.2): États-Unis~11
# 278 Fixation 10 (601.7,378.2): les~10 États-Unis~11
# 264 Fixation 11 (565.5,452.4): vertigineuse~22
# 412 Fixation 12 (541.8,502.9): vertigineuse~22
%%
# Le~0 Fixations= "1/"
# chef~1 Fixations= "2/"
# du~2 Fixations= ""
# gouvernement~3 Fixations= "3/"
# est~4 Fixations= "4/"
# accusé~5 Fixations= "4/"
# des~6 Fixations= ""
# relations~7 Fixations= "7/"
# tendues~8 Fixations= "7/"
# avec~9 Fixations= ""
# les~10 Fixations= "10/"
# États-Unis~11 Fixations= "10/"
# de~12 Fixations= ""
# l'isolement~13 Fixations= ""
# d'Israël~14 Fixations= ""
# dans~15 Fixations= ""
# le~16 Fixations= ""
# monde~17 Fixations= ""
# du~18 Fixations= ""
# chômage~19 Fixations= ""
# en~20 Fixations= ""
# montée~21 Fixations= ""
# vertigineuse~22 Fixations= "12/"
# et~23 Fixations= ""
# de~24 Fixations= ""
# la~25 Fixations= ""
# détérioration~26 Fixations= ""
# de~27 Fixations= ""
# toutes~28 Fixations= ""
# les~29 Fixations= ""
# normes~30 Fixations= ""
# de~31 Fixations= ""
# gouvernement~32 Fixations= ""
# démocratique~33 Fixations= ""
%%
#  LE[1] CHEF[2] du GOUVERNEMENT[3] EST[4] ACCUSé[4] des RELATIONS[7] TENDUES[7] avec LES[10] ÉTATS-UNIS[10] de l'isolement d'Israël dans le monde du chômage en montée VERTIGINEUSE[12] et de la détérioration de toutes les normes de gouvernement démocratique
# s14 conflit_israelo_palestinien-m1 PercentageOfWordsProcessed= 0.294117647058824 PercentageOfWordsReallyProcessed= 0.434782608695652
