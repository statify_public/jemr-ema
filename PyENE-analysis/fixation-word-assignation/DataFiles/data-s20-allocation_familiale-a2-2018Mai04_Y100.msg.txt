# 391 Fixation 1 (273.6,289.9): La~0 pianiste~1
# 266 Fixation 2 (416.3,283.7): pianiste~1 américaine~2
# 229 Fixation 3 (557.7,217.7): Rosalyn~3
# 137 Fixation 4 (714.5,163.3):
# 114 Fixation 5 (774.3,157.1):
# 201 Fixation 6 (278.5,342.7): La~0 pianiste~1 entrée~7
# 183 Fixation 7 (322.2,346.5): La~0 pianiste~1 entrée~7
# 353 Fixation 8 (451.6,346.4): américaine~2 à~8
# 178 Fixation 9 (571.9,340.3): Rosalyn~3 quatre-vingt-quatre~9
# 142 Fixation 10 (676.4,285.4): Tureck~4
%%
# La~0 Fixations= "7/"
# pianiste~1 Fixations= "7/"
# américaine~2 Fixations= "8/"
# Rosalyn~3 Fixations= "9/"
# Tureck~4 Fixations= "10/"
# fait~5 Fixations= ""
# son~6 Fixations= ""
# entrée~7 Fixations= "7/"
# à~8 Fixations= "8/"
# quatre-vingt-quatre~9 Fixations= "9/"
# ans~10 Fixations= ""
# dans~11 Fixations= ""
# le~12 Fixations= ""
# répertoire~13 Fixations= ""
# discographique~14 Fixations= ""
# de~15 Fixations= ""
# Deutsche~16 Fixations= ""
# Grammophon~17 Fixations= ""
# Son~18 Fixations= ""
# interprétation~19 Fixations= ""
# des~20 Fixations= ""
# variations~21 Fixations= ""
# Goldberg~22 Fixations= ""
# de~23 Fixations= ""
# Bach~24 Fixations= ""
# va~25 Fixations= ""
# la~26 Fixations= ""
# faire~27 Fixations= ""
# connaître~28 Fixations= ""
# au~29 Fixations= ""
# grand~30 Fixations= ""
# public~31 Fixations= ""
%%
#  LA[7] PIANISTE[7] AMéRICAINE[8] ROSALYN[9] TURECK[10] fait son ENTRéE[7] à[8] QUATRE-VINGT-QUATRE[9] ans dans le répertoire discographique de Deutsche Grammophon Son interprétation des variations Goldberg de Bach va la faire connaître au grand public
# s20 allocation_familiale-a2 PercentageOfWordsProcessed= 0.25 PercentageOfWordsReallyProcessed= 0.8
