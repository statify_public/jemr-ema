# 164 Fixation 1 (351.2,294.6): détresse~1
# 166 Fixation 2 (304.2,289.7): La~0 détresse~1
# 147 Fixation 3 (466.4,281.8): et~2 souffrance~4
# 140 Fixation 4 (523.4,278.1): souffrance~4
# 300 Fixation 5 (674.7,282.7): des~5 réfugiés~6
# 149 Fixation 6 (364.2,340.6): largement~7 médiatisées~8
# 156 Fixation 7 (296.1,344.4): largement~7
# 477 Fixation 8 (417.4,342.6): médiatisées~8
# 353 Fixation 9 (562.4,347): suscitent~9
# 157 Fixation 10 (587.2,344.9): suscitent~9
# 164 Fixation 11 (714.6,341): d'immenses~10
# 213 Fixation 12 (401.1,411.2): de~12 générosité~13
# 264 Fixation 13 (299.6,402.6): élans~11
# 274 Fixation 14 (390.6,279.7): détresse~1
# 109 Fixation 15 (313.8,287.8): La~0 détresse~1
# 198 Fixation 16 (483.5,278.8): et~2 souffrance~4
# 152 Fixation 17 (669.9,276.6): des~5 réfugiés~6
# 162 Fixation 18 (332.9,325.6): largement~7
# 196 Fixation 19 (412.3,336.7): médiatisées~8
# 211 Fixation 20 (544.2,409.3): Des~14 convois~15
# 286 Fixation 21 (579,404.8): Des~14 convois~15
# 105 Fixation 22 (693.8,399.9): convois~15 d'aide~16
# 147 Fixation 23 (334.2,458.1): humanitaire~17
# 159 Fixation 24 (293.7,462.3): humanitaire~17
# 213 Fixation 25 (464.8,466.3): constitués~18
# 177 Fixation 26 (630.6,470.6): par~19 bénévoles~21
# 152 Fixation 27 (692.5,466.6): bénévoles~21
# 154 Fixation 28 (345.1,520.3): différentes~23
# 195 Fixation 29 (296.6,514.9): différentes~23
# 273 Fixation 30 (451,521.6): organisations~24
# 294 Fixation 31 (611.5,534.7): convergent~25
# 151 Fixation 32 (483.2,533.6): organisations~24
# 284 Fixation 33 (634.3,542.3): convergent~25
# 158 Fixation 34 (391.5,588.7): camps~28 réfugiés~30
# 185 Fixation 35 (304.7,586.4): les~27 camps~28
# 139 Fixation 36 (435.6,593): de~29 réfugiés~30
# 183 Fixation 37 (701.9,523.4): convergent~25
# 327 Fixation 38 (423.5,617.1): de~29 réfugiés~30
%%
# La~0 Fixations= "15/"
# détresse~1 Fixations= "15/"
# et~2 Fixations= "16/"
# la~3 Fixations= ""
# souffrance~4 Fixations= "16/"
# des~5 Fixations= "17/"
# réfugiés~6 Fixations= "17/"
# largement~7 Fixations= "18/"
# médiatisées~8 Fixations= "19/"
# suscitent~9 Fixations= "10/"
# d'immenses~10 Fixations= "11/"
# élans~11 Fixations= "13/"
# de~12 Fixations= "12/"
# générosité~13 Fixations= "12/"
# Des~14 Fixations= "21/"
# convois~15 Fixations= "22/"
# d'aide~16 Fixations= "22/"
# humanitaire~17 Fixations= "24/"
# constitués~18 Fixations= "25/"
# par~19 Fixations= "26/"
# des~20 Fixations= ""
# bénévoles~21 Fixations= "27/"
# de~22 Fixations= ""
# différentes~23 Fixations= "29/"
# organisations~24 Fixations= "32/"
# convergent~25 Fixations= "37/"
# vers~26 Fixations= ""
# les~27 Fixations= "35/"
# camps~28 Fixations= "35/"
# de~29 Fixations= "38/"
# réfugiés~30 Fixations= "38/"
%%
#  LA[15] DéTRESSE[15] ET[16] la SOUFFRANCE[16] DES[17] RéFUGIéS[17] LARGEMENT[18] MéDIATISéES[19] SUSCITENT[10] D'IMMENSES[11] éLANS[13] DE[12] GéNéROSITé[12] DES[21] CONVOIS[22] D'AIDE[22] HUMANITAIRE[24] CONSTITUéS[25] PAR[26] des BéNéVOLES[27] de DIFFéRENTES[29] ORGANISATIONS[32] CONVERGENT[37] vers LES[35] CAMPS[35] DE[38] RéFUGIéS[38]
# s04 associations_humanitaires-f1 PercentageOfWordsProcessed= 0.870967741935484 PercentageOfWordsReallyProcessed= 0.870967741935484
