# 143 Fixation 1 (343.5,296.3): président~1
# 268 Fixation 2 (466.1,285.7): de~2 l'APM~3
# 117 Fixation 3 (566.8,290): a~4 vilipendé~5
# 241 Fixation 4 (610.7,296.1): vilipendé~5
# 243 Fixation 5 (587.7,296.4): a~4 vilipendé~5
# 218 Fixation 6 (619.3,294.2): vilipendé~5
# 164 Fixation 7 (331.4,345.4): réforme~7
# 141 Fixation 8 (296.1,352.4): réforme~7
# 240 Fixation 9 (437.1,351): la~9 ministre~10
# 253 Fixation 10 (604.8,349.9): de~11 justice~13
# 143 Fixation 11 (556.6,349.4): ministre~10 justice~13
# 297 Fixation 12 (382.5,351.5): de~8 ministre~10
# 194 Fixation 13 (567,352.4): de~11 justice~13
# 214 Fixation 14 (628.2,351.4): la~12 justice~13
# 177 Fixation 15 (727.5,353.4): justice~13
# 189 Fixation 16 (347.3,409.8): a~16
# 129 Fixation 17 (306.4,413): il~15 a~16
# 165 Fixation 18 (471,407): mené~18
# 203 Fixation 19 (532,411.5): la~19 charge~20
# 148 Fixation 20 (636.3,414.1): contre~21
# 160 Fixation 21 (297.8,448.4): élus~23
# 129 Fixation 22 (278.2,462.1): élus~23
# 314 Fixation 23 (648.9,418.3): contre~21
# 149 Fixation 24 (682,407.8): contre~21
# 198 Fixation 25 (576.1,342.7): de~11 justice~13
# 199 Fixation 26 (450.7,458): de~25 dresser~26
# 156 Fixation 27 (368.1,462.7): accusés~24
# 210 Fixation 28 (531.5,475.4): dresser~26
# 185 Fixation 29 (613.5,475.5): le~27 procès~28
# 123 Fixation 30 (460.5,486.2): de~25 dresser~26
# 184 Fixation 31 (490.9,473.3): de~25 dresser~26
# 171 Fixation 32 (323.7,518.6): magistrature~31
# 143 Fixation 33 (686.8,471.4): procès~28
# 208 Fixation 34 (273.2,515.5): magistrature~31
# 226 Fixation 35 (313.2,298.3): Le~0 président~1
# 211 Fixation 36 (454,291.8): de~2 l'APM~3
# 267 Fixation 37 (577.9,290.6): a~4 vilipendé~5
# 214 Fixation 38 (322.5,360.6): réforme~7
# 145 Fixation 39 (288.4,351.3): réforme~7
%%
# Le~0 Fixations= "35/"
# président~1 Fixations= "35/"
# de~2 Fixations= "36/"
# l'APM~3 Fixations= "36/"
# a~4 Fixations= "37/"
# vilipendé~5 Fixations= "37/"
# la~6 Fixations= ""
# réforme~7 Fixations= "39/"
# de~8 Fixations= "12/"
# la~9 Fixations= "9/"
# ministre~10 Fixations= "12/"
# de~11 Fixations= "25/"
# la~12 Fixations= "14/"
# justice~13 Fixations= "25/"
# mais~14 Fixations= ""
# il~15 Fixations= "17/"
# a~16 Fixations= "17/"
# surtout~17 Fixations= ""
# mené~18 Fixations= "18/"
# la~19 Fixations= "19/"
# charge~20 Fixations= "19/"
# contre~21 Fixations= "24/"
# les~22 Fixations= ""
# élus~23 Fixations= "22/"
# accusés~24 Fixations= "27/"
# de~25 Fixations= "31/"
# dresser~26 Fixations= "31/"
# le~27 Fixations= "29/"
# procès~28 Fixations= "33/"
# de~29 Fixations= ""
# la~30 Fixations= ""
# magistrature~31 Fixations= "34/"
%%
#  LE[35] PRéSIDENT[35] DE[36] L'APM[36] A[37] VILIPENDé[37] la RéFORME[39] DE[12] LA[9] MINISTRE[12] DE[25] LA[14] JUSTICE[25] mais IL[17] A[17] surtout MENé[18] LA[19] CHARGE[19] CONTRE[24] les éLUS[22] ACCUSéS[27] DE[31] DRESSER[31] LE[29] PROCèS[33] de la MAGISTRATURE[34]
# s04 reforme_justice-f1 PercentageOfWordsProcessed= 0.8125 PercentageOfWordsReallyProcessed= 0.8125
