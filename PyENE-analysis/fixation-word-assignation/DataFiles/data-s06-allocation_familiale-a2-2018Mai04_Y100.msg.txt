# 136 Fixation 1 (283.2,286.1): La~0 pianiste~1
# 129 Fixation 2 (337.4,279.7): pianiste~1
# 225 Fixation 3 (277.6,281.7): La~0 pianiste~1
# 201 Fixation 4 (426,278.5): américaine~2
# 121 Fixation 5 (481,279.2): américaine~2
# 227 Fixation 6 (565.2,284.5): Rosalyn~3
# 121 Fixation 7 (460.9,281.4): américaine~2
# 149 Fixation 8 (490.7,285.4): américaine~2
# 251 Fixation 9 (562.4,280.5): Rosalyn~3
# 122 Fixation 10 (581,283.1): Rosalyn~3
# 91 Fixation 11 (627.7,286): Rosalyn~3 Tureck~4
# 181 Fixation 12 (676.1,289.6): Tureck~4
# 112 Fixation 13 (751.7,289.2): Tureck~4 fait~5
# 190 Fixation 14 (280.5,332.3): La~0 pianiste~1 entrée~7
# 81 Fixation 15 (335.7,333.6): pianiste~1 entrée~7
# 220 Fixation 16 (465.7,337.7): américaine~2 à~8 quatre-vingt-quatre~9
# 161 Fixation 17 (531.8,337): américaine~2 Rosalyn~3 quatre-vingt-quatre~9
# 179 Fixation 18 (645,341.6): Rosalyn~3 Tureck~4 ans~10
# 146 Fixation 19 (697.3,449.7): de~15 Deutsche~16
%%
# La~0 Fixations= "14/"
# pianiste~1 Fixations= "15/"
# américaine~2 Fixations= "17/"
# Rosalyn~3 Fixations= "18/"
# Tureck~4 Fixations= "18/"
# fait~5 Fixations= "13/"
# son~6 Fixations= ""
# entrée~7 Fixations= "15/"
# à~8 Fixations= "16/"
# quatre-vingt-quatre~9 Fixations= "17/"
# ans~10 Fixations= "18/"
# dans~11 Fixations= ""
# le~12 Fixations= ""
# répertoire~13 Fixations= ""
# discographique~14 Fixations= ""
# de~15 Fixations= "19/"
# Deutsche~16 Fixations= "19/"
# Grammophon~17 Fixations= ""
# Son~18 Fixations= ""
# interprétation~19 Fixations= ""
# des~20 Fixations= ""
# variations~21 Fixations= ""
# Goldberg~22 Fixations= ""
# de~23 Fixations= ""
# Bach~24 Fixations= ""
# va~25 Fixations= ""
# la~26 Fixations= ""
# faire~27 Fixations= ""
# connaître~28 Fixations= ""
# au~29 Fixations= ""
# grand~30 Fixations= ""
# public~31 Fixations= ""
%%
#  LA[14] PIANISTE[15] AMéRICAINE[17] ROSALYN[18] TURECK[18] FAIT[13] son ENTRéE[15] à[16] QUATRE-VINGT-QUATRE[17] ANS[18] dans le répertoire discographique DE[19] DEUTSCHE[19] Grammophon Son interprétation des variations Goldberg de Bach va la faire connaître au grand public
# s06 allocation_familiale-a2 PercentageOfWordsProcessed= 0.375 PercentageOfWordsReallyProcessed= 0.705882352941177
