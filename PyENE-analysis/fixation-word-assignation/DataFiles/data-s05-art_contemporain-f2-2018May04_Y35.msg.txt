# 191 Fixation 1 (283.5,288.6): Le~0 musée~1
# 178 Fixation 2 (396.4,294.2): d'art~2
# 189 Fixation 3 (451.6,297.9): d'art~2 moderne~3
# 97 Fixation 4 (480.2,299.3): moderne~3
# 171 Fixation 5 (587.7,305.1): de~4 ville~6
# 128 Fixation 6 (601,304.3): de~4 ville~6
# 176 Fixation 7 (711.8,302): ville~6 Paris~8
# 213 Fixation 8 (325.7,341.8): présente~9
# 154 Fixation 9 (400.1,351.5): jusqu'au~10
# 170 Fixation 10 (479.7,354.7): jusqu'au~10 18~11
# 119 Fixation 11 (527.3,355.9): 18~11 avril~12
# 280 Fixation 12 (623,359.3): avril~12 soixante-neuf~13
# 109 Fixation 13 (693.1,359.3): soixante-neuf~13
# 163 Fixation 14 (363.2,407.9): peintures~14 Mark~16
# 190 Fixation 15 (318.4,405.6): peintures~14
# 227 Fixation 16 (468.2,411.4): Mark~16 Rothko~17
# 121 Fixation 17 (533.6,412.5): Rothko~17
# 189 Fixation 18 (642.3,415): un~18 grands~20
# 126 Fixation 19 (665.8,418): des~19 grands~20
# 114 Fixation 20 (605.6,415): un~18 grands~20
%%
# Le~0 Fixations= "1/"
# musée~1 Fixations= "1/"
# d'art~2 Fixations= "3/"
# moderne~3 Fixations= "4/"
# de~4 Fixations= "6/"
# la~5 Fixations= ""
# ville~6 Fixations= "7/"
# de~7 Fixations= ""
# Paris~8 Fixations= "7/"
# présente~9 Fixations= "8/"
# jusqu'au~10 Fixations= "10/"
# 18~11 Fixations= "11/"
# avril~12 Fixations= "12/"
# soixante-neuf~13 Fixations= "13/"
# peintures~14 Fixations= "15/"
# de~15 Fixations= ""
# Mark~16 Fixations= "16/"
# Rothko~17 Fixations= "17/"
# un~18 Fixations= "20/"
# des~19 Fixations= "19/"
# grands~20 Fixations= "20/"
# classiques~21 Fixations= ""
# de~22 Fixations= ""
# la~23 Fixations= ""
# peinture~24 Fixations= ""
# abstraite~25 Fixations= ""
# de~26 Fixations= ""
# l'après-guerre~27 Fixations= ""
# aux~28 Fixations= ""
# Etats-unis~29 Fixations= ""
%%
#  LE[1] MUSéE[1] D'ART[3] MODERNE[4] DE[6] la VILLE[7] de PARIS[7] PRéSENTE[8] JUSQU'AU[10] 18[11] AVRIL[12] SOIXANTE-NEUF[13] PEINTURES[15] de MARK[16] ROTHKO[17] UN[20] DES[19] GRANDS[20] classiques de la peinture abstraite de l'après-guerre aux Etats-unis
# s05 art_contemporain-f2 PercentageOfWordsProcessed= 0.6 PercentageOfWordsReallyProcessed= 0.857142857142857
