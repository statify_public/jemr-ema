# 129 Fixation 1 (265.5,277.4): Malgré~0
# 188 Fixation 2 (369,276.8): un~1 accord~2
# 230 Fixation 3 (518.2,234.5): de~3 principe~4
# 238 Fixation 4 (251.6,342.4): nécessité~7
# 255 Fixation 5 (462.4,325.4): d'une~8 réforme~9
# 246 Fixation 6 (567.9,324): réforme~9 comprenant~10
# 244 Fixation 7 (279.4,403.2): individualisée~13
# 184 Fixation 8 (511.6,415.4): aux~14 élèves~15
# 155 Fixation 9 (479.1,427.5): aux~14 élèves~15
%%
# Malgré~0 Fixations= "1/"
# un~1 Fixations= "2/"
# accord~2 Fixations= "2/"
# de~3 Fixations= "3/"
# principe~4 Fixations= "3/"
# sur~5 Fixations= ""
# la~6 Fixations= ""
# nécessité~7 Fixations= "4/"
# d'une~8 Fixations= "5/"
# réforme~9 Fixations= "6/"
# comprenant~10 Fixations= "6/"
# une~11 Fixations= ""
# aide~12 Fixations= ""
# individualisée~13 Fixations= "7/"
# aux~14 Fixations= "9/"
# élèves~15 Fixations= "9/"
# le~16 Fixations= ""
# syndicat~17 Fixations= ""
# des~18 Fixations= ""
# enseignants~19 Fixations= ""
# affirmait~20 Fixations= ""
# qu'il~21 Fixations= ""
# fallait~22 Fixations= ""
# abandonner~23 Fixations= ""
# ce~24 Fixations= ""
# projet~25 Fixations= ""
# et~26 Fixations= ""
# retravailler~27 Fixations= ""
# sur~28 Fixations= ""
# d'autres~29 Fixations= ""
# bases~30 Fixations= ""
%%
#  MALGRé[1] UN[2] ACCORD[2] DE[3] PRINCIPE[3] sur la NéCESSITé[4] D'UNE[5] RéFORME[6] COMPRENANT[6] une aide INDIVIDUALISéE[7] AUX[9] éLèVES[9] le syndicat des enseignants affirmait qu'il fallait abandonner ce projet et retravailler sur d'autres bases
# s20 reforme_enseignement-f2 PercentageOfWordsProcessed= 0.387096774193548 PercentageOfWordsReallyProcessed= 0.75
