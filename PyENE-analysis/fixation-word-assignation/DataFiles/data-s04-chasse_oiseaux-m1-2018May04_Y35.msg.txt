# 193 Fixation 1 (338.5,291.4): L'ortolan~0
# 152 Fixation 2 (310.5,289.4): L'ortolan~0
# 166 Fixation 3 (438.4,282.1): petit~1
# 147 Fixation 4 (479.5,275.2): petit~1 oiseau~2
# 107 Fixation 5 (380.7,288.1): L'ortolan~0 petit~1
# 158 Fixation 6 (288.4,292.4): L'ortolan~0
# 161 Fixation 7 (321.9,287.7): L'ortolan~0
# 181 Fixation 8 (295.6,289.4): L'ortolan~0
# 360 Fixation 9 (498.7,280.8): oiseau~2
# 258 Fixation 10 (585.1,285.5): très~3
# 154 Fixation 11 (630.8,287.2): très~3 prisé~4
# 126 Fixation 12 (730.2,293.1): par~5
# 183 Fixation 13 (339.1,355.1): gourmets~7
# 141 Fixation 14 (302.7,354.5): gourmets~7
# 134 Fixation 15 (427.1,349.6): du~8 Sud-ouest~9
# 156 Fixation 16 (275.4,362.7): gourmets~7
# 310 Fixation 17 (412.3,353): du~8 Sud-ouest~9
# 212 Fixation 18 (288.4,354.6): gourmets~7
# 188 Fixation 19 (429,351.4): du~8 Sud-ouest~9
# 238 Fixation 20 (600.3,343.6): appartiendra~10
# 167 Fixation 21 (728.8,349.6): bientôt~11
# 229 Fixation 22 (392.7,398.9): espèces~13 protégées~14
# 181 Fixation 23 (314.1,405): aux~12 espèces~13
# 244 Fixation 24 (437.6,408.7): protégées~14
# 143 Fixation 25 (582.6,408.6): Un~15 décret~16
# 160 Fixation 26 (644.7,403.6): décret~16 a~17
# 196 Fixation 27 (303.4,466): signé~19
# 201 Fixation 28 (409,471.3): qui~20 représente~21
# 263 Fixation 29 (559.4,466.9): le~22 premier~23
# 142 Fixation 30 (672.5,466.3): premier~23 acte~24
# 210 Fixation 31 (411.2,473.3): qui~20 représente~21
# 164 Fixation 32 (348.4,471.2): signé~19 représente~21
# 321 Fixation 33 (558.7,469.9): le~22 premier~23
# 168 Fixation 34 (649.1,471.2): premier~23 acte~24
# 94 Fixation 35 (736.8,459.6): acte~24
# 199 Fixation 36 (303.6,519.4): compromis~26
# 368 Fixation 37 (395.7,525): global~27
# 185 Fixation 38 (304.8,522.4): compromis~26
# 178 Fixation 39 (421.3,527.6): global~27
# 252 Fixation 40 (541.9,535.1): entre~28 écologistes~29
# 252 Fixation 41 (578.3,533.9): écologistes~29
# 158 Fixation 42 (500.9,530.1): entre~28 écologistes~29
# 324 Fixation 43 (320,570.5): compromis~26
# 153 Fixation 44 (622.7,547.2): écologistes~29
%%
# L'ortolan~0 Fixations= "8/"
# petit~1 Fixations= "5/"
# oiseau~2 Fixations= "9/"
# très~3 Fixations= "11/"
# prisé~4 Fixations= "11/"
# par~5 Fixations= "12/"
# les~6 Fixations= ""
# gourmets~7 Fixations= "18/"
# du~8 Fixations= "19/"
# Sud-ouest~9 Fixations= "19/"
# appartiendra~10 Fixations= "20/"
# bientôt~11 Fixations= "21/"
# aux~12 Fixations= "23/"
# espèces~13 Fixations= "23/"
# protégées~14 Fixations= "24/"
# Un~15 Fixations= "25/"
# décret~16 Fixations= "26/"
# a~17 Fixations= "26/"
# été~18 Fixations= ""
# signé~19 Fixations= "32/"
# qui~20 Fixations= "31/"
# représente~21 Fixations= "32/"
# le~22 Fixations= "33/"
# premier~23 Fixations= "34/"
# acte~24 Fixations= "35/"
# d'un~25 Fixations= ""
# compromis~26 Fixations= "43/"
# global~27 Fixations= "39/"
# entre~28 Fixations= "42/"
# écologistes~29 Fixations= "44/"
# et~30 Fixations= ""
# chasseurs~31 Fixations= ""
%%
#  L'ORTOLAN[8] PETIT[5] OISEAU[9] TRèS[11] PRISé[11] PAR[12] les GOURMETS[18] DU[19] SUD-OUEST[19] APPARTIENDRA[20] BIENTôT[21] AUX[23] ESPèCES[23] PROTéGéES[24] UN[25] DéCRET[26] A[26] été SIGNé[32] QUI[31] REPRéSENTE[32] LE[33] PREMIER[34] ACTE[35] d'un COMPROMIS[43] GLOBAL[39] ENTRE[42] éCOLOGISTES[44] et chasseurs
# s04 chasse_oiseaux-m1 PercentageOfWordsProcessed= 0.84375 PercentageOfWordsReallyProcessed= 0.9
