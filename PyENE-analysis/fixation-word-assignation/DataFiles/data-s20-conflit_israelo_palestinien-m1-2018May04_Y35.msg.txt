# 152 Fixation 1 (288.5,294.6): Le~0 chef~1
# 167 Fixation 2 (376.9,269.7): chef~1 gouvernement~3
# 160 Fixation 3 (585.4,200.8):
# 134 Fixation 4 (634.6,190):
# 276 Fixation 5 (262.1,348.3): relations~7
# 172 Fixation 6 (431.9,336.1): tendues~8
# 234 Fixation 7 (396.8,343.2): tendues~8
# 156 Fixation 8 (581.2,315.3): est~4 accusé~5
# 243 Fixation 9 (286.1,399): l'isolement~13
# 206 Fixation 10 (466.8,409.4): d'Israël~14
# 143 Fixation 11 (441.7,408.4): d'Israël~14
# 444 Fixation 12 (594.2,417.8): dans~15 monde~17
# 166 Fixation 13 (478.2,451.8): montée~21 vertigineuse~22
# 187 Fixation 14 (351.2,448.8): chômage~19 montée~21
# 354 Fixation 15 (270.1,447.8): chômage~19
# 258 Fixation 16 (491,464.7): montée~21 vertigineuse~22
# 254 Fixation 17 (398.8,456.1): en~20 montée~21
# 216 Fixation 18 (537.7,474.5): vertigineuse~22
# 216 Fixation 19 (673.6,485): et~23
# 200 Fixation 20 (314.9,501.2): chômage~19
# 150 Fixation 21 (283.2,512.8): détérioration~26
# 209 Fixation 22 (486.8,524): de~27 toutes~28
# 235 Fixation 23 (614.4,544.2): les~29 normes~30
# 200 Fixation 24 (322.2,577): gouvernement~32
# 155 Fixation 25 (280.5,575): gouvernement~32
# 392 Fixation 26 (454.5,588.8): démocratique~33
# 286 Fixation 27 (299.9,587.3): gouvernement~32
# 362 Fixation 28 (289.4,585.2): gouvernement~32
# 292 Fixation 29 (595.8,551.1): les~29 normes~30
# 152 Fixation 30 (634.5,552.6): les~29 normes~30
# 187 Fixation 31 (480.5,611): démocratique~33
# 334 Fixation 32 (443.4,594.8): démocratique~33
%%
# Le~0 Fixations= "1/"
# chef~1 Fixations= "2/"
# du~2 Fixations= ""
# gouvernement~3 Fixations= "2/"
# est~4 Fixations= "8/"
# accusé~5 Fixations= "8/"
# des~6 Fixations= ""
# relations~7 Fixations= "5/"
# tendues~8 Fixations= "7/"
# avec~9 Fixations= ""
# les~10 Fixations= ""
# États-Unis~11 Fixations= ""
# de~12 Fixations= ""
# l'isolement~13 Fixations= "9/"
# d'Israël~14 Fixations= "11/"
# dans~15 Fixations= "12/"
# le~16 Fixations= ""
# monde~17 Fixations= "12/"
# du~18 Fixations= ""
# chômage~19 Fixations= "20/"
# en~20 Fixations= "17/"
# montée~21 Fixations= "17/"
# vertigineuse~22 Fixations= "18/"
# et~23 Fixations= "19/"
# de~24 Fixations= ""
# la~25 Fixations= ""
# détérioration~26 Fixations= "21/"
# de~27 Fixations= "22/"
# toutes~28 Fixations= "22/"
# les~29 Fixations= "30/"
# normes~30 Fixations= "30/"
# de~31 Fixations= ""
# gouvernement~32 Fixations= "28/"
# démocratique~33 Fixations= "32/"
%%
#  LE[1] CHEF[2] du GOUVERNEMENT[2] EST[8] ACCUSé[8] des RELATIONS[5] TENDUES[7] avec les États-Unis de L'ISOLEMENT[9] D'ISRAëL[11] DANS[12] le MONDE[12] du CHôMAGE[20] EN[17] MONTéE[17] VERTIGINEUSE[18] ET[19] de la DéTéRIORATION[21] DE[22] TOUTES[22] LES[30] NORMES[30] de GOUVERNEMENT[28] DéMOCRATIQUE[32]
# s20 conflit_israelo_palestinien-m1 PercentageOfWordsProcessed= 0.676470588235294 PercentageOfWordsReallyProcessed= 0.676470588235294
