library("nlme")
library("DMwR2")  # sampleCSV 

setwd("/home/bolivier/chap3/data/modwt_coeff_for_lme")

df = read.csv("occipital-scale7.csv", header = FALSE)
#df = sampleCSV("occipital-scale7.csv", percORn=0.01, nrLines=8602590, header=FALSE)
colnames(df) = c("y", "subject", "phase")
df$phase = as.factor(df$phase)
nrow_df = nrow(df)
sample_size = 5000
df = df[sample(nrow_df, sample_size), ]
str(df)

fitM0 = lme(y ~ 1, random=~1|subject, method="ML", data=df, na.action = na.omit) # constant mu
summary(fitM0)

fitM1 = lme(y ~ phase, random=~1|subject, method="ML", data=df, na.action = na.omit) # mu per phase
summary(fitM1)

fitM2 = lme(y ~ phase, random=list(subject=~1, phase=~1, ), method="ML", data=df, na.action = na.omit)
summary(fitM2)


anova(fitM0, fitM1)

fitM2 = lme(y ~ . , random=~1|subject, method="ML", data=df, na.action = na.omit)
summary(fitM2)