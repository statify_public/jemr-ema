library(waveslim)
library(fields)

remove_baseline_activity = function(eeg_trial, gain_or_additive='gain') {
  index_zero = get_time_index(eeg_trial, 0)
  baseline_activity = eeg_trial$eeg[1:index_zero, ]
  if(gain_or_additive == 'gain')
    eeg_trial$eeg = (eeg_trial$eeg - colMeans(baseline_activity)) / apply(baseline_activity, 2, sd)
  else
    eeg_trial$eeg = eeg_trial$eeg / colMeans(baseline_activity)
  return(eeg_trial)
}

apply_avg_referencing = function(eeg_trial) eeg_trial$eeg - colMeans(eeg_trial$eeg)

get_time_index = function(eeg_trial, time) which(eeg_trial$times == time)
get_fixations_event_id = function(eeg_trial) which(eeg_trial$numlinexls != -1)
get_first_fixation_time = function(eeg_trial) eeg_trial$eventlatency[get_fixations_event_id(eeg_trial)[1]]
get_first_fixation_time_id = function(eeg_trial) which(eeg_trial$times == get_first_fixation_time(eeg_trial))
get_last_fixation_time = function(eeg_trial) {
  last_fixation_id = get_fixations_event_id(eeg_trial)
  last_fixation_id = last_fixation_id[length(last_fixation_id)]
  return(eeg_trial$eventlatency[last_fixation_id])
}
get_last_fixation_time_id = function(eeg_trial) which(eeg_trial$times == get_last_fixation_time(eeg_trial))

compute_modwt = function(eeg_trial, tmin, tmax, margin, n_levels, wf) {
  len_ts = dim(eeg_trial$eeg)[1]
  left_margin = right_margin = margin
  if (tmin - margin < 0) left_margin = tmin
  if (tmax + margin > len_ts) right_margin = len_ts - tmax
  censor = ((tmax + right_margin) - (tmin - left_margin)) %% (2 ** n_levels)
  left_censor = ceiling(censor / 2)
  right_censor = floor(censor / 2)
  wt = apply(eeg_trial$eeg[(tmin - left_margin + left_censor):(tmax + right_margin - right_censor),],
             2, modwt, wf=wf, n.levels=n_levels, boundary='periodic')
  wt = lapply(wt, phase.shift, 'la8')
  len_wt = length(wt[[1]][[1]])
  for(i in 1:length(wt)) {
    for(j in 1:(n_levels+1)) {
      wt[[i]][[j]] = wt[[i]][[j]][(left_margin - left_censor):(len_wt + (right_censor - right_margin))]
    }
  }
  return(wt)
}


compute_epoch_phases = function(em_trial, tmax) {
  phases = list()
  em_start = start = em_trial$FIX_LATENCY[1]
  phase = em_trial$PHASE[1] + 1
  old_phase = phase
  nb_phase = 1
  for(i in 2:(nrow(em_trial)-1)) {
    phase = em_trial$PHASE[i] + 1
    if (phase != old_phase) {
      end = em_trial$FIX_LATENCY[i] - 1
      phases[[nb_phase]] = c(start, end, old_phase)
      start = end + 1
      old_phase = phase
      nb_phase = nb_phase + 1
    }
  }
  end = em_trial$FIX_LATENCY[nrow(em_trial)]
  phases[[nb_phase]] = c(start, end, old_phase)
  phases_tmp = list()
  for(i in 1:length(phases)) {
    if(phases[[i]][2] < tmax){
      phases_tmp[[i]] = c(phases[[i]][1] - em_start + 1, phases[[i]][2] - em_start + 1, phases[[i]][3])
    } else {
      phases_tmp[[i]] = c(phases[[i]][1] - em_start + 1, tmax - em_start + 1, phases[[i]][3])
      break
    }
  }
  phases = phases_tmp
  return(phases)  
}

cut_modwt_into_phases = function(wt, phases, n_levels) {
  wt_per_phase = list()
  for(i in 1:4){
    wt_per_phase[[i]] = list()
  }
  for(phase in phases) {
    wt_per_phase[[phase[3]]] = list()
    for(i in 1:length(wt)) {
      wt_per_phase[[phase[3]]][[i]] = list()
      for(j in 1:(n_levels+1)) {
        wt_per_phase[[phase[3]]][[i]][[j]] = wt[[i]][[j]][phase[1]:phase[2]]
      }
    }
  }
  return(wt_per_phase)
}

compute_corr = function(cwt, threshold) {
  n_chan = length(cwt[[1]])
  matrices = list()
  adjancency_mat = corr_mat = list()
  for(i in 1:4){
    adjancency_mat[[i]] = corr_mat[[i]] = list()
  }
  for (k in 1:length(cwt)) {
    if(length(cwt[[k]]) != 0) {
      modwt_len = length(cwt[[k]][[1]][[1]])
      adjancency_mat[[k]] = corr_mat[[k]] = array(rep(0, n_chan*n_chan*n_levels), dim=c(n_chan, n_chan, n_levels+1))
      for (i in 1:n_chan) {
        for(j in i:n_chan) {
          if(i != j) {
            wc = wave.correlation(cwt[[k]][[i]], cwt[[k]][[j]], modwt_len, p=0.975)
            which_sup_threshold = which(wc[,2] > threshold)
            wc_thresholded = wc[wc[,2] > threshold, 1]
            corr_mat[[k]][i, j, ] = corr_mat[[k]][j, i, ] = wc[, 1]
            adjancency_mat[[k]][i, j, ] = adjancency_mat[[k]][j, i, ] = wc[,2] > threshold
          }
        }
      }
    } 
  }
  matrices[[1]] = corr_mat
  matrices[[2]] = adjancency_mat
  return(matrices)
}

compute_corr2 = function(cwt, threshold) {
  alpha = 0.05 #/ sum(1:29) # bonferroni correction
  #alpha = 0.05
  n_chan = length(cwt[[1]])
  matrices = list()
  lower_bound_cint = corr_mat = list()
  modwt_len = 0
  for(i in 1:4){
    lower_bound_cint[[i]] = corr_mat[[i]] = list()
    if (length(cwt[[i]]) != 0) n_chan = length(cwt[[i]])
    if(length(cwt[[i]]) != 0) modwt_len = modwt_len + length(cwt[[i]][[1]][[1]])
  }
  for (k in 1:length(cwt)) {
    if(length(cwt[[k]]) != 0) {
      lower_bound_cint[[k]] = corr_mat[[k]] = array(rep(0, n_chan*n_chan*n_levels), dim=c(n_chan, n_chan, n_levels))
      for (i in 1:n_chan) {
        for(j in i:n_chan) {
          if(i != j) {
            wc = wave.correlation(cwt[[k]][[i]], cwt[[k]][[j]], modwt_len, p=1-alpha/2)
            corr_mat[[k]][i, j, ] = corr_mat[[k]][j, i, ] = wc[1:(nrow(wc)-1), 1]
            lower_bound_cint[[k]][i, j, ] = lower_bound_cint[[k]][j, i, ] = wc[1:(nrow(wc)-1), 2]
          }
        }
      }
    } 
  }
  matrices[[1]] = corr_mat
  matrices[[2]] = lower_bound_cint
  return(matrices)
}


setwd("/home/bolivier/chap3/data")
phases_color = c('#F8766D', '#00BFC4', '#7CAE00', '#C77CFF') #red, blue, green, purple
em_df = read.csv("rdf-mki.csv")
channels = read.csv("channels.csv", row.names = 1)
n_chan = dim(channels)[1]
n_levels = 7
wf = "la8"


data_files = list.files(path="/home/bolivier/chap3/data/eeg_data")


# covar analysis
data_files = sample(data_files, size=10)
search_threshold = c()
search_threshold_per_phase = c()
threshold_per_scale_phase = c()
thresholds=seq(0.4,0.7,0.05)
thresholds=c(0.52)
for(threshold in thresholds){
  n_trials = length(data_files)
  avg_corr_mat = lower_bound_cint = seq_len = list()
  for(i in 1:4) {
    avg_corr_mat[[i]] = lower_bound_cint[[i]] = array(rep(NaN, n_chan*n_chan*n_levels*n_trials), dim=c(n_chan, n_chan, n_levels, n_trials))
    seq_len[[i]] = rep(NaN, n_trials)
  }
  current_corr_mat_phase_index = c(1,1,1,1)
  phase_counter = c(0,0,0,0)
  for(i in 1:length(data_files)) { #data_file in data_files
    load(paste0("eeg_data/",data_files[i]))
    eeg_trial$eeg = t(as.matrix(eeg_trial$eeg))
    tmin = get_first_fixation_time_id(eeg_trial)
    tmax = get_last_fixation_time_id(eeg_trial)
    em_trial[nrow(eeg_trial),]$FDUR
    margin = - eeg_trial$times[1]
    wt = compute_modwt(eeg_trial, tmin, tmax, margin, n_levels, wf)
    em_trial = em_df[which((em_df$SUBJ_NAME == eeg_trial$subject_name) & (em_df$TEXT == eeg_trial$text_name)) , ]
    phases = compute_epoch_phases(em_trial, tmax)
    for(phase in phases) {
      phase_counter[phase[3]] = phase_counter[phase[3]] + 1
    }
    cwt = cut_modwt_into_phases(wt, phases, n_levels)
    corr = compute_corr2(cwt, threshold=threshold)
    for(j in 1:4) {
      if(length(corr[[1]][[j]]) != 0) {
        avg_corr_mat[[j]][,,,current_corr_mat_phase_index[j]] = corr[[1]][[j]]
        lower_bound_cint[[j]][,,,current_corr_mat_phase_index[j]] = corr[[2]][[j]]
        seq_len[[j]][current_corr_mat_phase_index[j]] = length(cwt[[j]][[1]][[1]])
        current_corr_mat_phase_index[j] = current_corr_mat_phase_index[j] + 1
      }
    }
    rm(eeg_trial)
  }
  
  avg_corr = list()
  for(i in 1:4) {
    if(length(avg_corr_mat[[i]]) != 0) {
      avg_corr[[i]] = apply(avg_corr_mat[[i]][,,,1:(current_corr_mat_phase_index[i]-1)],
                            c(1,2,3), weighted.mean, w=seq_len[[1]][1:(current_corr_mat_phase_index[i]-1)], na.rm=TRUE)
    }
  }
  #avg_corr[[1]]
  
  avg_lower_bound_cint = list()
  for(i in 1:4) {
    if(length(lower_bound_cint[[i]]) != 0) {
      avg_lower_bound_cint[[i]] = apply(lower_bound_cint[[i]][,,,1:(current_corr_mat_phase_index[i]-1)],
                            c(1,2,3), weighted.mean, w=seq_len[[1]][1:(current_corr_mat_phase_index[i]-1)], na.rm=TRUE)
    }
  }
  #avg_lower_bound_cint[[1]]
  
  adj_mat = list()
  for(i in 1:4) {
    adj_mat[[i]] = avg_lower_bound_cint[[i]] > threshold
  }
  
  threshold_per_scale_phase = rbind(apply(adj_mat[[1]], 3, mean, na.rm=TRUE)*30,
                                    apply(adj_mat[[2]], 3, mean, na.rm=TRUE)*30,
                                    apply(adj_mat[[3]], 3, mean, na.rm=TRUE)*30,
                                    apply(adj_mat[[4]], 3, mean, na.rm=TRUE)*30)
  threshold_per_scale = apply(threshold_per_scale_phase, 2, mean, na.rm=TRUE)
  search_threshold = rbind(search_threshold, cbind(threshold_per_scale, 1:7, rep(threshold, 7)))

  search_threshold_per_phase = rbind(search_threshold_per_phase, cbind(c(t(threshold_per_scale_phase)), rep(1:7, 4), rep(threshold, 7*4),
        c(rep(1, 7), rep(2,7), rep(3,7), rep(4,7))))

}

st = as.data.frame(search_threshold)
colnames(st) = c("mean_degree", "scale", "correlation")
st$scale = as.factor(st$scale)
ggplot(st, aes(x = correlation, y = mean_degree)) +
  geom_point(aes(shape=scale, color = scale)) +
  geom_line(aes(color = scale)) +
  geom_hline(yintercept=log(30), linetype="dashed",  size=1) +
  theme(text = element_text(size=30)) +
  labs(x = "Correlation threshold", y="Mean degree")

#load('st.Rdata')


search_threshold_per_phase = st2
st2= search_threshold_per_phase
search_threshold_per_phase = as.data.frame(search_threshold_per_phase)
search_threshold_per_phase$phase = factor(search_threshold_per_phase$phase, levels = c('NR', 'IS', 'SR', 'SC'),
                                          labels = c("Normal Reading", 'Information Search',
                                                     'Speed Reading', 'Slow Confirmation'))
search_threshold_per_phase$scale = as.factor(search_threshold_per_phase$scale)
colnames(search_threshold_per_phase) = c("mean_degree", "Wavelet scale", "threshold", "phase")
ggplot(search_threshold_per_phase, aes(x = threshold, y = mean_degree)) +
  geom_point(aes(shape=scale, color = scale)) +
  geom_line(aes(color = factor(scale))) +
  geom_hline(yintercept=log(30), linetype="dashed",  size=1) +
  facet_wrap(phase ~ ., ncol=2) +
  theme(text = element_text(size=30)) +
  labs(x = "Correlation threshold", y="Mean degree")

load('search_threshold_per_phase.Rdata')
#save(search_threshold_per_phase, file='search_threshold_per_phase.Rdata')



# load(paste0("eeg_data/",data_files[1]))
# eeg_trial$eeg = t(as.matrix(eeg_trial$eeg))
# #eeg_trial = remove_baseline_activity(eeg_trial)
# tmin = get_first_fixation_time_id(eeg_trial)
# tmax = get_last_fixation_time_id(eeg_trial)
# margin = - eeg_trial$times[1]
# wt = compute_modwt(eeg_trial, tmin, tmax, margin, n_levels, wf)
# em_trial = em_df[which((em_df$SUBJ_NAME == eeg_trial$subject_name) & (em_df$TEXT == eeg_trial$text_name)) , ]
# phases = compute_epoch_phases(em_trial, tmax)
# cwt = cut_modwt_into_phases(wt, phases, n_levels)
# corr = compute_corr(cwt, threshold=0.5)


par(mfrow=c(1,3))
image.plot(avg_corr[[1]][,,7] , axes=F, main="Correlation matrix", cex.main=1.5) # theta
mtext(text=rownames(channels), side=2, line=0.3, at=seq(0,1,0.0343), las=1, cex=1)
mtext(text=rownames(channels), side=1, line=0.3, at=seq(0,1,0.0343), las=2, cex=1)

image.plot(adj_mat[[1]][,,7], axes=F, main="Adjacency matrix", cex.main=1.5)

#image.plot(corr[[1]][[1]][,,1] ,main='wavelet scale 1')
#image.plot(corr[[1]][[1]][,,2] ,main='wavelet scale 2')
#image.plot(corr[[1]][[1]][,,3] ,main='wavelet scale gamma +') # gamma +
image.plot(avg_corr[[1]][,,1] ,main='wavelet scale 1') # gamma -
image.plot(avg_corr[[1]][,,2] ,main='wavelet scale 2') # gamma -
image.plot(avg_corr[[1]][,,3] ,main='wavelet scale 3') # gamma -
image.plot(avg_corr[[1]][,,4] ,main='wavelet scale gamma -') # gamma -
image.plot(avg_corr[[1]][,,5] ,main='wavelet scale beta') # beta
image.plot(avg_corr[[1]][,,6] ,main='wavelet scale alpha') # alpha
image.plot(avg_corr[[1]][,,7] ,main='wavelet scale theta') # theta

#image.plot(corr[[1]][[1]][,,8] ,main='wavelet scale 8')

image.plot(avg_corr[[2]][,,4] ,main='wavelet scale gamma -') # gamma -
image.plot(avg_corr[[2]][,,5] ,main='wavelet scale beta') # beta
image.plot(avg_corr[[2]][,,6] ,main='wavelet scale alpha') # alpha
image.plot(avg_corr[[2]][,,7] ,main='wavelet scale theta') # theta
#image.plot(corr[[1]][[2]][,,8] ,main='wavelet scale 8')

image.plot(avg_corr[[3]][,,4] ,main='wavelet scale gamma -') # gamma -
image.plot(avg_corr[[3]][,,5] ,main='wavelet scale beta') # beta
image.plot(avg_corr[[3]][,,6] ,main='wavelet scale alpha') # alpha
image.plot(avg_corr[[3]][,,7] ,main='wavelet scale theta') # theta

image.plot(avg_corr[[4]][,,4] ,main='wavelet scale gamma -') # gamma -
image.plot(avg_corr[[4]][,,5] ,main='wavelet scale beta') # beta
image.plot(avg_corr[[4]][,,6] ,main='wavelet scale alpha') # alpha
image.plot(avg_corr[[4]][,,7] ,main='wavelet scale theta') # theta


plot_anatomical_maps = function(adj_list, subject, phase, scale) {
adj_mat = adj_list[[subject]]
#phase = 2
#scale = 7
coord_file = channels
n.regions = dim(coord_file)[1]
set2 = coord_file
euclid = 2 * dist(set2, method = "euclidean")
x.euclid = as.matrix(euclid)
#index = c(1:(n.regions/2))*2 # regions gauches et droites
index = 1:n.regions
set2 = as.matrix(set2)
set2[index,c(2,3)] = set2[index,c(2,3)] + 1.5
adj.mat = adj_mat[[phase]][,,scale]
par(mfrow=c(1,2))
x.coord<-2
y.coord<-3
plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
     asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
     xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
     yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1)
for(kk in 2:(n.regions)){
  for(q in 1:(kk-1)){
    if(adj.mat[kk,q]==1) {
      lines(c(set2[kk,x.coord], set2[q,x.coord]),
            c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase],lw=2)
    }
  }
}
pos_vector <- rep(3, n.regions)
pos_vector[row.names(coord_file) %in% c("O1", "P8", "T8", "F8", "Fp1", "C3", "F3", "TP10", "PO9", "P3")] = 1
pos_vector[row.names(coord_file) == "O2"] = 4
text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)

x.coord<-1
y.coord<-2
plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
     asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
     xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
     yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1.5, main="Anatomical map")
for(kk in 2:(n.regions)){
  for(q in 1:(kk-1)){
    if(adj.mat[kk,q]==1) {
      lines(c(set2[kk,x.coord], set2[q,x.coord]),
            c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase] ,lw=2)
    }
  }
}
pos_vector <- rep(3, n.regions)
pos_vector[row.names(coord_file) %in% c("CP5", "CP6", "PO9", "PO10")] = 1
text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)
}












plot_anatomical_maps2 = function(adj.mat) {
  #phase = 2
  #scale = 7
  coord_file = channels
  n.regions = dim(coord_file)[1]
  set2 = coord_file
  euclid = 2 * dist(set2, method = "euclidean")
  x.euclid = as.matrix(euclid)
  #index = c(1:(n.regions/2))*2 # regions gauches et droites
  index = 1:n.regions
  set2 = as.matrix(set2)
  set2[index,c(2,3)] = set2[index,c(2,3)] + 1.5
  par(mfrow=c(1,2))
  x.coord<-2
  y.coord<-3
  plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
       asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
       xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
       yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1)
  for(kk in 2:(n.regions)){
    for(q in 1:(kk-1)){
      if(adj.mat[kk,q]==1) {
        lines(c(set2[kk,x.coord], set2[q,x.coord]),
              c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase],lw=2)
      }
    }
  }
  pos_vector <- rep(3, n.regions)
  pos_vector[row.names(coord_file) %in% c("O1", "P8", "T8", "F8", "Fp1", "C3", "F3", "TP10", "PO9", "P3")] = 1
  pos_vector[row.names(coord_file) == "O2"] = 4
  text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)
  
  x.coord<-1
  y.coord<-2
  plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
       asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
       xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
       yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1.5, main="Anatomical map")
  for(kk in 2:(n.regions)){
    for(q in 1:(kk-1)){
      if(adj.mat[kk,q]==1) {
        lines(c(set2[kk,x.coord], set2[q,x.coord]),
              c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase] ,lw=2)
      }
    }
  }
  pos_vector <- rep(3, n.regions)
  pos_vector[row.names(coord_file) %in% c("CP5", "CP6", "PO9", "PO10")] = 1
  text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)
}









plot_anatomical_maps3 = function(adj.mat) {
  #phase = 2
  #scale = 7
  coord_file = channels
  n.regions = dim(coord_file)[1]
  set2 = coord_file
  euclid = 2 * dist(set2, method = "euclidean")
  x.euclid = as.matrix(euclid)
  #index = c(1:(n.regions/2))*2 # regions gauches et droites
  index = 1:n.regions
  set2 = as.matrix(set2)
  set2[index,c(2,3)] = set2[index,c(2,3)] + 1.5
  #par(mfrow=c(1,2))
  x.coord<-2
  y.coord<-3
  # plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
  #      asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
  #      xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
  #      yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1)
  # for(kk in 2:(n.regions)){
  #   for(q in 1:(kk-1)){
  #     if(adj.mat[kk,q]==1) {
  #       lines(c(set2[kk,x.coord], set2[q,x.coord]),
  #             c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase],lw=2)
  #     }
  #   }
  # }
  pos_vector <- rep(3, n.regions)
  pos_vector[row.names(coord_file) %in% c("O1", "P8", "T8", "F8", "Fp1", "C3", "F3", "TP10", "PO9", "P3")] = 1
  pos_vector[row.names(coord_file) == "O2"] = 4
  #text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)
  
  x.coord<-1
  y.coord<-2
  plot(set2[,x.coord], set2[,y.coord], type = "p",xlab= "", ylab="",cex.lab=2,
       asp=1,ylim=c(min(set2[,y.coord]),max(set2[,y.coord])),
       xlim=c(min(set2[,x.coord]),max(set2[,x.coord])),xaxt='n',
       yaxt='n',bty='n',cex=0.5,pch=16,cex.main=1.5)
  for(kk in 2:(n.regions)){
    for(q in 1:(kk-1)){
      if(adj.mat[kk,q]==1) {
        lines(c(set2[kk,x.coord], set2[q,x.coord]),
              c(set2[kk,y.coord], set2[q,y.coord]), col = phases_color[phase] ,lw=2)
      }
    }
  }
  pos_vector <- rep(3, n.regions)
  pos_vector[row.names(coord_file) %in% c("CP5", "CP6", "PO9", "PO10")] = 1
  text(set2[,x.coord], set2[,y.coord], labels=row.names(coord_file), cex=1, pos=pos_vector)
}


