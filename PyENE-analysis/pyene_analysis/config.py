# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import os
cdir = os.path.dirname(__file__)
from pathlib import Path

# OUTPUT_PATH = '/home/bolivier/PyENE/PyENE/em-analysis/output/'
PROJECT_PATH = cdir 
PROJECT_PATH = str(Path(PROJECT_PATH).parent)

R_PATH = os.path.join(PROJECT_PATH, 'R-scripts')
DATA_PATH = os.path.join(PROJECT_PATH, 'data')
PNG_PATH = os.path.join(PROJECT_PATH, 'material')
MODEL_PATH = os.path.join(PROJECT_PATH, 'models')
OUTPUT_PATH = os.path.join(PROJECT_PATH, 'outputs')
POST_PROC_PATH = os.path.join(OUTPUT_PATH, 'a-posteriori-analysis-tables')
MODEL_SCORES_PATH = os.path.join(OUTPUT_PATH, 'model-scores')
TRIGGER_WORDS_PATH = os.path.join(OUTPUT_PATH, 'trigger-words')
