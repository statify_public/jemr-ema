# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import pandas as pd
from ema import EyeMovementDataFrame
from ema import Model
import numpy as np
from openalea.sequence_analysis._sequence_analysis import _MarkovianSequences
from openalea.sequence_analysis import Estimate
from openalea.sequence_analysis import HiddenSemiMarkov
from openalea.sequence_analysis import Sequences
import os
import random


DATA_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/data'
MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'
POST_PROC_PATH = os.path.join(MODEL_PATH, "postproc")

def generate_random_sequences(K, input_sequences, n_seq):
    def generate_any_random_sequences(random_sequences, input_sequences, K, number_of_sequences_left):
        iseq = input_sequences
        rseq = random_sequences
        n_output_processes = len(iseq[0][0])
        for _ in range(0, number_of_sequences_left):
            sequence_number = random.randint(0, len(iseq) - 1)
            random_sequence = []
            sequence_length = len(iseq[sequence_number])
            number_of_transitions = random.randint(1, min(sequence_length - 1, 3))  # min(sequence_length-1, 7)
            # tirage uniforme sans remise
            transition_instants = sorted(random.sample(np.arange(sequence_length) + 1, number_of_transitions))
            # tirage uniforme avec remise
            # mais pas deux fois le meme qui se suit
            random_sequence = iseq[sequence_number][:]
            current_hidden_state = random.sample(range(0, K), 1)[0]
            random_sequence[0].insert(0, current_hidden_state)
            for i in range(1, sequence_length):
                if i in transition_instants:
                    possible_new_state = range(0, K)
                    possible_new_state.remove(current_hidden_state)
                    current_hidden_state = random.sample(possible_new_state, 1)[0]
                random_sequence[i].insert(0, current_hidden_state)
            rseq.append(random_sequence)
        return rseq
    def remove_invalid_sequences(random_sequences, input_sequences):
        # check that censured freq are always smaller than freq
        # initialization of freq tables
        iseq = input_sequences
        max_seq_len = -1
        for text_reading in iseq:
            if len(text_reading) > max_seq_len:
                max_seq_len = len(text_reading)
        single_run = dict(
            [(key, [0] * max_seq_len) for key in range(0, K)])
        initial_run = dict(
            [(key, [0] * max_seq_len) for key in range(0, K)])
        final_run = dict(
            [(key, [0] * max_seq_len) for key in range(0, K)])
        frequency = dict(
            [(key, [0] * max_seq_len) for key in range(0, K)])
        # build freq tables
        for i in range(0, len(random_sequences)):
            initial_run_bool = True
            current_state = random_sequences[i][0][0]
            it = 0
            global_it = 0
            for j in range(0, len(random_sequences[i])):
                previous_state = current_state
                current_state = random_sequences[i][j][0]
                if previous_state != current_state:
                    if initial_run_bool:
                        initial_run_bool = False
                        initial_run[previous_state][it] += 1
                    else:
                        frequency[previous_state][it] += 1
                    it = 0
                elif it == len(random_sequences[i]):
                    single_run[previous_state][it] += 1
                elif global_it == len(random_sequences[i]):
                    final_run[current_state][it] += 1
                it += 1
                global_it += 1
        # max freq for non censored sojourn time
        max_seq_len_per_hidden_space = []
        for i in range(0, K):
            for j in range(max_seq_len - 1, 0, -1):
                if frequency[i][j] != 0:
                    max_seq_len_per_hidden_space.append(j)
                    break
            if len(max_seq_len_per_hidden_space) <= i:
                max_seq_len_per_hidden_space.append(0)

        # seq to be removed
        # (which have censored time longer than non censored time)
        pop_list = []
        for i in range(0, len(random_sequences)):
            initial_run_bool = True
            current_state = random_sequences[i][0][0]
            it = 0
            global_it = 0
            for j in range(0, len(random_sequences[i])):
                previous_state = current_state
                current_state = random_sequences[i][j][0]
                if previous_state != current_state:
                    if initial_run_bool:
                        initial_run_bool = False
                        initial_run[previous_state][it] += 1
                        if it >= max_seq_len_per_hidden_space[previous_state]:
                            pop_list.append(i)
                            break
                    else:
                        frequency[previous_state][it] += 1
                    it = 0
                elif it == len(random_sequences[i]):
                    single_run[previous_state][it] += 1
                    if it >= max_seq_len_per_hidden_space[previous_state]:
                        pop_list.append(i)
                        break
                elif global_it == len(random_sequences[i]):
                    final_run[current_state][it] += 1
                    if it >= max_seq_len_per_hidden_space[previous_state]:
                        pop_list.append(i)
                        break
                it += 1
                global_it += 1

        pop_list.sort()
        for _ in range(len(pop_list), 0, -1):
            random_sequences.pop(pop_list.pop())
        return random_sequences

    random_sequences = []
    i = 0
    while len(random_sequences) < n_seq:
        i += 1
        random_sequences = generate_any_random_sequences(random_sequences, input_sequences, K,
                                                         n_seq - len(random_sequences))
        random_sequences = remove_invalid_sequences(random_sequences, input_sequences)
    return random_sequences


def generate_hypercube_jitter_init(hsmc_file, K, V):
    pi = np.random.dirichlet(np.ones(K))
    A = np.random.dirichlet(np.ones(K), K)
    np.fill_diagonal(A, 0)
    A = A / np.sum(A, axis=1)[:, np.newaxis]
    B = np.random.dirichlet(np.ones(V), K)
    with open(hsmc_file, 'w') as f:
        f.write('HIDDEN_SEMI-MARKOV_CHAIN\n\n')
        f.write(str(K) + ' STATES\n\n')
        f.write('INITIAL_PROBABILITIES\n')
        for i in range(K):
            f.write(str(pi[i]))
            if i != K - 1:
                f.write('     ')
        f.write('\n\n')
        f.write('TRANSITION_PROBABILITIES\n')
        for i in range(K):
            for j in range(K):
                f.write(str(A[i, j]))
                if j != K - 1:
                    f.write('     ')
            f.write('\n')
        f.write('\n\n')
        for i in range(K):
            f.write('STATE ' + str(i) + ' OCCUPANCY_DISTRIBUTION\n')
            f.write('NEGATIVE_BINOMIAL   INF_BOUND : 1   PARAMETER : 1   PROBABILITY : 0.1\n\n')
        f.write('1 OUTPUT_PROCESS\n\nOUTPUT_PROCESS 1 : CATEGORICAL\n\n')
        for i in range(K):
            f.write('STATE ' + str(i) + ' OBSERVATION_DISTRIBUTION\n')
            for j in range(V):
                f.write('OUTPUT ' + str(j) + ' : ' + str(B[i, j]))
                f.write('\n')
            f.write('\n')

random.seed(0)  # find seed which causes remove_seq to fail and debug

nb_seq = 100
seq_len = 100
K=5
V=5
nb_iter = 500
n_init = 100

synthetic_model = HiddenSemiMarkov(os.path.join(MODEL_PATH, 'synthetic.hsmc'))
synthetic_seqs = synthetic_model.simulation_nb_sequences(nb_seq, seq_len, True)
synthetic_seqs = [[value[1] for value in seq] for seq in synthetic_seqs]

# add noise
n_noise = 2000
for _ in range(0, n_noise):
    i_seq = np.random.randint(nb_seq)
    pos_seq = np.random.randint(seq_len)
    obs_val = np.random.randint(V)
    #synthetic_seqs[i_seq].insert(pos_seq, obs_val)
    synthetic_seqs[i_seq][pos_seq] = obs_val
synthetic_seqs = _MarkovianSequences(Sequences(synthetic_seqs))

# check that the model can be recovered
estimated_model_real_init = Estimate(synthetic_seqs, "HIDDEN_SEMI-MARKOV", synthetic_model,
                                     NbIteration=nb_iter)
estimated_model_real_init.save(os.path.join(MODEL_PATH, 'noise_estimated_model_real_init.hsmc'))
print estimated_model_real_init.get_likelihood()

# standard estimation
estimated_model_standard_init = Estimate(synthetic_seqs, 'HIDDEN_SEMI-MARKOV', 'Ordinary', K, 'Irreducible',
                                         OccupancyMean='Estimated', NbIteration=nb_iter,
                                         Estimator='CompleteLikelihood', StateSequences='Viterbi', Counting=False)
estimated_model_standard_init.save(os.path.join(MODEL_PATH, 'noise_estimated_model_standard_init.hsmc'))
print estimated_model_standard_init.get_likelihood()

sb_ll = []
sb_bic = []
hcj_ll = []
hcj_bic = []
for init_index in range(n_init):
    print('init %d' % init_index)
    # sequence breaking initialization
    random_sequences = generate_random_sequences(K, synthetic_seqs, 50)
    init_sb_synthetic_model = Estimate(_MarkovianSequences(Sequences(random_sequences)),
                                       "SEMI-MARKOV", 'Ordinary')
    init_sb_synthetic_model_file = os.path.join(MODEL_PATH, 'noise2_init_sb_synthetic.hsmc')
    init_sb_synthetic_model.write_hidden_semi_markov_init_file(init_sb_synthetic_model_file)
    init_sb_synthetic_model = HiddenSemiMarkov(init_sb_synthetic_model_file)
    estimated_model_sb_init = Estimate(synthetic_seqs, "HIDDEN_SEMI-MARKOV", init_sb_synthetic_model,
                                       NbIteration=nb_iter)
    estimated_model_sb_init.save(os.path.join(MODEL_PATH, 'noise2_estimated_model_sb_init.hsmc'))
    print('estimated_model_sb_init %.2f' % estimated_model_sb_init.get_likelihood())
    sb_ll.append(estimated_model_sb_init.get_likelihood_vector())
    sb_bic.append(estimated_model_sb_init.get_bic_vector())
    # hypercube jitter initialization
    init_hcj_synthetic_model_file = os.path.join(MODEL_PATH, 'noise2_init_hcj_synthetic.hsmc')
    generate_hypercube_jitter_init(init_hcj_synthetic_model_file, K=K, V=V)
    init_hcj_synthetic_model = HiddenSemiMarkov(init_hcj_synthetic_model_file)
    estimated_model_hcj_init = Estimate(synthetic_seqs, "HIDDEN_SEMI-MARKOV", init_hcj_synthetic_model,
                                        NbIteration=nb_iter)
    estimated_model_hcj_init.save(os.path.join(MODEL_PATH, 'noise2_estimated_model_hcj_init.hsmc'))
    print('estimated_model_hcj_init %.2f' % estimated_model_hcj_init.get_likelihood())
    hcj_ll.append(estimated_model_hcj_init.get_likelihood_vector())
    hcj_bic.append(estimated_model_hcj_init.get_bic_vector())
np.save(os.path.join(MODEL_PATH, 'noise2_sb_ll.npy'), np.array(sb_ll))
np.save(os.path.join(MODEL_PATH, 'noise2_sb_bic.npy'), np.array(sb_bic))
np.save(os.path.join(MODEL_PATH, 'noise2_hcj_ll.npy'), np.array(hcj_ll))
np.save(os.path.join(MODEL_PATH, 'noise2_hcj_bic.npy'), np.array(hcj_bic))

"""
em_data = pd.read_excel(os.path.join(DATA_PATH,'em-y35-fasttext.xlsx'))
em_data.at[em_data.WINC > 1, 'READMODE'] = 4
em_data.at[em_data.WINC == 1, 'READMODE'] = 3
em_data.at[em_data.WINC == 0, 'READMODE'] = 2
em_data.at[em_data.WINC == -1, 'READMODE'] = 1
em_data.at[em_data.WINC < -1, 'READMODE'] = 0

em_df = EyeMovementDataFrame(em_data)

model = Model(em_df, k=5, output_process_name=['READMODE'],
              output_path=MODEL_PATH, benchmark_random_init=True, n_init=100, n_iter_init=1000, n_random_seq=50)

model2 = Model(em_df, k=5, output_process_name=['READMODE'],
              output_path=MODEL_PATH, hypercube_jitter_init=True, n_init=100, n_iter_init=1000)
"""