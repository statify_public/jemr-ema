# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

#%%
import matplotlib.pyplot as plt
import numpy as np
import subprocess

plt.style.use('seaborn')

n_inits = np.array([1, 3, 7, 20, 50, 150, 400, 1000])
n_iters = np.array([20, 50, 150, 400, 1000])  # , 3000])

best_ll = np.load('/home/bolivier/benchmark-random-init/max-ll.npy')
best_bic = np.load('/home/bolivier/benchmark-random-init/max-bic.npy')
best_icl = np.load('/home/bolivier/benchmark-random-init/max-icl.npy')

"""
for j in range(len(n_inits)):
    parr = []
    for i in range(best_ll.shape[0]):
        parr.append(plt.plot(n_iters, best_ll[j, :, :].T))

    plt.xlabel('EM iterations')
    plt.ylabel('LL')
    plt.legend((p[0] for p in parr), ('Best after %d iter' % best_it for best_it in n_iters))
    plt.title('Best after %d init' % n_inits[j])
    plt.savefig('/home/bolivier/benchmark-random-init/plots/%d-ll-ba%dinit.png' % (j, n_inits[j]))
    plt.show()


for j in range(len(n_inits)):
    parr = []
    for i in range(best_bic.shape[0]):
        parr.append(plt.plot(n_iters, best_bic[j, :, :].T))

    plt.xlabel('EM iterations')
    plt.ylabel('BIC')
    plt.legend((p[0] for p in parr), ('Best after %d iter' % best_it for best_it in n_iters))
    plt.title('Best after %d init' % n_inits[j])
    plt.savefig('/home/bolivier/benchmark-random-init/plots/%d-bic-ba%dinit.png' % (j, n_inits[j]))
    plt.show()


for j in range(len(n_inits)):
    parr = []
    for i in range(best_icl.shape[0]):
        parr.append(plt.plot(n_iters, best_icl[j, :, :].T))

    plt.xlabel('EM iterations')
    plt.ylabel('LL')
    plt.legend((p[0] for p in parr), ('Best after %d iter' % best_it for best_it in n_iters))
    plt.title('Best after %d init' % n_inits[j])
    plt.savefig('/home/bolivier/benchmark-random-init/plots/%d-icl-ba%dinit.png' % (j, n_inits[j]))
    plt.show()

"""
#%%
parr = []
for i in range(3, best_ll.shape[0] - 1):
    parr.append(plt.plot(n_iters, best_ll[i, -1, :].T))
plt.xlabel('EM iterations')
plt.ylabel('LL')
plt.legend((p[0] for p in parr), ('Best after %d init' % best_init for best_init in n_inits[3:-1]))
plt.savefig('/home/bolivier/benchmark-random-init/plots/ll-ba-n-inits-n-iters.png')
plt.show()

#%%
parr = []
for i in range(0, best_bic.shape[0] - 1):
    parr.append(plt.plot(n_iters, best_bic[i, -1, :].T))
plt.xlabel('EM iterations')
plt.ylabel('LL')
plt.legend((p[0] for p in parr), ('Best after %d init' % best_init for best_init in n_inits[0:-1]))
plt.savefig('/home/bolivier/benchmark-random-init/plots/bic-ba-n-inits-n-iters.png')
plt.show()

#%%
parr = []
for i in range(0, best_icl.shape[0] - 1):
    parr.append(plt.plot(n_iters, best_icl[i, -1, :].T))
plt.xlabel('EM iterations')
plt.ylabel('ICL')
plt.legend((p[0] for p in parr), ('Best after %d init' % best_init for best_init in n_inits[0:-1]))
plt.savefig('/home/bolivier/benchmark-random-init/plots/icl-ba-n-inits-n-iters.png')
plt.show()
