# -*- coding: utf-8 -*-
__author__ = 'Brice Olivier'

import numpy as np
import pandas as pd
from ema import EyeMovementDataFrame
from ema import Model
import os
from openalea.sequence_analysis import Estimate

DATA_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/data/'
MODEL_PATH = '/home/bolivier/PyENE/PyENE-analysis/em-analysis/share/models'

def redefine_readmode(dataframe, type=0):
    df = dataframe.copy()
    if type == 0:
        df.at[df.WINC > 1, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    if type == 1:
        df['READMODE'] = 5
        df.at[df.WINC == 2, 'READMODE'] = 4
        df.at[df.WINC == 1, 'READMODE'] = 3
        df.at[df.WINC == 0, 'READMODE'] = 2
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    elif type == 2:
        df['READMODE'] = 6
        df.at[df.WINC == 2, 'READMODE'] = 5
        df.at[df.WINC == 1, 'READMODE'] = 4
        df.at[df.WINC == 0, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 2
        df.at[df.WINC == -2, 'READMODE'] = 1
        df.at[df.WINC < -2, 'READMODE'] = 0
    elif type == 3:
        df['READMODE'] = 2
        df.at[df.WINC > 2, 'READMODE'] = 4
        df.at[df.WINC == 2, 'READMODE'] = 3
        df.at[df.WINC == -1, 'READMODE'] = 1
        df.at[df.WINC < -1, 'READMODE'] = 0
    df['READMODE'] = pd.Categorical(df.READMODE)
    return df

dfA = pd.read_csv(os.path.join(DATA_PATH, 'dataY35Y50/fixationFile_A.extraColumns_Y35.csv'), sep=" ", header=None, lineterminator="\n")
dfM = pd.read_csv(os.path.join(DATA_PATH, 'dataY35Y50/fixationFile_M.extraColumns_Y35.csv'), sep=" ", header=None, lineterminator="\n")
dfF = pd.read_csv(os.path.join(DATA_PATH, 'dataY35Y50/fixationFile_F.extraColumns_Y35.csv'), sep=" ", header=None, lineterminator="\n")
df = pd.concat([dfA, dfM, dfF], ignore_index=True)
col_names = ["SUBJ", "SUBJ_NAME", "TEXT_NO", "TEXT", "ANSWER", "FIX_NUM",
             "FIX_LATENCY", "X", "Y", "FDUR", "OFF_DUR", "SACAMP", "SACOR", "INEEG",
             "ISFIRST", "ISLAST", "READMODE", "WINC", "CINC", "FIXED_WORD", "FIXED_WINDOW",
             "WORD_FREQUENCY", "COS_INST", "COS_CUM"]
col_names = dict(zip(range(0,24), col_names))
df = df.rename(index=str, columns=col_names)
df.index = df.index.map(int)
df = df.reset_index(drop=True)

df = df[(df.READMODE != '_TOBEREPLACED_') | (df.ISLAST == 1)]
df.loc[df.READMODE == '_TOBEREPLACED_', 'READMODE'] = np.NaN
df = df.reset_index(drop=True)
def drop_na_fixations(dataframe):
    df = dataframe.copy()
    index_to_drop = df.READMODE.isnull()
    set_is_last = []
    # in case the fixations before the last one also have wfreq < 0
    for i in df.index[index_to_drop & (df.ISLAST == 1)]:
        j = i-1
        while pd.isnull(df.at[j, 'READMODE']):
            j -= 1
        set_is_last.append(j)

    set_is_first = []
    for i in df.index[index_to_drop & (df.ISFIRST == 1)]:
        j = i+1
        while pd.isnull(df.at[j, 'READMODE']):
            j += 1
        set_is_first.append(j)

    df.at[set_is_last, 'ISLAST'] = 1
    df.at[set_is_first, 'ISFIRST'] = 1
    df = df[~index_to_drop]
    df = df.reset_index(drop=True)
    return df
df = drop_na_fixations(df)
df.WINC = df.WINC.astype(int)

data_all = df
data_human_filter = pd.read_excel(os.path.join(DATA_PATH,'em-y35-fasttext.xlsx'))
df_hf4 = data_human_filter[data_human_filter.SUBJ_NAME != 's04']
df_hf4 = df_hf4.reset_index(drop=True)


data_all = redefine_readmode(data_all, type=0)
em_df = EyeMovementDataFrame(data_all)
our5_path = os.path.join(MODEL_PATH, 'model3_init5abs_rev_v2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'all_rm1_ki_5.hsmc'))

data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'model3_init5abs_rev_v2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'hf_rm1_ki_5.hsmc'))

rseqs = mour5.hsmm.extract_data()
np.set_printoptions(precision=2, suppress=True)

empirical_pi = np.zeros(5)
for seq in rseqs:
    empirical_pi[seq[0][0]] +=1

last_pi = np.zeros(6)
for seq in rseqs:
    last_pi[seq[-1][0]] +=1

empirical_A = np.zeros((5,5))
for seq in rseqs:
    last_state = -1
    current_state = -1
    for elem in seq:
        current_state = elem[0]
        if (last_state != -1) & (current_state != last_state):
            empirical_A[last_state, current_state] +=1
        last_state = current_state
empirical_A

empirical_B = np.zeros((5,5))
for seq in rseqs:
    for elem in seq:
        empirical_B[elem[0], elem[1]] +=1
empirical_B


df_hf4 = redefine_readmode(df_hf4, type=0)
em_df = EyeMovementDataFrame(df_hf4)
our5_path = os.path.join(MODEL_PATH, 'model3_init5abs_rev_v2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'hf4_rm1_ki_5.hsmc'))

for readmode_type in range(1, 4):
    em_data = redefine_readmode(data_human_filter, type=readmode_type)
    em_df = EyeMovementDataFrame(em_data)
    init_file = os.path.join(MODEL_PATH, 'model_init_knowledge_readmode_' + str(readmode_type) + '.hsmc')
    model = Model(em_df, init_hsmc_file=init_file, output_path=MODEL_PATH)
    model.iterate_em(3500)
    model.hsmm.save(os.path.join(MODEL_PATH, 'model_knowledge_readmode_' + str(readmode_type) + '.hsmc'))


data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
m = Model(em_df, k=3, output_path=MODEL_PATH)
m.iterate_em(3500)
m.hsmm.save(os.path.join(MODEL_PATH, 'hf4_rm1_ki_3.hsmc'))

m = Model(em_df, k=4, output_path=MODEL_PATH)
m.iterate_em(3500)
m.hsmm.save(os.path.join(MODEL_PATH, 'hf4_rm1_ki_4.hsmc'))

m = Model(em_df, k=6, output_path=MODEL_PATH)
m.iterate_em(3500)
m.hsmm.save(os.path.join(MODEL_PATH, 'hf4_rm1_ki_6.hsmc'))

# Simola
em_df['LOG_FDUR'] = np.log(em_df['FDUR'])
em_df['LOG_FDUR'] = (em_df['LOG_FDUR'] - em_df['LOG_FDUR'].mean()) / em_df['LOG_FDUR'].std()
em_df['LOG_SACAMP'] = np.log(em_df['SACAMP'].astype(float))
em_df['LOG_SACAMP'] = (em_df['LOG_SACAMP'] - em_df['LOG_SACAMP'].mean()) / em_df['LOG_SACAMP'].std()

em_df['REPASS'] = 0
em_df['FIXED_WORD2']= em_df.FIXED_WORD.apply(lambda x: x.split('~')[0])

for idx, val in enumerate(em_df['SACDIR'].unique()):
    em_df.loc[em_df['SACDIR'] == val, 'SACDIR'] = idx
#em_df['SACDIR'] = pd.Categorical(em_df['SACDIR'])

word_list = []
for i in em_df.index:
    if em_df.loc[i, 'ISFIRST']:
        word_list = []
    elif em_df.loc[i, 'FIXED_WORD2'] in word_list:
        em_df.loc[i, 'REPASS'] = 1
    word_list.append(em_df.loc[i, 'FIXED_WORD2'])


m = Model(em_df, k=5, output_path=MODEL_PATH, output_process_name=['LOG_FDUR', 'LOG_SACAMP', 'SACDIR','REPASS'])
m.iterate_em(3500)


hsmm = Estimate(em_df.get_input_sequence(['LOG_FDUR', 'LOG_SACAMP', 'SACDIR','REPASS']),
                'HIDDEN_SEMI-MARKOV', 'Ordinary', 3, 'Irreducible',
                     OccupancyMean='Estimated', NbIteration=1000,
                     Estimator='CompleteLikelihood', StateSequences='Viterbi', Counting=False)

hsmm.save(os.path.join(MODEL_PATH, 'hf_simola_ki_3.hsmc'))


# statistics
agg_df = em_df.groupby(['SUBJ_NAME', 'TEXT']).size().reset_index(name='counts')
agg_df.mean()
agg_df.std()
agg_df.loc[agg_df['SUBJ_NAME'] == 's04'].mean()
agg_df.loc[agg_df['SUBJ_NAME'] == 's04'].std()

# bootstrap checking
trial_da = data_all.groupby(['SUBJ_NAME', 'TEXT']).size().reset_index(name='counts')
trial_dhf = data_human_filter.groupby(['SUBJ_NAME', 'TEXT']).size().reset_index(name='counts')

tda = []
for i in data_all[data_all['ISFIRST'] == 1].index:
    tda.append(data_all.loc[i, 'SUBJ_NAME'] + '+' + data_all.loc[i, 'TEXT'])

tdhf = []
for i in data_human_filter[data_human_filter['ISFIRST'] == 1].index:
    tdhf.append(data_human_filter.loc[i, 'SUBJ_NAME'] + '+' + data_human_filter.loc[i, 'TEXT'])

filtered_trials = np.setdiff1d(tda, tdhf)
other_trials = np.intersect1d(tdhf, tda)

#
data_all['TRIAL_ID'] = data_all['SUBJ_NAME'] + '+' + data_all['TEXT']
data_human_filter['TRIAL_ID'] = data_human_filter['SUBJ_NAME'] + '+' + data_human_filter['TEXT']

B=100
bootstrap_scores = np.zeros((B, 4))
for b in range(B):
    print('bootstrap iter %d' % b)
    other_sample = np.random.choice(other_trials, size=len(other_trials))
    filtered_train = np.random.choice(filtered_trials, size=len(filtered_trials)/2, replace=False)
    filtered_test = np.setdiff1d(filtered_trials, filtered_train)

    df_train_1 = [data_all[data_all['TRIAL_ID'] == trial] for trial in other_sample]
    df_train_1 = pd.concat(df_train_1)
    df_train_1 = df_train_1.reset_index(drop=True)

    df_train_2 = [data_all[data_all['TRIAL_ID'] == trial] for trial in filtered_train]
    df_train_2 = pd.concat(df_train_2)
    df_train_2 = pd.concat([df_train_1, df_train_2])
    df_train_2 = df_train_2.reset_index(drop=True)

    df_test = [data_all[data_all['TRIAL_ID'] == trial] for trial in filtered_test]
    df_test = pd.concat(df_test)
    df_test = df_test.reset_index(drop=True)

    df_train_1 = redefine_readmode(df_train_1, type=0)
    df_train_2 = redefine_readmode(df_train_2, type=0)
    df_test = redefine_readmode(df_test, type=0)

    df_train_1 = EyeMovementDataFrame(df_train_1)
    df_train_2 = EyeMovementDataFrame(df_train_2)
    df_test = EyeMovementDataFrame(df_test)

    m_train_1 = Estimate(df_train_1.get_input_sequence(['READMODE']), 'HIDDEN_SEMI-MARKOV', 'Ordinary', 5, 'Irreducible',
                         OccupancyMean='Estimated', NbIteration=500, Estimator='CompleteLikelihood',
                         StateSequences='Viterbi', Counting=False)
    m_train_1.save(os.path.join(MODEL_PATH, 'mtrain1_tmp.hsmc'))
    bootstrap_scores[b, 0] = m_train_1.get_likelihood()

    m_train_2 = Estimate(df_train_2.get_input_sequence(['READMODE']), 'HIDDEN_SEMI-MARKOV', 'Ordinary', 5, 'Irreducible',
                         OccupancyMean='Estimated', NbIteration=500, Estimator='CompleteLikelihood',
                         StateSequences='Viterbi', Counting=False)
    m_train_2.save(os.path.join(MODEL_PATH, 'mtrain2_tmp.hsmc'))
    bootstrap_scores[b, 1] = m_train_2.get_likelihood()

    m_test_1 = Model(df_test, init_hsmc_file=os.path.join(MODEL_PATH, 'mtrain1_tmp.hsmc'), output_path=MODEL_PATH)
    m_test_1.iterate_em(1)
    bootstrap_scores[b, 2] = m_test_1.hsmm.get_likelihood()
    print('ll test hdf model %.2f' % m_test_1.hsmm.get_likelihood())
    m_test_2 = Model(df_test, init_hsmc_file=os.path.join(MODEL_PATH, 'mtrain2_tmp.hsmc'), output_path=MODEL_PATH)
    m_test_2.iterate_em(1)
    bootstrap_scores[b, 3] = m_test_2.hsmm.get_likelihood()
    print('ll test all data model %.2f' % m_test_2.hsmm.get_likelihood())

np.save(os.path.join(MODEL_PATH, 'postproc', 'bootstrap_scores'), bootstrap_scores)

# bootstrap s4
trial_dhf = data_human_filter.groupby(['SUBJ_NAME', 'TEXT']).size().reset_index(name='counts')
trial_dhf4 = df_hf4.groupby(['SUBJ_NAME', 'TEXT']).size().reset_index(name='counts')

tdhf4 = []
for i in df_hf4[df_hf4['ISFIRST'] == 1].index:
    tdhf4.append(df_hf4.loc[i, 'SUBJ_NAME'] + '+' + df_hf4.loc[i, 'TEXT'])

tdhf = []
for i in data_human_filter[data_human_filter['ISFIRST'] == 1].index:
    tdhf.append(data_human_filter.loc[i, 'SUBJ_NAME'] + '+' + data_human_filter.loc[i, 'TEXT'])

filtered_trials = np.setdiff1d(tdhf, tdhf4)
other_trials = np.intersect1d(tdhf, tdhf4)

df_hf4['TRIAL_ID'] = df_hf4['SUBJ_NAME'] + '+' + df_hf4['TEXT']
data_human_filter['TRIAL_ID'] = data_human_filter['SUBJ_NAME'] + '+' + data_human_filter['TEXT']

B=100
bootstrap_scores4 = np.zeros((B, 4))
for b in range(B):
    print('bootstrap iter %d' % b)
    other_sample = np.random.choice(other_trials, size=len(other_trials))
    filtered_train = np.random.choice(filtered_trials, size=len(filtered_trials)/2, replace=False)
    filtered_test = np.setdiff1d(filtered_trials, filtered_train)

    df_train_1 = [data_human_filter[data_human_filter['TRIAL_ID'] == trial] for trial in other_sample]
    df_train_1 = pd.concat(df_train_1)
    df_train_1 = df_train_1.reset_index(drop=True)

    df_train_2 = [data_human_filter[data_human_filter['TRIAL_ID'] == trial] for trial in filtered_train]
    df_train_2 = pd.concat(df_train_2)
    df_train_2 = pd.concat([df_train_1, df_train_2])
    df_train_2 = df_train_2.reset_index(drop=True)

    df_test = [data_human_filter[data_human_filter['TRIAL_ID'] == trial] for trial in filtered_test]
    df_test = pd.concat(df_test)
    df_test = df_test.reset_index(drop=True)

    df_train_1 = redefine_readmode(df_train_1, type=0)
    df_train_2 = redefine_readmode(df_train_2, type=0)
    df_test = redefine_readmode(df_test, type=0)

    df_train_1 = EyeMovementDataFrame(df_train_1)
    df_train_2 = EyeMovementDataFrame(df_train_2)
    df_test = EyeMovementDataFrame(df_test)

    m_train_1 = Estimate(df_train_1.get_input_sequence(['READMODE']), 'HIDDEN_SEMI-MARKOV', 'Ordinary', 5, 'Irreducible',
                         OccupancyMean='Estimated', NbIteration=500, Estimator='CompleteLikelihood',
                         StateSequences='Viterbi', Counting=False)
    m_train_1.save(os.path.join(MODEL_PATH, 'mtrain1_tmp.hsmc'))
    bootstrap_scores4[b, 0] = m_train_1.get_likelihood()

    m_train_2 = Estimate(df_train_2.get_input_sequence(['READMODE']), 'HIDDEN_SEMI-MARKOV', 'Ordinary', 5, 'Irreducible',
                         OccupancyMean='Estimated', NbIteration=500, Estimator='CompleteLikelihood',
                         StateSequences='Viterbi', Counting=False)
    m_train_2.save(os.path.join(MODEL_PATH, 'mtrain2_tmp.hsmc'))
    bootstrap_scores4[b, 1] = m_train_2.get_likelihood()

    m_test_1 = Model(df_test, init_hsmc_file=os.path.join(MODEL_PATH, 'mtrain1_tmp.hsmc'), output_path=MODEL_PATH)
    m_test_1.iterate_em(1)
    bootstrap_scores4[b, 2] = m_test_1.hsmm.get_likelihood()
    print('ll test hdf model %.2f' % m_test_1.hsmm.get_likelihood())
    m_test_2 = Model(df_test, init_hsmc_file=os.path.join(MODEL_PATH, 'mtrain2_tmp.hsmc'), output_path=MODEL_PATH)
    m_test_2.iterate_em(1)
    bootstrap_scores4[b, 3] = m_test_2.hsmm.get_likelihood()
    print('ll test all data model %.2f' % m_test_2.hsmm.get_likelihood())

np.save(os.path.join(MODEL_PATH, 'postproc', 'bootstrap_scores4'), bootstrap_scores4)

# thesis k4 SB model
data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'fav-k4-init-abs.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'k4-thesis.hsmc'))

rseqs = mour5.hsmm.extract_data()
empirical_pi = np.zeros(4)
for seq in rseqs:
    empirical_pi[seq[0][0]] +=1

last_pi = np.zeros(4)
for seq in rseqs:
    last_pi[seq[-1][0]] +=1

empirical_A = np.zeros((4,4))
for seq in rseqs:
    last_state = -1
    current_state = -1
    for elem in seq:
        current_state = elem[0]
        if (last_state != -1) & (current_state != last_state):
            empirical_A[last_state, current_state] +=1
        last_state = current_state
empirical_A


# thesis k4 spurious model
data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'best_bic_model_k4_abs2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(10)
#mour5.hsmm.save(os.path.join(MODEL_PATH, 'best_bic_model_k4_abs2.hsmc'))

rseqs = mour5.hsmm.extract_data()
empirical_pi = np.zeros(4)
for seq in rseqs:
    empirical_pi[seq[0][0]] +=1
empirical_pi
last_pi = np.zeros(4)
for seq in rseqs:
    last_pi[seq[-1][0]] +=1
last_pi
empirical_A = np.zeros((4,4))
for seq in rseqs:
    last_state = -1
    current_state = -1
    for elem in seq:
        current_state = elem[0]
        if (last_state != -1) & (current_state != last_state):
            empirical_A[last_state, current_state] +=1
        last_state = current_state
empirical_A


# thesis k3 init knowledge
data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'k3-init-knowledge.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)
mour5.hsmm.save(os.path.join(MODEL_PATH, 'k3-knowledge.hsmc'))

rseqs = mour5.hsmm.extract_data()
empirical_pi = np.zeros(4)
for seq in rseqs:
    empirical_pi[seq[0][0]] +=1

last_pi = np.zeros(4)
for seq in rseqs:
    last_pi[seq[-1][0]] +=1

empirical_A = np.zeros((4,4))
for seq in rseqs:
    last_state = -1
    current_state = -1
    for elem in seq:
        current_state = elem[0]
        if (last_state != -1) & (current_state != last_state):
            empirical_A[last_state, current_state] +=1
        last_state = current_state
empirical_A

# thesis model k5 init abs
data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'model_k5abs_rev_v2.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(3500)

phase_dur = {0: [], 1: [], 2: [], 3: []}

rdf = mour5.eye_movement_data.restored_data.copy()
last_state = -1
current_state_duration = 0
for i in rdf.index:
    current_state = rdf.loc[i, 'PHASE']
    if rdf.loc[i, 'ISFIRST']:
        last_state = -1
        current_state_duration = 0
    elif (last_state != current_state) or rdf.loc[i, 'ISLAST']:
        current_state_duration += rdf.loc[i, 'FDUR']
        phase_dur[rdf.loc[i, 'PHASE']].append(current_state_duration)

mour5.eye_movement_data.restored_data.groupby(['PHASE'])['FDUR'].mean()

# restorations
data_human_filter = redefine_readmode(data_human_filter, type=0)
em_df = EyeMovementDataFrame(data_human_filter)
our5_path = os.path.join(MODEL_PATH, 'model_k5abs_rev_v3.hsmc')
mour5 = Model(em_df, init_hsmc_file=our5_path, output_path=MODEL_PATH)
mour5.iterate_em(50)

from ema import html_report
import random
random.seed(1)
r = html_report.Htmlreport(mour5, number_of_text_restorations_to_display=1000, phase=True,
                           png_path='/home/bolivier/PyENE/PyENE-data/em-analysis/data/Materiel')
r.make_html(True)

em_df2 = em_df.copy()
em_df2 = EyeMovementDataFrame(em_df2)
our4_path = os.path.join(MODEL_PATH, 'k4-thesis.hsmc')
mour4 = Model(em_df2, init_hsmc_file=our4_path, output_path=MODEL_PATH)
mour4.iterate_em(50)
r2 = html_report.Htmlreport(mour4, number_of_text_restorations_to_display=500, phase=False,
                           png_path='/home/bolivier/PyENE/PyENE-data/em-analysis/data/Materiel')
r2.make_html(True)

# state profile plot
from openalea.sequence_analysis import Plot

Plot(mour5.hsmm, 1, ViewPoint="StateProfile")
Plot(mour4.hsmm, 1, ViewPoint="StateProfile")

Plot(mour5.hsmm, 2, ViewPoint="StateProfile")
Plot(mour4.hsmm, 2, ViewPoint="StateProfile")

Plot(mour5.hsmm, 3, ViewPoint="StateProfile")
Plot(mour4.hsmm, 3, ViewPoint="StateProfile")

Plot(mour5.hsmm, 4, ViewPoint="StateProfile")
Plot(mour4.hsmm, 4, ViewPoint="StateProfile")

Plot(mour5.hsmm, 5, ViewPoint="StateProfile")
Plot(mour4.hsmm, 5, ViewPoint="StateProfile")

Plot(mour5.hsmm, 6, ViewPoint="StateProfile")
Plot(mour4.hsmm, 6, ViewPoint="StateProfile")

Plot(mour5.hsmm, 7, ViewPoint="StateProfile")
Plot(mour4.hsmm, 7, ViewPoint="StateProfile")

Plot(mour5.hsmm, 8, ViewPoint="StateProfile")
Plot(mour4.hsmm, 8, ViewPoint="StateProfile")

Plot(mour5.hsmm, 9, ViewPoint="StateProfile")
Plot(mour4.hsmm, 9, ViewPoint="StateProfile")

Plot(mour5.hsmm, 10, ViewPoint="StateProfile")
Plot(mour4.hsmm, 10, ViewPoint="StateProfile")

Plot(mour5.hsmm, 11, ViewPoint="StateProfile")
Plot(mour4.hsmm, 11, ViewPoint="StateProfile")

Plot(mour5.hsmm, 12, ViewPoint="StateProfile")
Plot(mour4.hsmm, 12, ViewPoint="StateProfile")
