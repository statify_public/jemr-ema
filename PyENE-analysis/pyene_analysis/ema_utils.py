# -*- coding: utf-8 -*-
"""
Various utility functions for post hoc analysis of eye-movement data
"""

import numpy as np
import pandas as pd
from collections import Counter

def transition_target(rdf, max_fst, nb_text_type_2):
    """
    Compute distances between transitions and closest target word
    
    :Parameters:
        `rdf` - data frame with restored states and phases
        `max_fst` - maximal number of fixations per trial
        `nb_text_type_2` - number of types of texts (type 2)
        
    :Returns:
        `rdfD` - texts of each type
        `transition_target_distance_D` - distance table 
            (distance, incoming phase)
        `freq_transition_target_distance_D` - frequencies of 
            distance for each phase (normalised by all possible 
            distances for a given incoming phase)
        `TD_df` - Dataframe of binary counts: subject, text_type2, 
            incoming phase, distance
        `st_df` - Number of fixations per subject and text number
        `dist_fix_max_cos` - Distance of fixations to closest 
            trigger words
    """
    # texts of each type
    rdfD = {} 
    # distance table (distance, incoming phase)
    transition_target_distance_D = {} 
    # frequencies of distance for each phase 
    # (normalised by all possible distances for a given incoming phase)
    freq_transition_target_distance_D = {} 
    
     # List of binary counts: subject, text_type2, incoming phase, distance
    TD = []
    # Note that criteria and distances differ for 3 types of text

    nb_phases = len(set(rdf.PHASE))


    # HR and HR+ texts

    for tt in ['HR', 'HR+']:
        rdfD[tt] = rdf.loc[rdf.TEXT_TYPE_2 == tt]
        rdfD[tt] = rdfD[tt].reset_index()
        transition_target_distance_D[tt] = np.zeros((max_fst, nb_phases))
        scanpath_transition = []
        scanpath_id = 0
        for i in rdfD[tt].index:
            if rdfD[tt].at[i, 'ISFIRST']:
                j = i
                cos_inst_max_1 = 0
                cos_inst_max_2 = 0
                jwmax_1 = np.infty
                jwmax_2 = np.infty
                while not rdfD[tt].at[j, 'ISLAST']:
                    if rdfD[tt].at[j, 'COS_INST'] > cos_inst_max_1:
                        cos_inst_max_2 = cos_inst_max_1
                        jwmax_2 = jwmax_1
                        cos_inst_max_1 = rdfD[tt].at[j, 'COS_INST']
                        jwmax_1 = j
                    elif rdfD[tt].at[j, 'COS_INST'] > cos_inst_max_2:
                        cos_inst_max_2 = rdfD[tt].at[j, 'COS_INST']
                        jwmax_2 = j
                    j += 1
            elif rdfD[tt].at[i, 'PHASE'] != previous_state:
                if cos_inst_max_1 > 0.3:
                    if cos_inst_max_2 > 0.3:
                        dist = min(abs(jwmax_1 - i), abs(jwmax_2 - i))
                    else:
                        dist = abs(jwmax_1 - i)
                    transition_target_distance_D[tt][dist, rdfD[tt].at[i, 'PHASE']] += 1
                    TD.append([rdfD[tt].at[i, 'SUBJ'], tt, rdfD[tt].at[i, 'PHASE'], dist])
            previous_state = rdfD[tt].at[i, 'PHASE']
        freq_transition_target_distance_D[tt] = transition_target_distance_D[tt] / transition_target_distance_D[tt].sum(axis=0)    

    # UR texts￼
    tt = 'UR'
    rdfD['UR'] = rdf.loc[rdf.TEXT_TYPE == 'UR']
    rdfD['UR'] = rdfD['UR'].reset_index()
    transition_target_distance_D['UR'] = np.zeros((max_fst, nb_phases))
    scanpath_transition = []
    scanpath_id = 0
    for i in rdfD['UR'].index:
        if rdfD['UR'].at[i, 'ISFIRST']:
            j = i
            cos_inst_min_1 = 1
            cos_inst_min_2 = 1
            jwmin_1 = np.infty
            jwmin_2 = np.infty
            while not rdfD['UR'].at[j, 'ISLAST']:
                if rdfD['UR'].at[j, 'COS_INST'] < cos_inst_min_1:
                    cos_inst_min_2 = cos_inst_min_1
                    jwmin_2 = jwmin_1
                    cos_inst_min_1 = rdfD['UR'].at[j, 'COS_INST']
                    jwmin_1 = j
                elif rdfD['UR'].at[j, 'COS_INST'] > cos_inst_min_2:
                    cos_inst_min_2 = rdfD['UR'].at[j, 'COS_INST']
                    jwmin_2 = j
                j += 1
        elif rdfD['UR'].at[i, 'PHASE'] != previous_state:
            if jwmin_1 != np.infty:
                dist = min(abs(jwmin_1 - i), abs(jwmin_2 - i))
                transition_target_distance_D['UR'][dist, rdfD['UR'].at[i, 'PHASE']] += 1
                TD.append([rdfD[tt].at[i, 'SUBJ'], tt, rdfD[tt].at[i, 'PHASE'], dist])
        previous_state = rdfD['UR'].at[i, 'PHASE']
    freq_transition_target_distance_D['UR'] = transition_target_distance_D['UR'] / transition_target_distance_D['UR'].sum(axis=0)

    # M texts   ￼
    tt = 'MR'
    rdfD['MR'] = rdf.loc[rdf.TEXT_TYPE_2 == 'MR']
    rdfD['MR'] = rdfD['MR'].reset_index()
    transition_target_distance_D['MR'] = np.zeros((max_fst, nb_phases))
    scanpath_transition = []
    scanpath_id = 0
    for i in rdfD['MR'].index:
        if rdfD['MR'].at[i, 'ISFIRST']:
            j = i
            cos_inst_min = 1
            cos_inst_max = 0 # Error: 1?
            jwmin = np.infty
            jwmax = np.infty
            while not rdfD['MR'].at[j, 'ISLAST']:
                if rdfD['MR'].at[j, 'COS_INST'] < cos_inst_min:
                    cos_inst_min = rdfD['MR'].at[j, 'COS_INST']
                    jwmin = j
                if rdfD['MR'].at[j, 'COS_INST'] > cos_inst_max:
                    cos_inst_max = rdfD['MR'].at[j, 'COS_INST']
                    jwmax = j
                j += 1
        elif rdfD['MR'].at[i, 'PHASE'] != previous_state:
            dist = min(abs(jwmin - i), abs(jwmax - i))
            if dist != np.infty:
                transition_target_distance_D['MR'][dist, rdfD['MR'].at[i, 'PHASE']] += 1
                TD.append([rdfD[tt].at[i, 'SUBJ'], tt, rdfD[tt].at[i, 'PHASE'], dist])
        previous_state = rdfD['MR'].at[i, 'PHASE']
    freq_transition_target_distance_D['MR'] = transition_target_distance_D['MR'] / transition_target_distance_D['MR'].sum(axis=0)

    # Make a data frame from TD

    TD_df = pd.DataFrame(data=TD, columns = ['SUBJ', 'TEXT_TYPE_2', 'PHASE', 'dist'])

    # Number of fixations per subject and text number

    st_df = pd.crosstab(rdf['TEXT_NO'],  rdf['SUBJ'], margins = False) 

    # Compute the number of scanpaths for each text type
    # In lines PHASE distance varies first, then PHASE, then TEXT_TYPE_2

    # Number of lines:     ​
    nbl_dist_fix_max_cos = max_fst * nb_phases * nb_text_type_2

    dist_fix_max_cos = pd.DataFrame(index=range(0, nbl_dist_fix_max_cos), 
                                    columns=['Text', 'ToPhase', 'Dist', 'Freq'])

    i = 0 # Text_type_2
    for tt in transition_target_distance_D.keys():       
        dist_fix_max_cos.loc[(i*max_fst * nb_phases):((i+1)*max_fst * nb_phases)-1, 'Text'] = tt
        # Incoming states for each type of text
        for s in range(nb_phases):
            dist_fix_max_cos.loc[(i*max_fst * nb_phases + s*max_fst):(i*max_fst * nb_phases + (s+1)*max_fst-1), 
                                'ToPhase'] = s
            dist_fix_max_cos.loc[(i*max_fst * nb_phases + s*max_fst):(i*max_fst * nb_phases + (s+1)*max_fst-1), 
                                'Freq'] = \
                freq_transition_target_distance_D[tt][:, s]
        i += 1

    dist_fix_max_cos['Dist'] = range(0,max_fst)* (nb_phases * nb_text_type_2)

    return (rdfD, transition_target_distance_D, freq_transition_target_distance_D, \
        TD_df, st_df, dist_fix_max_cos)

def phase_permutation_constr(rdf):
    """
    Resample transition locations and permute phase positions 
    (constrain the number of transitions to remain the same)
    """
    perm_rdf = rdf.copy()

    # Find restored scanpaths

    rs = {} # restored scanpaths
    ends = perm_rdf.loc[rdf['ISLAST'] == 1].index.tolist()

    b = 0 # beginning
    i = 0 # index
    for e in ends:
        rs[i] = list(perm_rdf.PHASE[b:(e+1)])
        b = e+1
        i += 1

    b = 0 # beginning
    for s in sorted(rs.keys()):
        e = ends[s] # End of scanpath
        scanp = rs[s]
        trans_loc = [j for j in range(len(scanp)) if (j < len(scanp)-1) and (scanp[j] != scanp[j+1])]
        val_phase = [scanp[v] for v in trans_loc] + [scanp[-1]]
        # Do not permute if no transition
        if len(val_phase) > 1:
            # New locations
            trans_loc = np.sort(np.random.choice(range(len(scanp)-1), size=len(val_phase)-1, replace=False))
            # New order of phases
            val_phase = np.random.choice(val_phase, size=len(val_phase), replace=False)
            # Check that incoming and outgoing phases are distinct
            phase_ctrl = [j for j in range(len(val_phase)) if (j < len(val_phase)-1) and \
                        (val_phase[j] != val_phase[j+1])]
            while (len(phase_ctrl) != len(val_phase)-1):
                val_phase = np.random.choice(val_phase, size=len(val_phase), replace=False)
                phase_ctrl = [j for j in range(len(val_phase)) if (j < len(val_phase)-1) and \
                            (val_phase[j] != val_phase[j+1])]
            p = 0
            scanp = (trans_loc[p]+1) * [val_phase[p]]
            # Add all phases except last transition    
            for p in range(1, len(trans_loc)):
                scanp += (trans_loc[p]-trans_loc[p-1]) * [val_phase[p]]
            # Add last transition
            p += 1
            scanp += (len(rs[s])-len(scanp)) * [val_phase[p]]
            assert Counter(val_phase) == Counter([scanp[v] for v in trans_loc] + [scanp[-1]])
        perm_rdf.loc[b:e,'PHASE'] = scanp
        b = e+1
    return(perm_rdf)
    
def phase_permutation_rand(rdf, phase_freq):
    """
    Resample transition locations and permute phase positions 
    (sample from empirical number of transitions 
    and empirical phase frequencies)
    """
    nb_phases = len(phase_freq)

    perm_rdf = rdf.copy()

    # Find restored scanpaths

    rs = {} # restored scanpaths
    ends = perm_rdf.loc[rdf['ISLAST'] == 1].index.tolist()
    nb_trans = [] # Number of transitions

    b = 0 # beginning
    i = 0 # index
    for e in ends:
        rs[i] = list(perm_rdf.PHASE[b:(e+1)])
        nb_trans += [len([j for j in range(len(rs[i])) if \
            (j < len(rs[i])-1) and (rs[i][j] != rs[i][j+1])])]
        b = e+1
        i += 1

    b = 0 # beginning
    for s in sorted(rs.keys()):
        e = ends[s] # End of scanpath
        scanp = rs[s]
        # New number of transitions
        nt = min(np.random.choice(nb_trans, size=1)[0], len(scanp)-1)
        # New phases
        val_phase = np.random.choice(range(nb_phases), size=nt+1, replace=True, p=phase_freq)
        if nt > 0:
            # New locations
            trans_loc = np.sort(np.random.choice(range(len(scanp)-1), size=nt, replace=False))
            # Check that incoming and outgoing phases are distinct
            phase_ctrl = [j for j in range(len(val_phase)) if (j < len(val_phase)-1) and \
                        (val_phase[j] != val_phase[j+1])]
            while (len(phase_ctrl) != len(val_phase)-1):
                val_phase = np.random.choice(range(nb_phases), size=nt+1, replace=True, p=phase_freq)
                phase_ctrl = [j for j in range(len(val_phase)) if (j < len(val_phase)-1) and \
                            (val_phase[j] != val_phase[j+1])]
            p = 0
            scanp = (trans_loc[p]+1) * [val_phase[p]]
            # Add all phases except last transition    
            for p in range(1, len(trans_loc)):
                scanp += (trans_loc[p]-trans_loc[p-1]) * [val_phase[p]]
            # Add last transition
            p += 1
            scanp += (len(rs[s])-len(scanp)) * [val_phase[p]]
        else:
            scanp = len(scanp) * [val_phase[0]]
        perm_rdf.loc[b:e,'PHASE'] = scanp
        b = e+1

    return(perm_rdf, nb_trans)
