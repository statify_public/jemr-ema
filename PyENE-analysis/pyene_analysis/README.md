# Notebook for oculo hmm article

https://github.com/brice-olivier/PyENE-analysis/blob/master/em-analysis/src/paper_oculo_hmm.ipynb

# directory structure
* data: data files (.csv / .xlsx)
* material: png text files
* model: .hsmc model files
* outputs
    * a-posteriori-analysis-tables: tables created for a posteriori analysis
    * model-scores: model selection scores
    * trigger-words: trigger words found with fasttext
* src: analysis on eye movements
    * config.py: paths to other directories
    * paper_oculo_hmm.ipynb: summary of analysis for oculo hmm paper
    * a_posteriori_analysis_ggplots.R: ggplots of a posteriori analysis