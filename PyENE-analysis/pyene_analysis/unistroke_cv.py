#!/usr/bin/python3.5.2
#!/usr/bin/ipython3

from hmmlearn import hmm
import numpy as np
import matplotlib.pyplot as plt
import os, copy
# import pandas as pd

import warnings, sys
# Disable warnings?
def customwarn(message, category, filename, lineno, file=None, line=None):
    warnings.formatwarning(message, category, filename, lineno)

warnings.showwarning = customwarn

Windows = 'win' in sys.platform
import numpy as np
import matplotlib.pyplot as plt
if not(Windows):
    lin_os = int(os.uname()[2][0])
    if lin_os > 3:
        from matplotlib.pyplot import ion
        ion()
    else:
        print("Use script with spyder3 \n")

#### Simulation

model = hmm.GaussianHMM(n_components=2, covariance_type="full")
model.startprob_ = np.array([1, 0])
model.transmat_ = np.array([[0.92, 0.08], [0, 1]])
model.means_ = np.array([[1.0, 1.0], [1.0, -1.0]])
model.covars_ = 0.2*np.tile(np.identity(2), (2, 1, 1))

np.random.seed(3)

Y, Z = model.sample(25)

X = np.array([np.cumsum(Y[:,0]), np.cumsum(Y[:,1])])
X = X.T # transpose

def LetterPlot(X):
    plt.figure()
    plt.plot(X[:,0], X[:,1], 'bo-')
    plt.show()

LetterPlot(X)

#### Estimation

import os
pathname = "/home/bolivier/unistroke_dataset/unistroke/Unistroke/"
data = os.listdir(pathname)

# keep letter A only
dataA = [s for s in data if 'A' in s]

# Read and plot one trajectory
x_table = np.loadtxt(pathname+dataA[0])
LetterPlot(x_table)

# Read all trajectories
trajA = []
lengths = []
for d in dataA:
    trajd = np.loadtxt(pathname+d)
    trajd = np.diff(trajd, axis=0) 
    norm = np.array(list(map(np.linalg.norm, trajd))) # np.linalg.norm(trajd, axis=1)
    trajd[norm>0] = np.divide(trajd[norm>0], np.array([norm[norm>0],norm[norm>0]]).T)
    trajA += [trajd]
    lengths += [len(trajd)]

YT = np.concatenate(trajA)


# data to angles
angA = []
lengths = []
for d in dataA:
    trajd = np.loadtxt(pathname + d)
    trajd = np.diff(trajd, axis=0)
    ang = [np.arctan2(p[0], p[1]) for p in trajd]
    angA += [ang]
    lengths += [len(trajd)]


hmcA = hmm.GaussianHMM(n_components=2, n_iter=1000, tol=1e-5, verbose=True).fit(YT, lengths)


# Viterbi
Z = hmcA.predict(YT)

from matplotlib.pyplot import cm 

def ViterbiLetterPlot(i, trajA, lengths, Z):
    ns = max(Z) # number of states
    color = iter(cm.rainbow(np.linspace(0,1,ns+2)))
    color = list(color)
    color = ['b','g','r','c','y','k']
    traj = np.array([np.cumsum(trajA[i][:,0]), np.cumsum(trajA[i][:,1])]).T
    cuml = np.array([0] + list(np.cumsum(lengths)))
    ztraj = Z[cuml[i]:cuml[i+1]]
    plt.figure()
    plt.subplot(211)
    for s in range(ns+1):
        c = color[s+1]
        plt.plot(trajA[i][ztraj==s,0], trajA[i][ztraj==s,1], 'o-',c=c)
    plt.subplot(212)
    for s in range(ns+1):
        c = color[s+1]
        plt.plot(traj[ztraj==s,0], traj[ztraj==s,1], 'o-', c=c)
    plt.show()

ViterbiLetterPlot(0, trajA, lengths, Z)

def cross_validate_nb_states(nfolds, KMax, traj):
    """Choice of the number of states"""
    folds = np.random.permutation(range(len(traj)))
    score = [0]*KMax
    traj_per_fold = int(len(traj) / nfolds)
    for i in range(nfolds):
        for k in range(2, KMax+1):
            # test data indices
            if i < nfolds-1:
                test = folds[i*traj_per_fold:((i+1)*traj_per_fold)]
            else:
                test = folds[((nfolds-1)*traj_per_fold):-1]
            # training data indices
            training = list(set(folds).difference(test))
            YT_training = np.concatenate([traj[j] for j in training])
            l_training = [lengths[j] for j in training]
            YT_test = np.concatenate([traj[j] for j in test])
            hmc = hmm.GaussianHMM(n_components=k, n_iter=1000, tol=1e-5, 
                                    verbose=True).fit(YT_training, l_training)
            # Cross-validated log-likelihood
            score[k-1] += hmc.score(YT_test)
    return score


nfolds = 6
KMax = 10 # max number of states

scores = cross_validate_nb_states(nfolds, KMax, trajA)

Kopt = scores.index(max(scores[1:-1]))+1 # optimal number of states
hmcA = hmm.GaussianHMM(n_components=Kopt, n_iter=1000, tol=1e-5, verbose=True).fit(YT, lengths)

# Extension to all letters
for l in ['E', 'H', 'L', 'O', 'Q']:
    command = "data"+ l +" = [s for s in data if '" + l + "' in s]"
    exec(command)
    command = "traj"+ l +" = []"
    exec(command)
    lengths = []
    command = "for d in data"+l+":\n"
    command += "\t trajd = np.loadtxt(pathname+d)\n"
    command += "\t trajd = np.diff(trajd, axis=0)\n" 
    command += "\t norm = np.array(list(map(np.linalg.norm, trajd)))\n"
    command += "\t trajd[norm>0] = np.divide(trajd[norm>0], \
        np.array([norm[norm>0],norm[norm>0]]).T)\n"
    command += "\t traj" + l + "+= [trajd]\n"
    command += "\t lengths += [len(trajd)]\n"
    exec(command)
    command = "YT = np.concatenate(traj" + l + ")"
    exec(command)
    command = "scores = cross_validate_nb_states(nfolds, KMax, traj" + l + ")"
    exec(command)
    Kopt = scores.index(max(scores[1:-1]))+1
    command = "hmc" + l + " = hmm.GaussianHMM(n_components=Kopt, n_iter=1000, "
    command += "tol=1e-5, verbose=True).fit(YT, lengths)"
    exec(command)

dic_model = {'A': copy.copy(hmcA), 'E': copy.copy(hmcE), 'H': copy.copy(hmcH), 'L': copy.copy(hmcL), 'O': copy.copy(hmcO), 'Q': copy.copy(hmcQ)}

# Prediction:
def predict_letter(traj, dic_model):
    """
    Predict letter from trajectory
    """
    best_score = hmcA.score(traj)
    pred = 'A'
    for (l, m) in dic_model.items():
        score = m.score(traj)
        if score > best_score:
            best_score = score
            pred = l
    return(pred)    

# Classification error:

dic_pred = {}
dic_traj = {}
for l in ['A', 'E', 'H', 'L', 'O', 'Q']:
    command = "dic_traj[l] = traj" + l
    exec(command)


for l in ['A', 'E', 'H', 'L', 'O', 'Q']:
    dic_pred[l] = []
    print(dic_model[l].means_)
    for t in dic_traj[l]:
        pred = predict_letter(t, dic_model)
        dic_pred[l] += [pred]

# Contigency table (pandas not working here?)

true_letters = np.array([], dtype=object)
pred_letters = np.array([], dtype=object)
for l in ['A', 'E', 'H', 'L', 'O', 'Q']:
    true_letters = np.append(true_letters, [l] * len(dic_traj[l]))
    pred_letters = np.append(pred_letters, dic_pred[l])
    

1-float(sum(true_letters == pred_letters)) / len(pred_letters)




