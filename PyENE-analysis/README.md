This repo contains all the analysis presented in the PhD thesis.

# Dependencies:
* chapter3-eeg-analysis-R
    * R
    * tidyverse
    * ggplot2
    * fields
    * waveslim
* em-analysis
    * python2.7
    * openalea.sequence_analysis
    * ema https://github.com/PyENE/em-analysis
    * numpy
    * pandas
* fixation-word-assignation
    * perl
* phd-graphs
    * dot

# Content:
chapter3-eeg-analysis-R:
* Script for Raw EEGs -> MODWT into wavelet coefficient per phase structure
* Analysis of EEGs (hypothesis testing for correlations, adjacency matrix, graphs)

em-analysis:
* results presented in chapter 2 (chapter2.py)
* results presented in chapter 3 except EEGs (chapter3.py)

fixation-word-assignation:
* Benoit Lemaire's perl scripts to associate fixations to words

em-analysis/src/R-scripts/
* Various R scripts for posterior analysis