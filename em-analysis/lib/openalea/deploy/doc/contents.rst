.. Do not edit. 
.. File automatically generated by sphinx_tools.py, revision 1658, on Mon Mar  9 12:11:23 2009

.. _deploy:

.. module:: deploy

OpenAlea Deploy documentation
#############################

Module description
==================

.. sidebar:: Summary

    :Version: |version|
    :Release: |release|
    :Date: |today|
    :Author: See `Authors`_ section
    :ChangeLog: See `ChangeLog`_ section

.. topic:: Overview

    .. include:: user/overview.txt

Documentation
=============

.. toctree::
    :maxdepth: 1

    User Guide<user/index.rst>   
    Reference Guide<user/autosum.rst>

- A `PDF <../latex/deploy.pdf>`_ version of |deploy| documentation is 
  available.

.. seealso::

   More documentation can be found on the
   `openalea <http://openalea.gforge.inria.fr>`__ wiki.

Authors
=======

.. include:: ../AUTHORS.txt

ChangeLog
=========

.. include:: ../ChangeLog.txt

License
=======

|deploy| is released under a Cecill-C License.

.. note:: `Cecill-C <http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html>`_ 
    license is a LGPL compatible license.

